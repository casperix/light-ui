package ru.casperix.light_ui.util

import ru.casperix.light_ui.element.Element
import ru.casperix.light_ui.node.Node
import ru.casperix.math.angle.float32.RadianFloat
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.geometry.CustomPolygon
import ru.casperix.math.geometry.Polygon2f
import ru.casperix.math.polar.float32.PolarCoordinateFloat
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.toQuad
import ru.casperix.misc.max
import ru.casperix.misc.min


fun List<Element>.wrapNodes():List<Node> {
    return map { Node(it) }
}

fun Polygon2f.getAABBox(): Box2f {
    val vertices = getVertices()
    val minVertex = vertices.reduce { a, b -> a.lower(b) }
    val maxVertex = vertices.reduce { a, b -> a.upper(b) }
    return Box2f(minVertex, maxVertex)
}

fun boxShape(x: Float, y: Float): Polygon2f {
    return boxShape(Vector2f(x, y))
}

fun boxShape(range: Vector2f): Polygon2f {
    return Box2f.byRadius(Vector2f.ZERO, range).toQuad()
}

fun ellipseShape(x: Float, y: Float, steps: Int = 32): Polygon2f {
    return ellipseShape(Vector2f(x, y), steps)
}

fun ellipseShape(range: Vector2f, steps: Int = 32): Polygon2f {
    return CustomPolygon((0 until steps).map {
        val angle = RadianFloat.PI2 * (it / steps.toFloat())
        PolarCoordinateFloat(1f, angle).toDecart() * range
    })
}

fun Matrix3f.transform(polygon: Polygon2f): Polygon2f {
    return polygon.convert { transform(it) }
}


/**
 * a can bigger than b
 */
fun IntRange.Companion.betweenInclusive(a: Int, b: Int): IntRange {
    return min(a, b) until  max(a, b)

}