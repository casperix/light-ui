package ru.casperix.light_ui.util

import kotlin.properties.ObservableProperty
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

object DelegatesExt {
    inline fun <T> observableChange(initialValue: T, crossinline onChange: (newValue: T) -> Unit):
            ReadWriteProperty<Any?, T> = object : ObservableProperty<T>(initialValue) {
        override fun afterChange(property: KProperty<*>, oldValue: T, newValue: T) {
            if (oldValue != newValue) {
                onChange(newValue)
            }
        }
    }
}