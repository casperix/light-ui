package ru.casperix.light_ui.util

import ru.casperix.misc.min
import kotlin.time.Duration
import kotlin.time.DurationUnit

class Transition<State>(val interpolator: (State, State, Float) -> State) {
    private var from: State? = null
    private var to: State? = null
    private var progress: Float = 1f
    private var secondsForSwitch = 1f

    fun calculate(target: State, timeForSwitch: Duration, tick: Duration): State {
        var from = from ?: target
        var to = to ?: target

        if (to != target) {
            from = interpolator(from, to, progress)
            to = target
            progress = 0f
            secondsForSwitch = timeForSwitch.toDouble(DurationUnit.SECONDS).toFloat()
        }

        if (secondsForSwitch == 0f) {
            progress = 1f
        } else {
            progress = min(1f, progress + tick.toDouble(DurationUnit.SECONDS).toFloat() / secondsForSwitch)
        }
        this.from = from
        this.to = to

        return interpolator(from, to, progress)
    }
}