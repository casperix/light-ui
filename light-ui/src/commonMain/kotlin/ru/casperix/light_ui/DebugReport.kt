package ru.casperix.light_ui

import ru.casperix.light_ui.element.AbstractContainer
import ru.casperix.light_ui.node.Node
import ru.casperix.misc.format.FormatType

object DebugReport {
    fun debugHierarchy(node: Node): String {
        return debugHierarchy(node, 0).joinToString("\n")
    }

    private fun debugHierarchy(node: Node, depth: Int = 0): List<String> {

        val offset = (0 until depth).joinToString("") {
            " + "
        }



        val report = offset + listOf(
            "T: " + node.element.tag,
            "C: " + node.element::class.simpleName,
            "P: " + node.transform.position.roundToVector2i().format(FormatType.SHORT),
            "S: " + node.placement.sizeMode,
            "V: " + node.placement.viewportSize.roundToVector2i().format(FormatType.SHORT),
            "C: " + node.placement.contentArea.dimension.roundToVector2i().format(FormatType.SHORT),
        ).joinToString("; ")

        val result = mutableListOf(report)

        val container = node.element as? AbstractContainer
        container?.children?.map {
            result += debugHierarchy(it, depth + 1)
        }
        return result
    }
}