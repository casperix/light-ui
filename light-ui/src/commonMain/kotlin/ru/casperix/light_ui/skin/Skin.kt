package ru.casperix.light_ui.skin

import ru.casperix.light_ui.component.button.CheckButtonSkin
import ru.casperix.light_ui.component.button.OpenableContainerSkin
import ru.casperix.light_ui.component.button.PushButtonSkin
import ru.casperix.light_ui.component.button.ToggleButtonSkin
import ru.casperix.light_ui.component.image.ImageSkin
import ru.casperix.light_ui.component.panel.QuadSkin
import ru.casperix.light_ui.component.progressbar.ProgressBarSkin
import ru.casperix.light_ui.component.scroll.ScrollBoxSkin
import ru.casperix.light_ui.component.slider.SliderSkin
import ru.casperix.light_ui.component.text.LabelSkin
import ru.casperix.light_ui.component.text.TextEditorSkin
import ru.casperix.light_ui.component.window.WindowSkin
import ru.casperix.multiplatform.font.FontReference

interface Skin {
    val defaultFont: FontReference
    val panelDefault: QuadSkin
    val panelTitle: QuadSkin
    val image: ImageSkin
    val label: LabelSkin
    val textEditor: TextEditorSkin
    val scrollBox: ScrollBoxSkin
    val window: WindowSkin
    val progressBar: ProgressBarSkin
    val pushButton: PushButtonSkin
    val toggleButton: ToggleButtonSkin
    val checkButton: CheckButtonSkin
    val sliderSkin:SliderSkin
    val openableContainer: OpenableContainerSkin
}