package ru.casperix.light_ui

import ru.casperix.light_ui.element.Element
import ru.casperix.light_ui.element.ElementUpdate

interface View {
    val root: Element

}

fun <CustomView : View> CustomView.buildAndUpdate(builder: CustomView.() -> ElementUpdate): Element {
    root.update = builder()
    return root
}

fun <CustomView : View> CustomView.build(builder: CustomView.() -> Unit): Element {
    builder()
    return root
}