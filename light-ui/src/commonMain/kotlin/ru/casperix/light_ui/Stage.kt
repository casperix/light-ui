package ru.casperix.light_ui

import ru.casperix.input.InputEvent
import ru.casperix.light_ui.node.InputContext
import ru.casperix.light_ui.node.Node
import ru.casperix.light_ui.node.NodeCollector
import ru.casperix.light_ui.node.NodeCollectorBuilder
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.multiplatform.toDimension
import ru.casperix.renderer.Renderer2D
import ru.casperix.renderer.depth.DepthState
import ru.casperix.renderer.misc.orthographic
import kotlin.time.Duration

class Stage(val root: Node) {

    private var collector = NodeCollector()
    private var sceneTime = Duration.ZERO
    private val nodeRenderer = NodeRenderer()

    fun render(renderer: Renderer2D, tick: Duration) {
        sceneTime += tick

       val next = NodeCollector()

        val viewport = renderer.viewPort.toVector2f()
        renderer.viewMatrix = Matrix3f.IDENTITY
        renderer.projectionMatrix = Matrix3f.orthographic(viewport.toDimension(), false, true)
        renderer.depthState = DepthState.DISABLE

        NodeCollectorBuilder.collectLayoutTo(next, root)
        LayoutCalculator.calculate(root, next.layoutList, viewport)
        NodeCollectorBuilder.collectTo(next, root)

        collector = next

        collector.updateList.forEach {
            it.update(tick)
        }

        nodeRenderer.render(collector.renderList, renderer, tick)

        DebugRender.debugRender(renderer, sceneTime, collector)

        renderer.flush()
    }

    fun input(event: InputEvent) {
        collector.inputList.forEach {
            inputToElement(event, it)
        }
    }

    private fun inputToElement(event: InputEvent, context: InputContext) = context.node.apply {
        val input = element.input ?: return@apply
        input.input(event, context)
    }
}

