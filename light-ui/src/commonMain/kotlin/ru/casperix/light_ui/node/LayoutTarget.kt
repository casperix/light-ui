package ru.casperix.light_ui.node

import ru.casperix.light_ui.element.ElementPlacement
import ru.casperix.math.axis_aligned.float32.Box2f

interface LayoutTarget {
    val placement: ElementPlacement

    fun setPlace(area: Box2f)
}