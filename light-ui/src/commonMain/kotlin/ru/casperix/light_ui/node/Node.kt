package ru.casperix.light_ui.node

import ru.casperix.light_ui.element.Element
import ru.casperix.light_ui.element.Empty
import ru.casperix.light_ui.element.setViewPort
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.transform.float32.Transform2f
import ru.casperix.math.vector.float32.Vector2f
import kotlinx.serialization.Serializable

@Serializable
class Node(var element: Element = Empty()) : LayoutTarget {
    var transform = Transform2f.ZERO

    override val placement get() = element.placement

    override fun setPlace(area: Box2f) {
        val borders = element.placement.borders

        transform = Transform2f(area.min + borders.leftTop)
        element.setViewPort(Vector2f.ZERO.upper(area.dimension - borders.size))
    }
}

