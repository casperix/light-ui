package ru.casperix.light_ui.node

import ru.casperix.light_ui.element.ElementUpdate

class NodeCollector() {
    var maxDepth = 0

    val layoutList = mutableListOf<LayoutContext>()
    val elementList = mutableListOf<ElementContext>()
    val renderList = mutableListOf<RenderContext>()
    val inputList = mutableListOf<InputContext>()
    val updateList = mutableListOf<ElementUpdate>()

    fun clear() {
        maxDepth = 0
        layoutList.clear()
        elementList.clear()
        renderList.clear()
        inputList.clear()
        updateList.clear()
    }
}