package ru.casperix.light_ui.node

import ru.casperix.input.PointerEvent
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.intersection.float32.Intersection2Float
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.toQuad
import ru.casperix.misc.Either
import ru.casperix.light_ui.element.Element
import ru.casperix.light_ui.element.ElementDrawer
import ru.casperix.light_ui.element.ElementInput
import ru.casperix.light_ui.element.ElementWithLayout


typealias RenderContext = Either<ViewPortContext, DrawContext>

interface AbstractContext {
    val element: Element

    val worldMatrix: Matrix3f
    val depth: Int

    val size get() =  element.placement.viewportSize
    val worldShape
        get() = Box2f(Vector2f.ZERO, element.placement.viewportSize).toQuad().convert {
            worldMatrix.transform(it)
        }
    val contentShape
        get() = element.placement.contentArea.toQuad().convert {
            worldMatrix.transform(it)
        }
}

class DrawContext(val node: Node, val drawer: ElementDrawer, override val worldMatrix: Matrix3f, override val depth: Int) : AbstractContext {
    override val element = node.element
}


enum class ViewPortPhase {
    START,
    FINISH,
}

class ViewPortContext(val node: Node, val worldMatrix: Matrix3f, val phase: ViewPortPhase)

class InputContext(val node: Node, val input: ElementInput, val worldMatrix: Matrix3f) {
    val worldShape get() = Box2f(Vector2f.ZERO, node.element.placement.viewportSize).toQuad().convert {
        worldMatrix.transform(it)
    }

    fun hasIntersection(event: PointerEvent) = Intersection2Float.hasPointWithPolygon(event.position, worldShape)
}

class LayoutContext(val node: Node, val element: ElementWithLayout)


class ElementContext(override val element: Element, override val worldMatrix: Matrix3f, override val depth: Int) : AbstractContext