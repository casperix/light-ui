package ru.casperix.light_ui.node

import ru.casperix.light_ui.NodeViewport
import ru.casperix.light_ui.StageConfig
import ru.casperix.light_ui.element.AbstractContainer
import ru.casperix.light_ui.element.ElementViewport
import ru.casperix.light_ui.element.ElementWithLayout
import ru.casperix.math.angle.float32.DegreeFloat
import ru.casperix.math.axis_aligned.int32.Box2i
import ru.casperix.math.intersection.int32.Intersection2Int
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.misc.Left
import ru.casperix.misc.Right
import kotlin.math.max


object NodeCollectorBuilder {

    fun collectLayoutTo(collector: NodeCollector, root: Node) {
        collectLayoutRecursive(collector, root, 0)
    }

    fun collectTo(collector: NodeCollector, root: Node) {
        val viewport = NodeViewport.getVisualArea(root, Matrix3f.IDENTITY) ?: Box2i.ZERO
        collectOtherRecursive(collector, root, Matrix3f.IDENTITY, true, 0, viewport)
    }

    private fun collectLayoutRecursive(collector: NodeCollector, node: Node, depth: Int) {
        val layoutList = collector.layoutList

        val layout = node.element as? ElementWithLayout
        if (layout != null) {
            layoutList += LayoutContext(node, layout)
        }

        val container = node.element as? AbstractContainer
        if (container != null) {
            container.children.forEach { entry ->
                collectLayoutRecursive(collector, entry, depth + 1)
            }
        }
    }

    private fun collectOtherRecursive(collector: NodeCollector, node: Node, worldMatrix: Matrix3f, worldMatrixTranslateOnly: Boolean, depth: Int, viewport: Box2i) {
       if (StageConfig.viewportFilter) {
            val elementArea = NodeViewport.getVisualArea(node, worldMatrix)
            if (elementArea == null || !Intersection2Int.hasBoxWithBox(elementArea, viewport)) return
        }

        collector.maxDepth = max(collector.maxDepth, depth)

        collector.elementList += ElementContext(node.element, worldMatrix, depth)

        val update = node.element.update
        if (update != null) {
            collector.updateList += update
        }

        val viewportHolder = if (worldMatrixTranslateOnly) {
            node.element as? ElementViewport
        } else null

        if (viewportHolder != null) {
            collector.renderList += Left(ViewPortContext(node, worldMatrix, ViewPortPhase.START))
        }

        val nextViewport = if (viewportHolder != null) {
            NodeViewport.getVisualArea(node, worldMatrix) ?: return
        } else viewport

        val drawer = node.element.drawer
        if (drawer != null) {
            collector.renderList += Right(DrawContext(node, drawer, worldMatrix, depth))
        }

        val container = node.element as? AbstractContainer
        if (container != null) {
            container.children.forEach { entry ->
                val localMatrixTranslateOnly = entry.transform.scale == Vector2f.ONE && entry.transform.rotation == DegreeFloat.ZERO

                val childMatrix = entry.transform.matrix
                val summaryMatrix = childMatrix * worldMatrix

                collectOtherRecursive(collector, entry, summaryMatrix, worldMatrixTranslateOnly && localMatrixTranslateOnly, depth + 1, nextViewport)
            }
        }

        val input = node.element.input
        if (input != null) {
            collector.inputList += InputContext(node, input, worldMatrix)
        }

        if (viewportHolder != null) {
            collector.renderList += Left(ViewPortContext(node, worldMatrix, ViewPortPhase.FINISH))
        }
    }


}