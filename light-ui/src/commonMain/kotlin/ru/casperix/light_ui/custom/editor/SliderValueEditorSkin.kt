package ru.casperix.light_ui.custom.editor

import ru.casperix.light_ui.component.misc.SideIndents
import ru.casperix.math.vector.float32.Vector2f


class SliderValueEditorSkin(
    val defaultSize: Vector2f = Vector2f(200f, 30f),
    val sampleSize: Vector2f = Vector2f(90f, 90f),
    val borders: SideIndents = SideIndents(5f),
)