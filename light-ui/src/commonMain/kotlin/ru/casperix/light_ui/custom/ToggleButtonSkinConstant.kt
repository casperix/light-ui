package ru.casperix.light_ui.custom

import ru.casperix.math.color.rgb.RgbColor3b

object ToggleButtonSkinConstant {
    val brightestColor = RgbColor3b(240u, 240u, 240u)
    val primaryColor = RgbColor3b(141u, 146u, 196u)
    val uncheckedColor = RgbColor3b(170u, 170u, 170u)
    val disableColor = RgbColor3b(128u, 128u, 128u)
    val darkContour = RgbColor3b(70u, 70u, 70u)

    val bodyChecked = primaryColor
    val contourChecked = darkContour
    val focusedBodyChecked = primaryColor

    val bodyUnchecked = uncheckedColor
    val contourUnchecked = darkContour
    val focusedBodyUnchecked = uncheckedColor

    val focusedContour = brightestColor

    val pressBody = brightestColor
    val pressContour = brightestColor

    val disableBody = disableColor
    val disableContour = disableColor

    val contourThick = 2f
    val crossThick = 6f
}