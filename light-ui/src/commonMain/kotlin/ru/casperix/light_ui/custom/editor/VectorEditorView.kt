package ru.casperix.light_ui.custom.editor

import ru.casperix.light_ui.component.panel.Panel
import ru.casperix.light_ui.component.text.Label
import ru.casperix.light_ui.element.Container
import ru.casperix.light_ui.element.SizeMode
import ru.casperix.light_ui.element.setSizeMode
import ru.casperix.light_ui.layout.Layout

class VectorEditorView(titleValue: String, val skin: SliderValueEditorSkin) {
    val title = Label(titleValue)
    val sliderX = SliderValueEditor("X")
    val sliderY = SliderValueEditor("Y")
    val sliderZ = SliderValueEditor("Z")

    val root = Panel(
        Layout.VERTICAL,
        title,
        Container(
            Layout.HORIZONTAL,
            Container(
                Layout.VERTICAL,
                sliderX.root.setSizeMode(SizeMode.fixed(skin.defaultSize)),
                sliderY.root.setSizeMode(SizeMode.fixed(skin.defaultSize)),
                sliderZ.root.setSizeMode(SizeMode.fixed(skin.defaultSize)),
            ),
        ).setSizeMode(SizeMode.content)
    ).setSizeMode(SizeMode.content)
}