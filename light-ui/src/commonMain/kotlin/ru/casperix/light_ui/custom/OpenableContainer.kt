package ru.casperix.light_ui.custom

import ru.casperix.light_ui.component.button.ButtonLogic
import ru.casperix.light_ui.component.button.ButtonState
import ru.casperix.light_ui.component.button.CheckBox
import ru.casperix.light_ui.component.button.OpenableContainerSkin
import ru.casperix.light_ui.component.panel.Panel
import ru.casperix.light_ui.component.text.SkinnedElement
import ru.casperix.light_ui.element.*
import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.node.Node
import ru.casperix.light_ui.skin.SkinProvider
import ru.casperix.light_ui.util.wrapNodes
import ru.casperix.math.vector.float32.Vector2f

class OpenableContainer(titleValue: String, val content: Element, skin: OpenableContainerSkin = SkinProvider.skin.openableContainer) :
    SkinnedElement<OpenableContainerSkin>(skin, SizeMode.layout),
    AbstractContainer {
    override val layout = Layout.VERTICAL
    private val openButton = CheckBox(titleValue, false, skin.checkBox) { setOpen(isChecked) }

    val isOpened get() = openButton.isChecked

    override val input = ButtonLogic(ButtonState())

    override var children = listOf(Node(openButton))

    init {
        setOpen(isOpened)
    }


    fun setOpen(value: Boolean) {
        openButton.isChecked = value

        val summaryChild = if (value) {
            listOf(openButton, content)
        } else {
            listOf(openButton)
        }

        children = listOf(
            Node(
                Panel(Layout.VERTICAL, summaryChild.wrapNodes())
            )
        )
    }


}

