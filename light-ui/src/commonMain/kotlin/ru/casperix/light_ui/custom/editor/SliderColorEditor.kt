package ru.casperix.light_ui.custom.editor

import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.color.Color
import ru.casperix.math.color.rgb.RgbColor
import ru.casperix.math.color.rgb.RgbColor3f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.renderer.vector.builder.VectorGraphicBuilder2D
import ru.casperix.signals.concrete.StorageSignal


class SliderColorEditor(
    val title: String? = null,
    val skin: SliderValueEditorSkin = SliderValueEditorSkin(),
) : ValueEditor<RgbColor> {
    private val view = ColorEditorView(title ?: "", skin)

    override val valueListener = StorageSignal<RgbColor>(Color.BLACK)
    override val root = view.root

    private var selfChanged = false

    init {
        view.apply {
            sliderR.valueListener.then {
                updateValue()
            }
            sliderG.valueListener.then {
                updateValue()
            }
            sliderB.valueListener.then {
                updateValue()
            }
            valueListener.then {
                selfChanged = true
                valueListener.value.toColor3f().apply {
                    sliderR.valueListener.value = red
                    sliderG.valueListener.value = green
                    sliderB.valueListener.value = blue
                }
                selfChanged = false

                updateInfo()
            }
        }

        updateInfo()
    }

    private fun updateValue() {
        if (selfChanged) return
        view.apply {
            valueListener.value =
                RgbColor3f(sliderR.valueListener.value, sliderG.valueListener.value, sliderB.valueListener.value)
        }
    }

    private fun updateInfo() {

        val color = valueListener.value

        view.sample.setGraphic(VectorGraphicBuilder2D.build {
            addRect(color, Box2f(Vector2f.ZERO, skin.sampleSize))
        }, skin.sampleSize)
    }

}