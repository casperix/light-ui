package ru.casperix.light_ui.custom.editor

import ru.casperix.light_ui.component.image.Image
import ru.casperix.light_ui.component.misc.SideIndents
import ru.casperix.light_ui.component.panel.Panel
import ru.casperix.light_ui.component.text.Label
import ru.casperix.light_ui.element.Container
import ru.casperix.light_ui.element.SizeMode
import ru.casperix.light_ui.element.setSizeMode
import ru.casperix.light_ui.layout.Layout

class ColorEditorView(titleValue: String, val skin: SliderValueEditorSkin) {
    val title = Label(titleValue)
    val sliderR = SliderValueEditor("R")
    val sliderG = SliderValueEditor("G")
    val sliderB = SliderValueEditor("B")
    val sample = Image().setSizeMode(SizeMode.fixed(skin.sampleSize))

    val root = Panel(
        Layout.VERTICAL,
        title,
        Container(
            Layout.HORIZONTAL_CENTER,
            Container(
                Layout.VERTICAL,
                sliderR.root.setSizeMode(SizeMode.content),
                sliderG.root.setSizeMode(SizeMode.content),
                sliderB.root.setSizeMode(SizeMode.content),
            ).setSizeMode(SizeMode.content),
            sample.setSizeMode(SizeMode.fixed(skin.sampleSize)).apply {
                placement.borders = SideIndents(5)
            },
        ).setSizeMode(SizeMode.content)
    ).setSizeMode(SizeMode.content)

}

