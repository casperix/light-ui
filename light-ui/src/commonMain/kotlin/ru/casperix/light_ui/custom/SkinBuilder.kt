package ru.casperix.light_ui.custom

import ru.casperix.light_ui.component.button.*
import ru.casperix.light_ui.component.image.ImageSkin
import ru.casperix.light_ui.component.misc.SideIndents
import ru.casperix.light_ui.component.panel.QuadBodySkin
import ru.casperix.light_ui.component.panel.QuadContourSkin
import ru.casperix.light_ui.component.panel.QuadSkin
import ru.casperix.light_ui.component.progressbar.ProgressBarSkin
import ru.casperix.light_ui.component.scroll.ScrollBoxSkin
import ru.casperix.light_ui.component.slider.SliderSkin
import ru.casperix.light_ui.component.text.LabelSkin
import ru.casperix.light_ui.component.text.TextEditorSkin
import ru.casperix.light_ui.component.window.WindowSkin
import ru.casperix.light_ui.skin.Skin
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.geometry.float32.median
import ru.casperix.math.geometry.float32.translate
import ru.casperix.math.geometry.grow
import ru.casperix.math.straight_line.float32.LineSegment2f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.toQuad
import ru.casperix.multiplatform.font.FontLeading
import ru.casperix.multiplatform.font.FontReference
import ru.casperix.renderer.vector.VectorGraphic2D
import ru.casperix.renderer.vector.builder.VectorGraphicBuilder2D
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds

class SkinBuilder(
    val defaultColor: SkinColor = SkinColor(),
    override val defaultFont: FontReference = FontReference("Serif", 20)
) : Skin {
    val densityFont = defaultFont.copy(leading = FontLeading.DENSITY)

    override val panelDefault = QuadSkin(QuadBodySkin(defaultColor.medium, 0f), QuadContourSkin(defaultColor.darker, 2f, 0f))
    override val panelTitle = QuadSkin(QuadBodySkin(defaultColor.darker, 1f), null)
    val activeBackSkin = QuadSkin(QuadBodySkin(defaultColor.darker, 0f), QuadContourSkin(defaultColor.darkest, 2f, 0f))


    override val scrollBox = ScrollBoxSkin(100.milliseconds, activeBackSkin, null)

    override val image = ImageSkin(SideIndents(5f), null)


    override val label = LabelSkin(defaultFont, defaultColor.brightest, null)

    override val textEditor = TextEditorSkin(LabelSkin(defaultFont, defaultColor.brightest, activeBackSkin))
    override val window =
        WindowSkin(defaultColor.medium, defaultColor.darker, LabelSkin(densityFont, defaultColor.brightest, null), 60f, SideIndents(0f))
    override val progressBar =
        ProgressBarSkin(Vector2f(200f, 30f), defaultColor.darkest, 2f, defaultColor.darker, defaultColor.bright)


    val labelButton = LabelButtonSkin(
        Vector2f(180f, 50f),
        mapOf(
            Pair(ButtonViewState.PressOut, LabelSkin(densityFont, defaultColor.darkest, null)),
            Pair(ButtonViewState.PressOver, LabelSkin(densityFont, defaultColor.darkest, null)),
            Pair(ButtonViewState.Out, LabelSkin(densityFont, defaultColor.darkest, null)),
            Pair(ButtonViewState.Over, LabelSkin(densityFont, defaultColor.darkest, null)),
            Pair(ButtonViewState.Disable, LabelSkin(densityFont, defaultColor.darkest, null)),
        )
    )

    override val pushButton = PushButtonSkin(
        labelButton,
        Vector2f(180f, 50f),
        SideIndents(5f),
        ButtonSkinConstant.contourThick,
        mapOf(
            Pair(ButtonViewState.PressOut, ButtonViewSkin(0.10.seconds, ButtonSkinConstant.pressBody, ButtonSkinConstant.pressContour)),
            Pair(ButtonViewState.PressOver, ButtonViewSkin(0.05.seconds, ButtonSkinConstant.pressBody, ButtonSkinConstant.pressContour)),
            Pair(ButtonViewState.Out, ButtonViewSkin(0.20.seconds, ButtonSkinConstant.defaultBody, ButtonSkinConstant.defaultContour)),
            Pair(ButtonViewState.Over, ButtonViewSkin(0.10.seconds, ButtonSkinConstant.selectBody, ButtonSkinConstant.selectContour)),
            Pair(ButtonViewState.Disable, ButtonViewSkin(0.20.seconds, ButtonSkinConstant.disableBody, ButtonSkinConstant.disableContour)),
        )
    )

    override val sliderSkin = SliderSkin(
        pushButton,
        QuadSkin(QuadBodySkin(ToggleButtonSkinConstant.disableColor, -2f), QuadContourSkin(ToggleButtonSkinConstant.contourUnchecked, 2f, 0f)),
        QuadSkin(QuadBodySkin(ToggleButtonSkinConstant.bodyChecked, -2f), null),
        SideIndents(5f),
    )

    private val toggleMap = mapOf(
        Pair(ButtonViewState.PressOut, ButtonViewSkin(0.10.seconds, ToggleButtonSkinConstant.pressBody, ToggleButtonSkinConstant.pressContour)),
        Pair(ButtonViewState.PressOver, ButtonViewSkin(0.05.seconds, ToggleButtonSkinConstant.pressBody, ToggleButtonSkinConstant.pressContour)),
//        Pair(ButtonViewState.Out, ButtonViewSkin(0.20.seconds, ToggleButtonSkinConstant.defaultBody, ToggleButtonSkinConstant.defaultContour)),
//        Pair(ButtonViewState.Over, ButtonViewSkin(0.10.seconds, ToggleButtonSkinConstant.selectBody, ToggleButtonSkinConstant.selectContour)),
        Pair(ButtonViewState.Disable, ButtonViewSkin(0.20.seconds, ToggleButtonSkinConstant.disableBody, ToggleButtonSkinConstant.disableContour)),
    )

    override val toggleButton = ToggleButtonSkin(
        labelButton,
        Vector2f(180f, 50f),
        SideIndents(5f),
        ToggleButtonSkinConstant.contourThick,
        toggleMap + mapOf(
            Pair(
                ButtonViewState.Out,
                ButtonViewSkin(
                    0.20.seconds,
                    ToggleButtonSkinConstant.bodyUnchecked,
                    ToggleButtonSkinConstant.contourUnchecked
                )
            ),
            Pair(
                ButtonViewState.Over,
                ButtonViewSkin(0.10.seconds, ToggleButtonSkinConstant.focusedBodyUnchecked, ToggleButtonSkinConstant.focusedContour)
            ),
        ),
        toggleMap + mapOf(
            Pair(
                ButtonViewState.Out,
                ButtonViewSkin(
                    0.20.seconds,
                    ToggleButtonSkinConstant.bodyChecked,
                    ToggleButtonSkinConstant.contourChecked
                )
            ),
            Pair(
                ButtonViewState.Over,
                ButtonViewSkin(0.10.seconds, ToggleButtonSkinConstant.focusedBodyChecked, ToggleButtonSkinConstant.focusedContour)
            ),
        )
    )

    override val checkButton = CheckButtonSkin(
        labelButton,
        Vector2f(190f, 40f),
        SideIndents(0f),
        CheckBoxSkin(
            VectorGraphicBuilder2D.build {
                val bounds = Box2f.ONE.toQuad().grow(-0.16f)
                add(ToggleButtonSkinConstant.darkContour) {
                    addLine2f(LineSegment2f(bounds.v0, bounds.v2), 0.12f)
                    addLine2f(LineSegment2f(bounds.v1, bounds.v3), 0.12f)
                }
            },
            VectorGraphic2D.EMPTY,
            toggleButton.copy(defaultSize = Vector2f(30f), defaultBorders = SideIndents(5f))
        )
    )

    override val openableContainer = OpenableContainerSkin(
        CheckButtonSkin(
            labelButton,
            Vector2f(200f, 30f),
            SideIndents(0f),
            CheckBoxSkin(
                VectorGraphicBuilder2D.build {
                    val thick = 0.1f
                    val offset = Vector2f(0f, 0.25f)
                    val bounds = Box2f.ONE.toQuad().grow(-thick * 2f).translate(offset)
                    val center = bounds.median()
                    add(ToggleButtonSkinConstant.darkContour) {
                        addLine2f(LineSegment2f(bounds.v0, center).grow(thick / 2f, thick / 2f), thick)
                        addLine2f(LineSegment2f(center, bounds.v1).grow(thick / 2f, thick / 2f), thick)
                    }
                },
                VectorGraphicBuilder2D.build {
                    val thick = 0.1f
                    val offset = Vector2f(0.25f, 0f)
                    val bounds = Box2f.ONE.toQuad().grow(-thick* 2f).translate(offset)
                    val center = bounds.median()
                    add(ToggleButtonSkinConstant.darkContour) {
                        addLine2f(LineSegment2f(bounds.v0, center).grow(thick / 2f, thick / 2f), thick)
                        addLine2f(LineSegment2f(center, bounds.v3).grow(thick / 2f, thick / 2f), thick)
                    }
                },
                toggleButton.copy(defaultSize = Vector2f(30f), defaultBorders = SideIndents(5f), checked = emptyMap(), unchecked = emptyMap())
            )
        )
    )
}