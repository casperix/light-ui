package ru.casperix.light_ui.custom

import ru.casperix.math.color.rgb.RgbColor3b

class SkinColor {
    val brightest = RgbColor3b(255u, 255u, 255u)
    val bright = RgbColor3b(210u, 210u, 210u)
    val medium = RgbColor3b(140u, 140u, 140u)
    val darker = RgbColor3b(70u, 70u, 70u)
    val darkest = RgbColor3b(0u, 0u, 0u)

}