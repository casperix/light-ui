package ru.casperix.light_ui.custom.editor

import ru.casperix.light_ui.element.Element
import ru.casperix.signals.concrete.*

interface ValueEditor<Value> {
    val root: Element
    val valueListener: StoragePromise<Value>
}