package ru.casperix.light_ui.custom

import ru.casperix.math.color.rgb.RgbColor3b


object  ButtonSkinConstant {

    val brightestColor = RgbColor3b(240u, 240u, 240u)
    val primaryColor = RgbColor3b(86u, 116u, 234u)
    val disableColor = RgbColor3b(128u, 128u, 128u)
    val darkContour = RgbColor3b(70u, 70u, 70u)

    val defaultBody = primaryColor
    val defaultContour = darkContour

    val selectBody = primaryColor
    val selectContour = brightestColor

    val pressBody = brightestColor
    val pressContour = brightestColor

    val disableBody = disableColor
    val disableContour = disableColor

    val contourThick = 2f
}
