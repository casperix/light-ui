package ru.casperix.light_ui.custom.editor

import ru.casperix.light_ui.component.panel.Panel
import ru.casperix.light_ui.component.slider.Slider
import ru.casperix.light_ui.component.text.Label
import ru.casperix.light_ui.element.Container
import ru.casperix.light_ui.element.SizeMode
import ru.casperix.light_ui.element.setSizeMode
import ru.casperix.light_ui.layout.Layout
import ru.casperix.misc.toPrecision
import ru.casperix.signals.concrete.StorageSignal

class SliderValueEditor(
    val title: String? = null,
    val range: ClosedFloatingPointRange<Float> = 0f..1f,
    val valueFormatter: (Float) -> String = { it.toPrecision(1) },
    val _skin: SliderValueEditorSkin = SliderValueEditorSkin(),
) : ValueEditor<Float> {
    private val slider = Slider()
    private val name = Label(title ?: "")
    private val value = Label("")

    override val valueListener = StorageSignal(0f)
    override val root = Panel(
        Layout.HORIZONTAL,
        name,
        Container(
            Layout.STACK,
            slider.setSizeMode(SizeMode.fixed(_skin.defaultSize)),
            value.setSizeMode(SizeMode.fixed(_skin.defaultSize)),
        )
    ).apply {
        placement.borders = _skin.borders
        setSizeMode(SizeMode.content)
    }

    private var selfChanged = false

    init {
        slider.selectSignal.then {
            if (!selfChanged) {
                valueListener.value = rangeDenormalize(slider.value, range)
            }
        }
        valueListener.then {
            selfChanged = true
            slider.value = rangeNormalize(valueListener.value, range)
            selfChanged = false

            updateInfo()
        }

        updateInfo()
    }

    private fun updateInfo() {
        value.text = valueFormatter(valueListener.value)
    }

    companion object {
        fun rangeNormalize(raw: Float, range: ClosedFloatingPointRange<Float>) =
            (raw - range.start) / (range.endInclusive - range.start)

        fun rangeDenormalize(normalized: Float, range: ClosedFloatingPointRange<Float>) =
            range.start + (range.endInclusive - range.start) * normalized

    }
}