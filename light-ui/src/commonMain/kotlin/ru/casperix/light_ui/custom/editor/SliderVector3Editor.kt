package ru.casperix.light_ui.custom.editor

import ru.casperix.math.vector.float32.Vector3f
import ru.casperix.signals.concrete.StorageSignal

class SliderVector3Editor(
    val title: String? = null,
    val componentRange: ClosedFloatingPointRange<Float> = 0f..1f,
    val skin: SliderValueEditorSkin = SliderValueEditorSkin(),
) : ValueEditor<Vector3f> {
    private val view = VectorEditorView(title ?: "", skin)

    override val valueListener = StorageSignal(Vector3f.X)
    override val root = view.root

    private var selfChanged = false

    init {
        view.apply {
            sliderX.valueListener.then {
                updateValue()
            }
            sliderY.valueListener.then {
                updateValue()
            }
            sliderZ.valueListener.then {
                updateValue()
            }
            valueListener.then {
                selfChanged = true
                valueListener.value.apply {
                    sliderX.valueListener.value = SliderValueEditor.rangeNormalize(x, componentRange)
                    sliderY.valueListener.value = SliderValueEditor.rangeNormalize(y, componentRange)
                    sliderZ.valueListener.value = SliderValueEditor.rangeNormalize(z, componentRange)
                }
                selfChanged = false

                updateInfo()
            }
            updateInfo()
        }
    }

    private fun updateValue() {
        if (selfChanged) return

        view.apply {
            valueListener.value = Vector3f(
                SliderValueEditor.rangeDenormalize(sliderX.valueListener.value, componentRange),
                SliderValueEditor.rangeDenormalize(sliderY.valueListener.value, componentRange),
                SliderValueEditor.rangeDenormalize(sliderZ.valueListener.value, componentRange),
            )
        }
    }

    private fun updateInfo() {

    }

}