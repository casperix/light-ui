package ru.casperix.light_ui

import ru.casperix.light_ui.element.*
import ru.casperix.light_ui.node.ElementContext
import ru.casperix.light_ui.node.NodeCollector
import ru.casperix.math.collection.getLooped
import ru.casperix.math.color.Color
import ru.casperix.math.geometry.Quad2f
import ru.casperix.math.geometry.grow
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.straight_line.float32.LineSegment2f
import ru.casperix.multiplatform.text.drawText
import ru.casperix.renderer.Renderer2D
import ru.casperix.renderer.material.SimpleMaterial
import ru.casperix.renderer.vector.builder.VectorGraphicBuilder2D
import kotlin.time.Duration

object DebugRender {
    enum class DebugLayer {
        CONTENT,
        VIEWPORT,
        HIERARCHY_PRIMARY,
        HIERARCHY_SECONDARY
    }



    fun debugRender(renderer: Renderer2D, sceneTime: Duration, collector: NodeCollector) {
        if (StageConfig.debugViewports) {
            collector.elementList.forEach {
                debugRender(renderer, sceneTime, it, collector.maxDepth, DebugLayer.VIEWPORT)
            }
            if (StageConfig.debugLegend) {
                renderer.drawText("fixed", Matrix3f.translate(0f, 0f), color = Color.RED)
                renderer.drawText("content", Matrix3f.translate(0f, 20f), color = Color.GREEN)
                renderer.drawText("view", Matrix3f.translate(0f, 40f), color = Color.BLUE)
                renderer.drawText("min", Matrix3f.translate(0f, 60f), color = Color.CYAN)
                renderer.drawText("max", Matrix3f.translate(0f, 80f), color = Color.FUCHSIA)
                renderer.drawText("layout", Matrix3f.translate(0f, 100f), color = Color.YELLOW)
            }
        }
        if (StageConfig.debugContents) {
            collector.elementList.forEach {
                debugRender(renderer, sceneTime, it, collector.maxDepth, DebugLayer.CONTENT)
            }
        }
        if (StageConfig.debugHierarchy) {
            collector.elementList.forEach {
                debugRender(renderer, sceneTime, it, collector.maxDepth, DebugLayer.HIERARCHY_PRIMARY)
            }
            collector.elementList.forEach {
                debugRender(renderer, sceneTime, it, collector.maxDepth, DebugLayer.HIERARCHY_SECONDARY)
            }
        }
    }

    private fun colorForDimensionMode(mode:DimensionMode):Color = when(mode) {
        is FixedDimension->Color.RED
        is ContentDimension->Color.GREEN
        is ViewDimension->Color.BLUE
        is MinContentOrViewDimension->Color.CYAN
        is MaxContentOrViewDimension->Color.FUCHSIA
        is MaxContentOrLayoutDimension->Color.YELLOW
    }

    private fun debugRender(renderer: Renderer2D, sceneTime: Duration, context: ElementContext, maxDepth: Int, layer: DebugLayer) {
        val color = StageConfig.COLORS.getLooped(context.depth)
        val actualDepth = (sceneTime.inWholeMilliseconds / 500L).toInt() % (maxDepth + 1) == context.depth

        val hColor = colorForDimensionMode(context.element.placement.sizeMode.width)
        val vColor = colorForDimensionMode(context.element.placement.sizeMode.height)


        renderer.draw {
            when (layer) {
                DebugLayer.HIERARCHY_PRIMARY -> if (actualDepth) {
                   addQuadContour(SimpleMaterial(Color.BLACK, 0.7f), context.worldShape.grow(4f), 8f)
                }

                DebugLayer.HIERARCHY_SECONDARY -> if (actualDepth) {
                    addQuad(SimpleMaterial(color, 0.7f), context.worldShape)
                }

                DebugLayer.CONTENT -> {
                    addQuadContour(color, context.contentShape, 2f)
                }

                DebugLayer.VIEWPORT -> {
                    addQuadContour2(hColor, vColor, context.worldShape, 2f)
//                    addQuadContour(color, context.worldShape, 2f)
                }
            }
        }
    }

    private fun VectorGraphicBuilder2D.addQuadContour2(hColor: Color, vColor: Color, worldShape: Quad2f, thick: Float) {
        addLine(hColor, LineSegment2f(worldShape.v0, worldShape.v1), thick)
        addLine(hColor, LineSegment2f(worldShape.v2, worldShape.v3), thick)

        addLine(vColor, LineSegment2f(worldShape.v1, worldShape.v2), thick)
        addLine(vColor, LineSegment2f(worldShape.v0, worldShape.v3), thick)
    }
}