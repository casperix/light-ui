package ru.casperix.light_ui

import ru.casperix.light_ui.node.LayoutContext
import ru.casperix.light_ui.node.Node
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.vector.float32.Vector2f

object LayoutCalculator {

    fun calculate(root: Node, layoutList: List<LayoutContext>, viewport: Vector2f) {
        /**
         * TODO: need improve layout update
         *  Current method not stable:
         *  - if elements added or removed
         *  - if manual resized
         *  etc.
         */
        root.element.placement.apply {
            if (viewportSize != viewport) {
                layoutList.forEach {
                    it.element.placement.viewportSize = Vector2f.ZERO
                    it.element.placement.contentArea = Box2f.ZERO
                }

                viewportSize = viewport
            }
        }

        repeat(2) {
            layoutList.asReversed().forEach {
                calculateLayoutUp(it)
            }
            layoutList.forEach {
                calculateLayoutUp(it)
            }
        }
    }


    private fun calculateLayoutUp(context: LayoutContext) = context.apply {
        element.invalidateLayout()
    }
}