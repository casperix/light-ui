package ru.casperix.light_ui.element

import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.misc.toPrecision
import kotlinx.serialization.Serializable

@Serializable
data class SizeMode(val width: DimensionMode, val height: DimensionMode) {
    companion object {
        fun fixed(value: Vector2f): SizeMode {
            return SizeMode(FixedDimension(value.x), FixedDimension(value.y))
        }

        fun fixedWidth(width: Float): SizeMode {
            return SizeMode(FixedDimension(width), MaxContentOrViewDimension)
        }

        fun fixedHeight(height: Float): SizeMode {
            return SizeMode(MaxContentOrViewDimension, FixedDimension(height))
        }

        val view = SizeMode(ViewDimension, ViewDimension)
        val content = SizeMode(ContentDimension, ContentDimension)
        val layout = SizeMode(MaxContentOrLayoutDimension, MaxContentOrLayoutDimension)
        val min = SizeMode(MinContentOrViewDimension, MinContentOrViewDimension)
        val max = SizeMode(MaxContentOrViewDimension, MaxContentOrViewDimension)

        val row = SizeMode(MaxContentOrViewDimension, MaxContentOrLayoutDimension)
        val column = SizeMode(MaxContentOrLayoutDimension, MaxContentOrViewDimension)
    }

    override fun toString(): String {
        return "" + dimToString(width) + "X" + dimToString(height)
    }

    private fun dimToString(mode: DimensionMode): String {
        return when (mode) {
            is FixedDimension -> mode.value.toPrecision(0)
            is ViewDimension -> "view"
            is ContentDimension -> "child"
            is MinContentOrViewDimension -> "min"
            is MaxContentOrViewDimension -> "max"
            is MaxContentOrLayoutDimension -> "layout"
        }
    }
}
