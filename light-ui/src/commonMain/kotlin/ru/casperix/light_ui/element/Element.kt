package ru.casperix.light_ui.element


interface Element {
    val placement: ElementPlacement
    val drawer: ElementDrawer?
    val input: ElementInput?

    /**
     * You can place any custom actions for run before element draw
     */
    var update: ElementUpdate?
    var tag:String
}

