package ru.casperix.light_ui.element

interface ElementWithLayout : Element {
    fun invalidateLayout()
}