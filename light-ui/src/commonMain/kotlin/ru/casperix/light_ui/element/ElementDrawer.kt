package ru.casperix.light_ui.element

import ru.casperix.light_ui.node.DrawContext
import ru.casperix.renderer.Renderer2D
import kotlin.time.Duration

interface ElementDrawer {
    fun draw(renderer: Renderer2D, context: DrawContext, tick: Duration)
}

