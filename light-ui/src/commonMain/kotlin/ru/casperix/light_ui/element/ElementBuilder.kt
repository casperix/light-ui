package ru.casperix.light_ui.element

import ru.casperix.math.vector.float32.Vector2f
import kotlin.time.Duration

fun <CustomElement : Element> CustomElement.setUpdate(value: ElementUpdate) = apply {
    update = value
}

fun <CustomElement : Element> CustomElement.setElementUpdate(handler: (CustomElement, Duration) -> Unit) = apply {
    update = ElementUpdate {
        handler(this, it)
    }
}

fun <CustomElement : Element> CustomElement.setSizeMode(sizeMode: SizeMode) = apply {
    placement.sizeMode = sizeMode
}

fun <CustomElement : Element> CustomElement.setViewPort(dimension: Vector2f) = apply {
    placement.viewportSize = dimension
}

fun <CustomElement : Element> CustomElement.setSizeMode(width:DimensionMode, height:DimensionMode) = apply {
    placement.sizeMode = SizeMode(width, height)
}
