package ru.casperix.light_ui.element

import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.node.Node
import ru.casperix.light_ui.node.DrawContext
import ru.casperix.renderer.Renderer2D
import kotlin.time.Duration

class DrawableContainer(
    override val layout: Layout,
    override val children: List<Node>,
    override val drawer: ElementDrawer,
) : AbstractElement(SizeMode.content), AbstractContainer, ElementDrawer {

    override fun draw(renderer: Renderer2D, context: DrawContext, tick: Duration) {
       drawer.draw(renderer, context, tick)
    }
}