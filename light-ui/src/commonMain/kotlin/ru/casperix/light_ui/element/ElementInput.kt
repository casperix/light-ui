package ru.casperix.light_ui.element

import ru.casperix.input.InputEvent
import ru.casperix.light_ui.node.InputContext

interface ElementInput  {
    fun input(event: InputEvent, context: InputContext)
}