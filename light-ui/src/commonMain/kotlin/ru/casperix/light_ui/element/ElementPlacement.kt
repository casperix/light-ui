package ru.casperix.light_ui.element

import ru.casperix.light_ui.component.misc.SideIndents
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.misc.max
import ru.casperix.misc.min
import kotlinx.serialization.Serializable


@Serializable
class ElementPlacement(var sizeMode: SizeMode) {
    /**
     * Preferred space around this element
     */
    var borders = SideIndents.ZERO

    /**
     * Actual size of this element
     * (Available size)
     */
    var viewportSize = Vector2f.ZERO

    /**
     * Used size (children, labels, images, etc.)
     */
    var contentArea = Box2f.ZERO

    fun calculateSize(viewportSize: Vector2f, contentSize: Vector2f, layoutSize: Vector2f): Vector2f {
        val bordersSize = borders.size

        return Vector2f(
            calculateSize(sizeMode.width, viewportSize.x - bordersSize.x, contentSize.x, layoutSize.x),
            calculateSize(sizeMode.height, viewportSize.y - bordersSize.y, contentSize.y, layoutSize.y),
        ) + bordersSize
    }

    private fun calculateSize(dimensionMode: DimensionMode, viewportSize: Float, contentSize: Float, layoutSize: Float): Float {
        val value = when (dimensionMode) {
            is FixedDimension -> dimensionMode.value
            is ContentDimension -> contentSize
            is ViewDimension -> viewportSize
            is MinContentOrViewDimension -> min(viewportSize, contentSize)
            is MaxContentOrViewDimension -> max(viewportSize, contentSize)
            is MaxContentOrLayoutDimension -> max(layoutSize, contentSize)
        }
        if (!value.isFinite()) return 0f
        return value
    }
}