package ru.casperix.light_ui.element

import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.node.Node
import ru.casperix.light_ui.util.wrapNodes

class Container(
    override val layout: Layout,
) : AbstractElement(SizeMode.layout), AbstractContainer {
    override val children = mutableListOf<Node>()

    constructor(layout: Layout, children: List<Node>) : this(layout) {
        this.children += children
    }

    constructor(layout: Layout, vararg children: Node?) : this(layout) {
        this.children += children.filterNotNull()
    }

    constructor(layout: Layout, vararg children: Element?) : this(layout) {
        this.children += children.filterNotNull().wrapNodes()
    }

    fun add(element: Element): Node {
        val node = Node(element)
        children += node
        return node
    }

    fun remove(element: Element): Node? {
        children.forEach { next ->
            if (next.element == element) {
                children.remove(next)
                return next
            }
        }
        return null
    }

}