package ru.casperix.light_ui.element

import ru.casperix.misc.toPrecision

sealed interface DimensionMode

class FixedDimension(val value: Float) : DimensionMode {
    init {
        if (!value.isFinite() || value < 0f) throw Error("Expected finite, non negative value, but actual is $value")
    }

    override fun toString(): String {
        return "(${value.toPrecision(1)})"
    }
}

object ContentDimension : DimensionMode
object ViewDimension : DimensionMode
object MinContentOrViewDimension : DimensionMode
object MaxContentOrViewDimension : DimensionMode

object MaxContentOrLayoutDimension : DimensionMode

