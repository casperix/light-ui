package ru.casperix.light_ui.element

open class AbstractElement(
    sizeBehaviour: SizeMode,
    override val drawer: ElementDrawer? = null,
    override val input: ElementInput? = null
) :
    Element {
    override val placement = ElementPlacement(sizeBehaviour)
    override var update: ElementUpdate? = null
    override var tag = ""

}

