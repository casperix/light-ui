package ru.casperix.light_ui.element

import kotlin.time.Duration

fun interface ElementUpdate  {
    fun update(tick: Duration)
}

