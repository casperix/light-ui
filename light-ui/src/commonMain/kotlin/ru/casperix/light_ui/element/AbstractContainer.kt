package ru.casperix.light_ui.element

import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.node.Node

interface AbstractContainer : Element, ElementWithLayout {
    val layout: Layout
    val children: List<Node>

    override fun invalidateLayout() {
        placement.contentArea = layout.calculate(placement, children)
    }
}