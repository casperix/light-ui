package ru.casperix.light_ui

import ru.casperix.light_ui.node.Node
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.axis_aligned.int32.Box2i
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.vector.float32.Vector2f

object NodeViewport {
    fun getVisualArea(node: Node, worldMatrix: Matrix3f): Box2i? {
        val pivot = worldMatrix.transform(Vector2f.ZERO)
        return Box2f.byDimension(pivot, node.element.placement.viewportSize).toBox2i()
    }
}