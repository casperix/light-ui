package ru.casperix.light_ui.component.text

import ru.casperix.light_ui.component.panel.QuadGraphic
import ru.casperix.light_ui.element.ElementDrawer
import ru.casperix.light_ui.element.ElementWithLayout
import ru.casperix.light_ui.node.DrawContext
import ru.casperix.light_ui.skin.SkinProvider
import ru.casperix.light_ui.util.DelegatesExt
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.color.Color
import ru.casperix.math.color.rgba.RgbaColor
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.multiplatform.font.FontReference
import ru.casperix.multiplatform.text.drawText
import ru.casperix.multiplatform.text.impl.TextScheme
import ru.casperix.multiplatform.text.impl.TextView
import ru.casperix.multiplatform.text.textRenderer
import ru.casperix.renderer.Renderer2D
import ru.casperix.renderer.misc.AlignMode
import kotlin.time.Duration

class Label(initialText: String = "", skin: LabelSkin = SkinProvider.skin.label, alignMode: AlignMode = AlignMode.CENTER_CENTER) : ElementDrawer,
    SkinnedElement<LabelSkin>(skin), ElementWithLayout {

    override val drawer = this

    private var scheme: TextScheme? = null

    class TextBlock(val text: String, val customFont: FontReference? = null, val customColor: RgbaColor? = null)

    var text: String
        get() {
            return textBlocks.joinToString { it.text }
        }
        set(value) {
            textBlocks = listOf(TextBlock(value))
        }

    private var viewportSize: Vector2f by DelegatesExt.observableChange(Vector2f.ZERO) { dropScheme() }

    var textBlocks: List<TextBlock> by DelegatesExt.observableChange(emptyList()) { dropScheme() }
    var font: FontReference by DelegatesExt.observableChange(skin.font) { dropScheme() }
    var alignMode: AlignMode by DelegatesExt.observableChange(alignMode) { dropScheme() }

    init {
        text = initialText
        placement.borders = skin.borders
    }

    private fun dropScheme() {
        scheme = null
    }

    override fun invalidateLayout() {
        viewportSize = placement.viewportSize
        updateScheme()
    }

    private fun updateScheme() {
        var nextScheme = scheme
        if (nextScheme == null) {
            val viewList = textBlocks.map {
                TextView(it.text, it.customFont ?: skin.font, it.customColor ?: skin.color)
            }
            nextScheme = textRenderer.getTextScheme(viewList, viewportSize, alignMode)

            placement.contentArea = nextScheme.summaryArea.value.run { Box2f.byDimension(min.round(), dimension.round()) }
            scheme = nextScheme
        }
    }


    override fun draw(renderer: Renderer2D, context: DrawContext, tick: Duration): Unit = context.run {
        updateScheme()

        skin.back?.let {
            QuadGraphic(it).draw(renderer, context, tick)
        }
        scheme?.let {
            renderer.drawText(it, worldMatrix, true, skin.drawTextMetrics)
        }
    }


}

