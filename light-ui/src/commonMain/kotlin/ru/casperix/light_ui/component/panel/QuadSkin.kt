package ru.casperix.light_ui.component.panel

import ru.casperix.math.color.Color

data class QuadSkin(val body: QuadBodySkin?, val contour: QuadContourSkin?)

data class QuadContourSkin(val color: Color, val thick: Float, val grow:Float)
data class QuadBodySkin(val color: Color, val grow:Float)