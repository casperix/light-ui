package ru.casperix.light_ui.component

import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.layout.orientation_layout.OrientationLayout
import ru.casperix.light_ui.layout.common.SizeCalculator
import ru.casperix.light_ui.types.Orientation
import ru.casperix.light_ui.util.wrapNodes
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.int32.Vector2i
import ru.casperix.renderer.misc.Align
import ru.casperix.renderer.misc.AlignMode
import ru.casperix.light_ui.element.*

@Deprecated(message = "Use TableLayout")
fun createTable(items: List<Element>, columns: Int, rowAlignCentered: Boolean = false, independentSize: Boolean = false): Container {
    val table = items.chunked(columns)

    val entry = TableBuilder()

    table.forEachIndexed { rowIndex, rowElements ->
        if (entry.rows.size <= rowIndex) {
            entry.rows += mutableListOf<Cell>()
        }
        rowElements.forEachIndexed { columnIndex, _ ->
            if (entry.columns.size <= columnIndex) {
                entry.columns += mutableListOf<Cell>()
            }
        }
    }

    repeat(2) {
        items.asReversed().forEach {
            (it as? AbstractContainer)?.invalidateLayout()
        }

        items.forEach {
            (it as? AbstractContainer)?.invalidateLayout()
        }
    }

    table.forEachIndexed { rowIndex, rowElements ->
        rowElements.forEachIndexed { columnIndex, element ->
            val cell = Cell(Vector2i(rowIndex, columnIndex), element)
            entry.columns[columnIndex] += cell
            entry.rows[rowIndex] += cell
        }
    }

    entry.columns.forEach { column ->
        val minSize = getMinSizeFor(column)
        column.forEach {
            it.minSizeByColumn = minSize
        }
    }

    entry.rows.forEach { row ->
        val minSize = getMinSizeFor(row)
        row.forEach {
            it.minSizeByRow = minSize
        }
    }

    val align = if (rowAlignCentered) {
        Layout.HORIZONTAL_CENTER
    } else {
        OrientationLayout(Orientation.HORIZONTAL, AlignMode(Align.MIN, Align.CENTER))
    }
    return Container(
        Layout.VERTICAL,
        entry.rows.map { row ->
            Container(
                align,
                row.map {
                    if (independentSize) {
                        it.element
                    } else {
                        val width = it.minSizeByColumn.x
                        val height = it.minSizeByRow.y
                        Container(Layout.DEFAULT, it.element).setSizeMode(SizeMode.fixed(Vector2f(width, height)))
                    }

                }.wrapNodes()
            )
        }.wrapNodes()
    )
}

internal class TableBuilder() {
    val columns = mutableListOf<MutableList<Cell>>()
    val rows = mutableListOf<MutableList<Cell>>()
}

internal class Cell(val key: Vector2i, val element: Element) {
    val minSize = SizeCalculator.getSize(element.placement, Vector2f.ZERO, Vector2f.ZERO)

    var minSizeByColumn = Vector2f.ZERO
    var minSizeByRow = Vector2f.ZERO
}


private fun getMinSizeFor(cells: List<Cell>): Vector2f {
    val minSizeList = cells.map {
        it.minSize
    }
    return minSizeList.reduceOrNull { a, b -> a.upper(b) } ?: Vector2f.ZERO
}
