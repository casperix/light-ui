package ru.casperix.light_ui.component.slider

import ru.casperix.light_ui.component.text.SkinnedElement
import ru.casperix.light_ui.element.ElementDrawer
import ru.casperix.light_ui.element.ElementInput
import ru.casperix.light_ui.node.DrawContext
import ru.casperix.light_ui.skin.SkinProvider
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.geometry.grow
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.toQuad
import ru.casperix.renderer.Renderer2D
import ru.casperix.renderer.material.SimpleMaterial
import kotlin.time.Duration

class Slider(skin: SliderSkin = SkinProvider.skin.sliderSkin) :
    SkinnedElement<SliderSkin>(skin), ElementDrawer {

    val logic = SliderLogic()
    var value: Float
        get() = logic.valueSignal.value
        set(value) = logic.valueSignal.set(value)

    val selectSignal get() = logic.valueSignal
    val touchSignal get() = logic.touchSignal

    override val input: ElementInput = logic
    override var drawer = this

    init {
        placement.borders = skin.borders
    }

    override fun draw(renderer: Renderer2D, context: DrawContext, tick: Duration) {
        val size = context.node.placement.viewportSize
        val factor = selectSignal.value.coerceIn(0f..1f)

        val backShape = Box2f.byDimension(Vector2f.ZERO, size).toQuad().convert {
            context.worldMatrix.transform(it)
        }
        val progressShape = Box2f.byDimension(Vector2f.ZERO, size.yAxis + size.xAxis * factor).toQuad().convert {
            context.worldMatrix.transform(it)
        }

        renderer.draw {
            skin.backArea.body?.apply {
                addQuad(color, backShape.grow(grow))
            }
            skin.backArea.contour?.apply {
                addQuadContour(color, backShape.grow(grow), thick)
            }

            skin.progressArea.body?.apply {
                addQuad(color, progressShape.grow(grow))
            }
            skin.progressArea.contour?.apply {
                addQuadContour(color, progressShape.grow(grow), thick)
            }
        }


    }

}