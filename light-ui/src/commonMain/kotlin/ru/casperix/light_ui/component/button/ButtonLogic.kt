package ru.casperix.light_ui.component.button

import ru.casperix.input.*
import ru.casperix.light_ui.element.ElementInput
import ru.casperix.light_ui.node.InputContext
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.signals.concrete.EmptySignal
import ru.casperix.signals.concrete.Signal


class ButtonLogic(val state: ButtonState) : ElementInput {
    val clickEvent = EmptySignal()
    val stateEvent = Signal<ButtonState>()

    private var buttonPivot:Vector2f? = null

    override fun input(event: InputEvent, context: InputContext) = state.run {

        if (!enabled) {
            return
        }
        if (event !is PointerEvent) {
            return
        }

        val hasIntersection = context.hasIntersection(event)

        if (event is MouseMove) {
            focused = hasIntersection
            stateEvent.set(state)
        }

        if (event is PointerDown && hasIntersection && !event.captured) {
            buttonPivot = context.worldShape.v0
            pressed = true
            stateEvent.set(state)
        }

        if (hasIntersection) {
            event.captured = true
        }

        if (event is PointerUp) {
            val isButtonMove = buttonPivot != context.worldShape.v0
            if (!isButtonMove && hasIntersection) {
                click()
            }

            buttonPivot = null
            pressed = false
            stateEvent.set(state)
        }
    }

    fun click() {
        state.checked = !state.checked
        stateEvent.set(state)
        clickEvent.set()
    }
}