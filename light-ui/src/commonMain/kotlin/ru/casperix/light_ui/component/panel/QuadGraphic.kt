package ru.casperix.light_ui.component.panel

import ru.casperix.light_ui.element.ElementDrawer
import ru.casperix.light_ui.node.DrawContext
import ru.casperix.math.geometry.grow
import ru.casperix.renderer.Renderer2D
import ru.casperix.renderer.material.SimpleMaterial
import kotlin.time.Duration


class QuadGraphic(val skin: QuadSkin) : ElementDrawer {

    override fun draw(renderer: Renderer2D, context: DrawContext, tick: Duration) {
        val worldShape = context.worldShape

        renderer.draw {
            skin.body?.apply {
                addQuad(color, worldShape.grow(grow))
            }
            skin.contour?.apply {
                addQuadContour(color, worldShape.grow(grow), thick)
            }
        }

    }

}


