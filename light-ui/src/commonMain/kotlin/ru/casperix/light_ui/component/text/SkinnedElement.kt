package ru.casperix.light_ui.component.text

import ru.casperix.light_ui.element.AbstractElement
import ru.casperix.light_ui.element.SizeMode
import ru.casperix.signals.concrete.Signal

abstract class SkinnedElement<CustomSkin>(initial: CustomSkin, sizeMode: SizeMode = SizeMode.layout) : AbstractElement(sizeMode) {
    protected val skinChanged = Signal<CustomSkin>()

    var skin: CustomSkin = initial
        set(value) {
            field = value
            skinChanged.set(value)
        }
}