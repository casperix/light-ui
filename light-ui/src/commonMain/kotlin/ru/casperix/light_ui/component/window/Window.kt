package ru.casperix.light_ui.component.window

import ru.casperix.light_ui.component.panel.Panel
import ru.casperix.light_ui.component.text.Label
import ru.casperix.light_ui.skin.SkinProvider
import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.node.Node
import ru.casperix.light_ui.util.wrapNodes
import ru.casperix.light_ui.element.*

class Window(val title: Label, contentLayout: Layout, children: List<Node>) : AbstractElement(SizeMode.layout), AbstractContainer {

    constructor(title: String, contentLayout: Layout, vararg children: Element?)
            : this(Label(title), contentLayout, children.filterNotNull().wrapNodes())

    constructor(title: String, contentLayout: Layout, vararg children: Node?)
            : this(Label(title), contentLayout, children.filterNotNull())

    constructor(title: String, contentLayout: Layout, children: List<Element>)
            : this(Label(title), contentLayout, children.wrapNodes())

    constructor(contentLayout: Layout, children: List<Element>)
            : this("", contentLayout, children)


    val content = Container(contentLayout)

    private val skin = SkinProvider.skin.window

    private val titleContainer = Panel(Layout.VERTICAL_CENTER, title).apply { skin = SkinProvider.skin.panelTitle }
    private val contentContainer = Panel(Layout.VERTICAL_CENTER, content)

    override val layout = Layout.VERTICAL
    override val children = listOf(titleContainer, contentContainer).wrapNodes()

    init {
        titleContainer.setSizeMode(SizeMode(MaxContentOrLayoutDimension, FixedDimension(skin.titleHeight)))
        contentContainer.setSizeMode(SizeMode.max)

        title.skin = skin.titleText
        title.setSizeMode(SizeMode.max)

        content.setSizeMode(SizeMode.max)
        content.placement.borders = skin.contentBorder
        content.children += children
    }


    fun setTitle(value: String) {
        title.text = value
    }

}