package ru.casperix.light_ui.component.panel

import ru.casperix.input.InputEvent
import ru.casperix.input.PointerEvent
import ru.casperix.light_ui.component.text.SkinnedElement
import ru.casperix.light_ui.skin.SkinProvider
import ru.casperix.light_ui.element.AbstractContainer
import ru.casperix.light_ui.element.Element
import ru.casperix.light_ui.element.ElementInput
import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.node.InputContext
import ru.casperix.light_ui.node.Node
import ru.casperix.light_ui.util.wrapNodes

class Panel(
    override val layout: Layout = Layout.DEFAULT,
    skin: QuadSkin = SkinProvider.skin.panelDefault,
) : SkinnedElement<QuadSkin>(skin), AbstractContainer, ElementInput {

    override val children = mutableListOf<Node>()
    override var drawer = QuadGraphic(skin)
    override val input = this

    
    init {
        skinChanged.then {
            drawer = QuadGraphic(it)
        }
    }

    constructor(layout: Layout = Layout.DEFAULT, children: List<Node>)
            : this(layout) {
        this.children += children
    }

    constructor(layout: Layout = Layout.DEFAULT, vararg children: Node)
            : this(layout, children.toList())

    constructor(layout: Layout = Layout.DEFAULT, vararg children: Element)
            : this(layout) {
        this.children += children.toList().wrapNodes()
    }

    override fun input(event: InputEvent, context: InputContext) {
        if (event !is PointerEvent) return
        if (context.hasIntersection(event)) {
            event.captured = true
        }
    }

}