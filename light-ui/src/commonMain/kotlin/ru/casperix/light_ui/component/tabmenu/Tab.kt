package ru.casperix.light_ui.component.tabmenu

import ru.casperix.light_ui.component.button.ToggleButton
import ru.casperix.light_ui.element.Element

class Tab(val button: ToggleButton, val content: Element) {
    constructor(label: String, content: Element) : this(ToggleButton(label), content)
}