package ru.casperix.light_ui.component.button.graphic

import ru.casperix.light_ui.component.button.AbstractButton
import ru.casperix.light_ui.component.button.ButtonState
import ru.casperix.light_ui.component.button.ToggleButtonSkin
import ru.casperix.light_ui.node.DrawContext
import ru.casperix.renderer.Renderer2D
import kotlin.time.Duration

class ToggleButtonGraphic(val skin: ToggleButtonSkin, val state: ButtonState) :
    ButtonGraphic() {
    override fun draw(renderer: Renderer2D, context: DrawContext, tick: Duration) {
        val view = AbstractButton.getViewState(state)
        val map = if (state.checked) skin.checked else skin.unchecked
        val viewSkin = map[view] ?: return
        drawState(skin.contourThick, viewSkin, renderer, context, tick)
    }
}