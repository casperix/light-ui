package ru.casperix.light_ui.component.progressbar

import ru.casperix.light_ui.skin.SkinProvider
import ru.casperix.light_ui.element.AbstractElement
import ru.casperix.light_ui.element.ElementDrawer
import ru.casperix.light_ui.element.SizeMode
import ru.casperix.light_ui.node.DrawContext
import ru.casperix.light_ui.util.transform
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.geometry.builder.BorderMode
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.toQuad
import ru.casperix.misc.clamp
import ru.casperix.renderer.Renderer2D
import ru.casperix.renderer.material.SimpleMaterial
import kotlin.time.Duration

class ProgressBar(val skin: ProgressBarSkin = SkinProvider.skin.progressBar) : AbstractElement(SizeMode.fixed(skin.defaultSize)), ElementDrawer {
    var progress = 0.5f

    override val drawer: ElementDrawer = this

    override fun draw(renderer: Renderer2D, context: DrawContext, tick: Duration) = context.run {

        val size = placement.viewportSize
        val backArea = Box2f(Vector2f.ZERO, size)

        val border = skin.borderThick
        val bodyDim = Vector2f.ZERO.upper(size - Vector2f(border * 2f))
        val bodyArea =
            Box2f.byDimension(Vector2f(border), Vector2f(bodyDim.x * progress.clamp(0f, 1f), bodyDim.y))

        renderer.draw {
            addPolygonWithContour(skin.backColor, skin.borderColor, backArea.toQuad(), skin.borderThick, BorderMode.INSIDE, worldMatrix)
            addRect(skin.bodyColor, bodyArea, worldMatrix)
        }
    }
}

