package ru.casperix.light_ui.component.slider

import ru.casperix.input.InputEvent
import ru.casperix.input.PointerDown
import ru.casperix.input.PointerEvent
import ru.casperix.input.PointerUp
import ru.casperix.light_ui.element.ElementInput
import ru.casperix.light_ui.node.InputContext
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.signals.concrete.StorageSignal

class SliderLogic : ElementInput {
    val valueSignal = StorageSignal(0f)
    val touchSignal = StorageSignal(false)

    override fun input(event: InputEvent, context: InputContext) {

        if (event !is PointerEvent) return

        if (event is PointerDown && context.hasIntersection(event)) {
            event.captured = true
            touchSignal.set(true)
        }

        if (event is PointerUp) {
            event.captured = true
            touchSignal.set(false)
        }

        if (touchSignal.value) {
            event.captured = true
            val localPosition = context.worldMatrix.inverse().transform(event.position)

            val normalized = (localPosition / context.node.placement.viewportSize).clamp(Vector2f.ZERO, Vector2f.ONE)
            valueSignal.set(normalized.x)
        }
    }
}