package ru.casperix.light_ui.component.button.graphic

import ru.casperix.light_ui.component.button.AbstractButton
import ru.casperix.light_ui.component.button.ButtonState
import ru.casperix.light_ui.component.button.PushButtonSkin
import ru.casperix.light_ui.node.DrawContext
import ru.casperix.renderer.Renderer2D
import kotlin.time.Duration


class PushButtonGraphic(val skin: PushButtonSkin, val state: ButtonState) : ButtonGraphic() {
    override fun draw(renderer: Renderer2D, context: DrawContext, tick: Duration) {
        val viewState = AbstractButton.getViewState(state)
        val viewSkin = skin.map[viewState] ?: return
        drawState(skin.contourThick, viewSkin, renderer, context, tick)
    }


}

