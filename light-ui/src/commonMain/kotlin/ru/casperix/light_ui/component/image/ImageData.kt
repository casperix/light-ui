package ru.casperix.light_ui.component.image

import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.renderer.vector.VectorGraphic2D

data class ImageData(val graphic: VectorGraphic2D, val size: Vector2f) {
    companion object {
        val DUMMY = ImageData(VectorGraphic2D.EMPTY, Vector2f(100f))
    }
}