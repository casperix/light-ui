package ru.casperix.light_ui.component.misc

import ru.casperix.input.InputEvent
import ru.casperix.input.PointerDown
import ru.casperix.input.PointerEvent
import ru.casperix.input.PointerUp
import ru.casperix.light_ui.element.ElementInput
import ru.casperix.light_ui.element.ElementPlacement
import ru.casperix.light_ui.node.InputContext
import ru.casperix.math.vector.float32.Vector2f

class DragWithPointer(val placement: ElementPlacement, val moveConsumer: (Vector2f) -> Unit, val dragStop:()->Unit = {}, val canMoveCaptured:Boolean = true)  :
    ElementInput {
    private var lastPosition: Vector2f? = null

    val isDragging get() = lastPosition != null

    var customIntersection: (ElementPlacement, Vector2f) -> Boolean = { placement, mouse ->
        mouse.greaterOrEq(Vector2f.ZERO) && mouse.lessOrEq(placement.viewportSize)
    }


    override fun input(event: InputEvent, context: InputContext) = context.run {
        if (event !is PointerEvent) return

        val localPosition = worldMatrix.inverse().transform(event.position)

        if (event is PointerDown) {
            val hasIntersection = customIntersection.invoke(placement, localPosition)
            if (hasIntersection && (canMoveCaptured || !event.captured)) {
                event.captured = true
                start(localPosition)
            }
        } else if (event is PointerUp) {
            finish(localPosition)
        } else {
            resume(localPosition)
        }
    }


    private fun start(position: Vector2f) {
        lastPosition = position
    }

    private fun finish(position: Vector2f) {
        lastPosition?.let {
            setMove(position - it)
            lastPosition = null
            dragStop()
        }
    }

    private fun resume(position: Vector2f) {
        lastPosition?.let {
            setMove(position - it)
            lastPosition = position
        }
    }

    private fun setMove(offset: Vector2f) {
        if (offset == Vector2f.ZERO) return
        moveConsumer(offset)
    }
}