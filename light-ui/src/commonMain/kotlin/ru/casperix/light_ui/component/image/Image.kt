package ru.casperix.light_ui.component.image

import ru.casperix.light_ui.component.panel.QuadGraphic
import ru.casperix.light_ui.component.text.SkinnedElement
import ru.casperix.light_ui.element.ElementDrawer
import ru.casperix.light_ui.element.ElementWithLayout
import ru.casperix.light_ui.element.SizeMode
import ru.casperix.light_ui.node.DrawContext
import ru.casperix.light_ui.skin.SkinProvider
import ru.casperix.math.angle.float32.DegreeFloat
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.renderer.Renderer2D
import ru.casperix.renderer.material.SimpleMaterial
import ru.casperix.renderer.material.Texture2D
import ru.casperix.renderer.material.TextureConfig
import ru.casperix.renderer.material.TextureWrap
import ru.casperix.renderer.pixel_map.PixelMap
import ru.casperix.renderer.vector.VectorGraphic2D
import ru.casperix.renderer.vector.builder.VectorGraphicBuilder2D
import ru.casperix.renderer.vector.builder.VectorGraphicBuilder3D
import kotlin.time.Duration


class Image() : SkinnedElement<ImageSkin>(SkinProvider.skin.image, SizeMode.content), ElementDrawer, ElementWithLayout {

    constructor(pixelMap: PixelMap) : this() {
        setPixelMap(pixelMap)
    }

    override var drawer: ElementDrawer = this

    var data: ImageData = ImageData.DUMMY

    var scale = 1f
    var rotate = DegreeFloat.ZERO

    init {
        skinChanged.then {
            placement.borders = it.borders
        }
    }

    fun setPixelMap(pixelMap: PixelMap) = apply {
        val size = pixelMap.dimension.toVector2f()
        val shape = Box2f.byDimension(Vector2f.ZERO, size)
        val graphic = VectorGraphicBuilder2D.build {
            add(SimpleMaterial(Texture2D(pixelMap, TextureConfig(uWrap = TextureWrap.CLAMP, vWrap = TextureWrap.CLAMP)))) {
                addRect(shape, Box2f.ONE)
            }
        }

        setGraphic(graphic, size)
    }

    fun setGraphic(graphic: VectorGraphic2D, size: Vector2f) {
        data = ImageData(graphic, size)

    }

    override fun invalidateLayout() {
        placement.contentArea = Box2f(Vector2f.ZERO, data.size * scale)
    }


    override fun draw(renderer: Renderer2D, context: DrawContext, tick: Duration) {

        skin.back?.let {
            QuadGraphic(it).draw(renderer, context, tick)
        }

        val data = data
        if (data.size.volume() == 0f) return

        val centerOffset = Vector2f.ZERO


        val imageMatrix = if (scale == 1f && rotate == DegreeFloat.ZERO) {
            Matrix3f.translate(centerOffset) *
                    context.worldMatrix
        } else {
            val rotateOffset = data.size / 2f

            Matrix3f.translate(-rotateOffset) *
                    Matrix3f.rotate(rotate) *
                    Matrix3f.translate(rotateOffset) *
                    Matrix3f.translate(centerOffset) *
                    Matrix3f.scale(Vector2f(scale)) *
                    context.worldMatrix
        }
        renderer.draw(data.graphic, imageMatrix)
    }
}

