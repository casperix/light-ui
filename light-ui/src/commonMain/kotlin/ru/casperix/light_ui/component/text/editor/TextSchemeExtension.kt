package ru.casperix.light_ui.component.text.editor

import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.multiplatform.text.impl.TextScheme
import ru.casperix.multiplatform.text.impl.TextSchemeElement

object TextSchemeExtension {
    class SymbolPosition(val container: TextSchemeElement, val charIndex: Int, val leftTop: Vector2f)

    fun getSymbols(textScheme: TextScheme, addStopSymbol: Boolean = false): List<SymbolPosition> {
        var charIndex = 0
        val output = textScheme.elements.flatMapIndexed { index, element ->
            (element.stringMetrics.advances).map { xOffset ->
                SymbolPosition(element, charIndex++, element.textArea.min + Vector2f(xOffset, 0f))
            }
        }

        if (addStopSymbol) {
            textScheme.elements.lastOrNull()?.let {
                return output + SymbolPosition(it, charIndex++, it.textArea.min + Vector2f(it.textArea.dimension.x, 0f))
            }
        }

        return output
    }


}