package ru.casperix.light_ui.component.button.graphic

import ru.casperix.light_ui.component.button.ButtonViewSkin
import ru.casperix.light_ui.element.ElementDrawer
import ru.casperix.light_ui.node.DrawContext
import ru.casperix.light_ui.util.Transition
import ru.casperix.math.geometry.builder.BorderMode
import ru.casperix.math.interpolation.float32.InterpolationFloat
import ru.casperix.math.interpolation.float32.linearInterpolatef
import ru.casperix.renderer.Renderer2D
import kotlin.time.Duration

abstract class ButtonGraphic : ElementDrawer {

    private val transition = Transition(::interpolate)

    fun drawState(contourThick: Float, target: ButtonViewSkin, renderer: Renderer2D, context: DrawContext, tick: Duration) {
        val interpolated = transition.calculate(target, target.timeForSwitch, tick)
        renderer.draw {
            addPolygonWithContour(
                interpolated.bodyColor,
                interpolated.contourColor,
                context.worldShape,
                contourThick,
                BorderMode.INSIDE
            )
        }
    }

    private fun interpolate(a: ButtonViewSkin, b: ButtonViewSkin, t: Float): ButtonViewSkin {
        val interpolator = linearInterpolatef
        return ButtonViewSkin(
            b.timeForSwitch,
            InterpolationFloat.color(a.bodyColor, b.bodyColor, t, interpolator),
            InterpolationFloat.color(a.contourColor, b.contourColor, t, interpolator),
        )
    }


}