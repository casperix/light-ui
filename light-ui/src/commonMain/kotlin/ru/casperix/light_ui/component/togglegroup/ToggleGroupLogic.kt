package ru.casperix.light_ui.component.togglegroup

import ru.casperix.light_ui.component.button.ToggleButton

class ToggleGroupLogic(val mode: ToggleGroupSelectionMode, val buttons: List<ToggleButton>) {
    private var selfClick = false

    init {
        buttons.map {
            it.clickEvent.then {
                buttonClick(it)
            }
        }

        val checkCandidate = buttons.firstOrNull { it.isChecked } ?: buttons.firstOrNull()
        if (checkCandidate != null) {
            checkCandidate.isChecked = true
            validateCheck(checkCandidate)
        }
    }

    private fun buttonClick(self: ToggleButton) {
        if (selfClick) return

        selfClick = true
        validateCheck(self)
        selfClick = false
    }

    private fun validateCheck(initiator: ToggleButton) {
        if (initiator.isChecked) {
            buttons.forEach {
                it.isChecked = it == initiator
            }
        } else {
            buttons.forEach {
                it.isChecked = it == initiator && mode == ToggleGroupSelectionMode.ALWAYS_ONE
            }
        }
    }
}