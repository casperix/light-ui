package ru.casperix.light_ui.component.text.editor

import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.multiplatform.font.FontMetrics
import ru.casperix.multiplatform.font.FontReference
import ru.casperix.multiplatform.text.impl.TextScheme
import ru.casperix.multiplatform.text.textRenderer

object TextEditorHelper {

    val BACKSPACE = 8.toChar()
    val CARRIAGE_RETURN = '\r'
    val NEWLINE = '\n'
    val TAB = '\t'
    val DELETE = 127.toChar()
    val SPECIAL_SYMBOLS = listOf(BACKSPACE, CARRIAGE_RETURN, NEWLINE, TAB, DELETE)

    fun getCursorArea(textScheme: TextScheme, cursorIndex: Int, cursorThick: Float, defaultFont: FontReference, defaultSize:Vector2f): Box2f? {
        val symbolList = TextSchemeExtension.getSymbols(textScheme, true)
        val symbol = symbolList.getOrNull(cursorIndex) ?: run {
            val metrics:FontMetrics = textRenderer.getFontMetrics(defaultFont)

            return Box2f.byRadius(defaultSize / 2f, Vector2f(cursorThick, metrics.textHeight) / 2f)
        }

        return Box2f.byDimension(
            symbol.leftTop - Vector2f(cursorThick / 2f),
            Vector2f(cursorThick, symbol.container.textArea.dimension.y)
        )
    }

    fun getNearIndex(textScheme: TextScheme, pointer: Vector2f): Int? {
        val symbolList = TextSchemeExtension.getSymbols(textScheme, true)

        val nearSymbol = symbolList.minByOrNull {
            val center = it.leftTop + it.container.textArea.dimension.yAxis / 2f
            center.distTo(pointer)
        } ?: return null

        return nearSymbol.charIndex
    }
}