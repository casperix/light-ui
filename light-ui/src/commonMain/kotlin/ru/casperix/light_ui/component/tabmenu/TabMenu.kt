package ru.casperix.light_ui.component.tabmenu

import ru.casperix.light_ui.component.togglegroup.ToggleGroup
import ru.casperix.light_ui.component.togglegroup.ToggleGroupSelectionMode
import ru.casperix.light_ui.element.*
import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.layout.LayoutSide
import ru.casperix.light_ui.layout.orientation_layout.OrientationLayout
import ru.casperix.light_ui.node.Node
import ru.casperix.light_ui.types.Orientation
import ru.casperix.renderer.misc.AlignMode


class TabMenu(val buttonSide: LayoutSide, val tabs: List<Tab>) : AbstractElement(SizeMode.content), AbstractContainer {
    private val tabNode = run {
        val tabButtonsLayout = when (buttonSide) {
            LayoutSide.LEFT, LayoutSide.RIGHT -> Layout.VERTICAL
            LayoutSide.TOP, LayoutSide.BOTTOM -> Layout.HORIZONTAL
        }

        val tabButtons = tabs.map { it.button }
        Node(ToggleGroup(tabButtonsLayout, ToggleGroupSelectionMode.ALWAYS_ONE, tabButtons))
    }

    private val contentNode = Node(Empty())

    override val layout = run {
        val buttonOrientation = when (buttonSide) {
            LayoutSide.LEFT, LayoutSide.RIGHT -> Orientation.HORIZONTAL
            LayoutSide.TOP, LayoutSide.BOTTOM -> Orientation.VERTICAL
        }

        OrientationLayout(buttonOrientation, AlignMode.LEFT_TOP)
    }

    override val children = listOf(
        tabNode,
        contentNode,
    ).run {
        if (buttonSide == LayoutSide.BOTTOM || buttonSide == LayoutSide.RIGHT) {
            asReversed()
        } else {
            this
        }
    }

    init {
        tabs.map {
            it.button.clickEvent.then {
                setContent(it.content)
            }
        }
        tabs.forEach {
            if (it.button.isChecked) {
                setContent(it.content)
            }
        }
    }

    private fun setContent(content: Element) {
        this.contentNode.element = content
//        content.placement.sizeMode = SizeMode.min
    }
}