package ru.casperix.light_ui.component.slider

import ru.casperix.light_ui.component.button.PushButtonSkin
import ru.casperix.light_ui.component.misc.SideIndents
import ru.casperix.light_ui.component.panel.QuadSkin

class SliderSkin(val thumb:PushButtonSkin?, val backArea: QuadSkin, val progressArea: QuadSkin, val borders:SideIndents)