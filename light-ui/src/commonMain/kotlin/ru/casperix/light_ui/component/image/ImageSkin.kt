package ru.casperix.light_ui.component.image

import ru.casperix.light_ui.component.misc.SideIndents
import ru.casperix.light_ui.component.panel.QuadSkin

data class ImageSkin(val borders: SideIndents, val back: QuadSkin?)