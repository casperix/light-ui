package ru.casperix.light_ui.component.text.editor

import ru.casperix.app.surface.SurfaceProvider
import ru.casperix.app.surface.component.VirtualKeyboardApi
import ru.casperix.light_ui.component.text.TextEditorSkin
import ru.casperix.light_ui.element.*
import ru.casperix.light_ui.node.DrawContext
import ru.casperix.light_ui.skin.SkinProvider
import ru.casperix.light_ui.util.betweenInclusive
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.color.Color
import ru.casperix.math.color.rgb.RgbColor
import ru.casperix.math.color.rgba.RgbaColor
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.misc.clamp
import ru.casperix.misc.sliceSafe
import ru.casperix.multiplatform.text.drawText
import ru.casperix.multiplatform.text.impl.TextScheme
import ru.casperix.multiplatform.text.impl.TextView
import ru.casperix.multiplatform.text.textRenderer
import ru.casperix.renderer.Renderer2D
import ru.casperix.renderer.material.SimpleMaterial
import ru.casperix.renderer.misc.AlignMode
import ru.casperix.signals.concrete.Signal
import kotlin.time.Duration

class TextEditor(var text: String = "", val alignMode: AlignMode = AlignMode.CENTER_CENTER) : AbstractElement(SizeMode.fixed(Vector2f(100f))),
    ElementDrawer, ElementWithLayout {
    val onEnter = Signal<String>()

    var allowedSymbols: String? = null
    var prohibitedSymbols: String? = null
    var skin: TextEditorSkin = SkinProvider.skin.textEditor
    var maxLength: Int? = null

    private var isAcitve = false

    //    private var node: Node? = null
    private var cursorIndex: Int? = null
    private var selectionIndices: Pair<Int, Int>? = null
    private var textScheme = TextScheme.EMPTY

    private val cursorAnimation = TextCursorAnimation()

    override val drawer = this
    override var input: ElementInput = TextEditorInput(this)

    override fun invalidateLayout() {
        invalidateTextScheme()
        placement.contentArea = textScheme.summaryArea.value
    }

    fun getSelectionRange(): IntRange? {
        return selectionIndices?.run {
            if (first != second) {
                IntRange.betweenInclusive(first, second)
            } else null
        }
    }

    fun getSelection(): String? {
        val range = getSelectionRange() ?: return null
        return text.sliceSafe(range)
    }

    private fun invalidateTextScheme() {
        val selectionRange = getSelectionRange()
        val viewList = if (selectionRange == null) {
            listOf(Pair(text, false))
        } else {
            listOf(
                Pair(text.sliceSafe(0 until selectionRange.first), false),
                Pair(text.sliceSafe(selectionRange), true),
                Pair(text.sliceSafe(selectionRange.last + 1 until text.length), false),
            )
        }.map { (text, isSelection) ->
            TextView(text, skin.label.font, skin.label.color, if (isSelection) skin.selectionColor else null)
        }

        textScheme = textRenderer.getTextScheme(viewList, placement.viewportSize, alignMode)
    }

    override fun draw(renderer: Renderer2D, context: DrawContext, tick: Duration): Unit = context.run {
        renderer.drawText(textScheme, worldMatrix)

        if (isAcitve) {
            getCursorArea()?.let {
                if (cursorAnimation.animate(tick)) {
                    renderer.draw {
                        addRect(Color.WHITE, it, worldMatrix)
                    }
                }
            }
        }
    }

    private fun getCursorArea(): Box2f? {
        val cursorIndex = cursorIndex ?: return null
        return TextEditorHelper.getCursorArea(textScheme, cursorIndex, skin.cursorThick, skin.label.font, placement.viewportSize)
    }


    fun getTextScheme(): TextScheme {
        return textScheme
    }

    fun getActive(): Boolean {
        return isAcitve
    }

    fun setActive(value: Boolean) {
        val isChanged = isAcitve != value
        isAcitve = value
        if (isAcitve) {
            cursorIndex = null
            selectionIndices = null
        }

        if (isChanged) {
            (SurfaceProvider.surface as? VirtualKeyboardApi)?.let {
                if (isAcitve) {
                    it.showVirtualKeyboard()
                } else {
                    it.hideVirtualKeyboard()
                }
            }
        }
    }

    fun setSelection(next: Pair<Int, Int>?) {
        selectionIndices = next
    }

    fun setSelectionGrow(nextIndex: Int?) {
        val lastCursor = cursorIndex
        if (lastCursor == nextIndex) {
            return
        }
        if (nextIndex == null || lastCursor == null) {
            selectionIndices = null
            return
        }

        val lastSelection = selectionIndices
        val selectionFrom = lastSelection?.first ?: lastCursor

        selectionIndices = Pair(selectionFrom, nextIndex)
        cursorIndex = nextIndex
    }

    fun removeBackward(): Boolean {
        if (removeSelected()) return true

        val cursorIndex = cursorIndex ?: return false
        val lastText = text
        if (cursorIndex <= 0) return true
        setupText(lastText.sliceSafe(0 until cursorIndex - 1) + lastText.sliceSafe(cursorIndex..lastText.length))
        setCursorPosition(cursorIndex - 1)
        return true
    }

    fun removeForward(): Boolean {
        if (removeSelected()) return true

        val cursorIndex = cursorIndex ?: return false

        val lastText = text
        if (cursorIndex >= lastText.length) return true
        setupText(lastText.sliceSafe(0 until cursorIndex) + lastText.sliceSafe(cursorIndex + 1..lastText.length))
        return true
    }

    fun removeSelected(): Boolean {
        val selectionRange = getSelectionRange() ?: return false

        setupText(text.sliceSafe(0 until selectionRange.first) + text.sliceSafe((selectionRange.last + 1) until text.length))
        selectionIndices = null
        setCursorPosition(selectionRange.first)
        return true
    }

    fun inputText(input: String) {
        val inputFiltered = filterText(input)
        if (inputFiltered.isEmpty()) return

        removeSelected()

        val lastCursorIndex = cursorIndex ?: 0
        val lastText = text
        val nextText =
            lastText.sliceSafe(0 until lastCursorIndex) + inputFiltered + lastText.sliceSafe(lastCursorIndex..lastText.length)

        setupText(nextText)
        setCursorPosition((cursorIndex ?: 0) + inputFiltered.length)
    }

    fun getCursorPosition(): Int? {
        return cursorIndex
    }

    fun setCursorPosition(index: Int?) {
        cursorAnimation.reset()
        cursorIndex = index?.clamp(0, text.length + 1)
    }

    private fun setupText(value: String) {
        var next = value
        maxLength?.let {
            next = next.take(it)
        }
        text = next
    }


    private fun filterText(inputSymbols: String): String {
        val allowedSymbols = allowedSymbols
        val prohibitedSymbols = prohibitedSymbols

        return if (allowedSymbols == null && prohibitedSymbols == null) {
            inputSymbols
        } else {
            inputSymbols.filter {
                (allowedSymbols != null && allowedSymbols.contains(it)) || (prohibitedSymbols != null && !prohibitedSymbols.contains(
                    it
                ))
            }
        }
    }

}
