package ru.casperix.light_ui.component.togglegroup

import ru.casperix.light_ui.component.button.ToggleButton
import ru.casperix.light_ui.component.panel.QuadGraphic
import ru.casperix.light_ui.element.AbstractContainer
import ru.casperix.light_ui.element.AbstractElement
import ru.casperix.light_ui.element.SizeMode
import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.node.Node
import ru.casperix.light_ui.skin.SkinProvider

class ToggleGroup(
    override val layout: Layout,
    val mode: ToggleGroupSelectionMode,
    val toggles: List<ToggleButton>,
) : AbstractElement(SizeMode.content), AbstractContainer {

    constructor(layout: Layout, mode: ToggleGroupSelectionMode, vararg toggles: ToggleButton) : this(layout, mode, toggles.toList())

    override val children = toggles.map(::Node)
    override val drawer = QuadGraphic(SkinProvider.skin.panelDefault)
    private val logic = ToggleGroupLogic(mode, toggles)
}

