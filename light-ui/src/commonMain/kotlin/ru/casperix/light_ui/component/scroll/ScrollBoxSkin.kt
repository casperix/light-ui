package ru.casperix.light_ui.component.scroll

import ru.casperix.light_ui.component.panel.QuadSkin
import kotlin.time.Duration

data class ScrollBoxSkin(val scrollReverseTime:Duration, val back: QuadSkin?, val front: QuadSkin?)