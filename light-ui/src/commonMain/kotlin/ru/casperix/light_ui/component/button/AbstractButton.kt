package ru.casperix.light_ui.component.button

import ru.casperix.light_ui.component.text.Label
import ru.casperix.light_ui.component.text.SkinnedElement
import ru.casperix.light_ui.element.AbstractContainer
import ru.casperix.light_ui.element.SizeMode
import ru.casperix.light_ui.layout.Layout

abstract class AbstractButton<CustomSkin : AbstractButtonSkin>(skin: CustomSkin) :
    SkinnedElement<CustomSkin>(skin),
    AbstractContainer {

    protected val state = ButtonState()
    final  override val input = ButtonLogic(state)

    val clickEvent = input.clickEvent

    override val layout: Layout = Layout.VERTICAL

    var isEnabled by state::enabled

    fun click() {
        input.click()
    }

    init {
        input.stateEvent.then {
            updateSkin()
        }
        skinChanged.then {
            updateSkin()
        }
    }

    private fun updateSkin() {
        placement.apply {
            sizeMode = SizeMode.fixed(skin.defaultSize)
            borders = skin.defaultBorders
        }

        children.forEach {
            val content = it.element
            if (content is Label) {
                val viewState = getViewState(state)
                content.skin = skin.labelSkin.map[viewState] ?: return
            }
        }
    }

    fun <CustomButton : AbstractButton<*>> CustomButton.setListener(
        handler: (CustomButton.() -> Unit)? = null
    ): CustomButton {
        if (handler == null) return this
        clickEvent.then { handler(this) }
        return this
    }

    companion object {
        fun getViewState(state: ButtonState): ButtonViewState {
            return if (!state.enabled) {
                ButtonViewState.Disable
            } else {
                if (state.pressed) {
                    if (state.focused) {
                        ButtonViewState.PressOver
                    } else {
                        ButtonViewState.PressOut
                    }
                } else {
                    if (state.focused) {
                        ButtonViewState.Over
                    } else {
                        ButtonViewState.Out
                    }

                }
            }
        }

    }
}