package ru.casperix.light_ui.component.button

import ru.casperix.light_ui.component.misc.SideIndents
import ru.casperix.light_ui.component.text.LabelSkin
import ru.casperix.math.color.Color
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.renderer.vector.VectorGraphic2D
import kotlin.time.Duration


interface AbstractButtonSkin {
    val labelSkin: LabelButtonSkin
    val defaultSize: Vector2f
    val defaultBorders: SideIndents
}

data class LabelButtonSkin(val defaultSize: Vector2f, val map: Map<ButtonViewState, LabelSkin>)
data class PushButtonSkin(
    override val labelSkin: LabelButtonSkin,
    override val defaultSize: Vector2f,
    override val defaultBorders: SideIndents,
    val contourThick: Float,
    val map: Map<ButtonViewState, ButtonViewSkin>
) : AbstractButtonSkin

data class ToggleButtonSkin(
    override val labelSkin: LabelButtonSkin,
    override val defaultSize: Vector2f,
    override val defaultBorders: SideIndents,

    val contourThick: Float,
    val unchecked: Map<ButtonViewState, ButtonViewSkin>,
    val checked: Map<ButtonViewState, ButtonViewSkin>
) : AbstractButtonSkin

data class CheckButtonSkin(
    override val labelSkin: LabelButtonSkin,
    override val defaultSize: Vector2f,
    override val defaultBorders: SideIndents,
    val checkBox: CheckBoxSkin,
) : AbstractButtonSkin

data class CheckBoxSkin(
    val selected:VectorGraphic2D,
    val unselected:VectorGraphic2D,
    val toggle: ToggleButtonSkin,
)

data class OpenableContainerSkin(
    val checkBox: CheckButtonSkin,
)

enum class ButtonViewState {
    PressOut,
    PressOver,
    Out,
    Over,
    Disable
}

data class ButtonViewSkin(
    val timeForSwitch: Duration,
    val bodyColor: Color,
    val contourColor: Color,
)
