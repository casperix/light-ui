package ru.casperix.light_ui.component.text.editor

import ru.casperix.input.*
import ru.casperix.light_ui.element.ElementInput
import ru.casperix.light_ui.node.InputContext
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.multiplatform.clipboard.clipboard

class TextEditorInput(val editor: TextEditor) : ElementInput {
    private var selectionPhase = false
    private var isCtrl = false
    private var isShift = false

    override fun input(event: InputEvent, context: InputContext) = context.run {
        if (event is KeyEvent) {
            if (event is KeyDown) {
                if (editor.getActive() && specialKeyDown(event.button)) {
                    event.captured = true
                }
            } else if (event is KeyUp) {
                specialKeyUp(event.button)
                event.captured = true
            } else if (event is KeyTyped) {
                if (editor.getActive() && !TextEditorHelper.SPECIAL_SYMBOLS.contains(event.char)) {
                    editor.inputText(event.char.toString())
                    event.captured = true
                }
            }
        } else if (event is PointerEvent) {
            if (event is PointerDown || event is PointerMove || event is PointerUp) {
                val localPosition = worldMatrix.inverse().transform(event.position)
                inputTouch(localPosition, event)
            }
        }
    }

    private fun inputTouch(localPosition: Vector2f, event: PointerEvent) {
        val isInside = localPosition.greaterOrEq(Vector2f.ZERO) && localPosition.lessOrEq(editor.placement.viewportSize)
        if (event is PointerDown) {
            selectionPhase = true
            editor.setActive(isInside)
        }

        if (editor.getActive() && selectionPhase) {
            val nearIndex = TextEditorHelper.getNearIndex(editor.getTextScheme(), localPosition)

            editor.setSelectionGrow(nearIndex)
            editor.setCursorPosition(nearIndex ?: 0)
        }

        if (event is PointerUp) {
            selectionPhase = false
        }
    }

    private fun specialKeyUp(button: KeyButton) {
        when (button) {
            KeyButton.CONTROL_LEFT, KeyButton.CONTROL_RIGHT -> {
                isCtrl = false
            }

            KeyButton.SHIFT_LEFT, KeyButton.SHIFT_RIGHT -> {
                isShift = false
            }

            else -> {

            }
        }
    }

    private fun specialKeyDown(button: KeyButton): Boolean {

        return when (button) {
            KeyButton.CONTROL_LEFT, KeyButton.CONTROL_RIGHT -> {
                isCtrl = true
                true
            }

            KeyButton.SHIFT_LEFT, KeyButton.SHIFT_RIGHT -> {
                isShift = true
                true
            }

            KeyButton.C -> {
                if (isCtrl) {
                    clipboardPush()
                }
                true
            }

            KeyButton.INSERT -> {
                if (isCtrl) {
                    clipboardPush()
                } else if (isShift) {
                    clipboardPop()
                }
                true
            }

            KeyButton.V -> {
                if (isCtrl) {
                    clipboardPop()
                }
                true
            }


            KeyButton.A -> {
                if (isCtrl) {
                    editor.setSelection(Pair(0, editor.text.length))
                }
                true
            }

            KeyButton.ARROW_LEFT -> {
                val cursorIndex = editor.getCursorPosition() ?: return false
                editor.setSelection(null)
                editor.setCursorPosition(cursorIndex - 1)
                true
            }

            KeyButton.ARROW_RIGHT -> {
                val cursorIndex = editor.getCursorPosition() ?: return false
                editor.setSelection(null)
                editor.setCursorPosition(cursorIndex + 1)
                true
            }

            KeyButton.ESCAPE -> {
                editor.setActive(false)
                true
            }

            KeyButton.ENTER, KeyButton.NUMPAD_ENTER -> {
                editor.onEnter.set(editor.text)
                true
            }

            KeyButton.BACKSPACE -> {
                editor.removeBackward()
            }

            KeyButton.DELETE -> {
                editor.removeForward()
            }

            else -> {
                false
            }
        }
    }

    private fun clipboardPush() {
        editor.getSelection()?.let {
            clipboard.pushString(it)
        }
    }

    private fun clipboardPop() {
        clipboard.popString().thenAccept {
            editor.inputText(it)
        }
    }
}