package ru.casperix.light_ui.component

import ru.casperix.light_ui.component.button.ToggleButton
import ru.casperix.light_ui.component.togglegroup.ToggleGroup
import ru.casperix.light_ui.component.togglegroup.ToggleGroupSelectionMode
import ru.casperix.light_ui.layout.Layout


@Deprecated("ToggleGroup is now a class", ReplaceWith("ru.casperix.light_ui.component.togglegroup.ToggleGroup(layout, mode, children)"))
fun createToggleGroup(layout: Layout, mode: ToggleGroupSelectionMode, children: List<ToggleButton>): ToggleGroup {
    return ToggleGroup(layout, mode, children)
}

@Deprecated("use ToggleGroup instead")
fun createToggleGroup(layout: Layout, mode: ToggleGroupSelectionMode, vararg children: ToggleButton): ToggleGroup {
    return ToggleGroup(layout, mode, *children)
}