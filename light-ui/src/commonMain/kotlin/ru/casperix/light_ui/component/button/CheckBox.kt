package ru.casperix.light_ui.component.button

import ru.casperix.light_ui.component.button.graphic.CheckBoxGraphic
import ru.casperix.light_ui.component.text.Label
import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.skin.SkinProvider
import ru.casperix.light_ui.util.wrapNodes
import ru.casperix.renderer.misc.AlignMode
import ru.casperix.light_ui.element.*
import kotlin.reflect.KMutableProperty0

class CheckBox(
    val label: Label,
    isChecked: Boolean = true,
    skin: CheckButtonSkin = SkinProvider.skin.checkButton,
    handler: (CheckBox.() -> Unit)? = null,
) : AbstractButton<CheckButtonSkin>(skin) {

    constructor(
        text: String = "",
        isChecked: Boolean = true,
        skin: CheckButtonSkin = SkinProvider.skin.checkButton,
        handler: (CheckBox.() -> Unit)? = null,
    )
            : this(Label(text, alignMode = AlignMode.LEFT_CENTER), isChecked, skin, handler)

    constructor(label: String, property: KMutableProperty0<Boolean>, skin: CheckButtonSkin = SkinProvider.skin.checkButton)
            : this(label, property.get(), skin, {
        property.set(isChecked)
    })


    override val layout = Layout.HORIZONTAL_CENTER

    override val children = listOf(
        AbstractElement(SizeMode.fixed(skin.checkBox.toggle.defaultSize), CheckBoxGraphic(skin.checkBox, state)).apply {
            placement.borders = skin.checkBox.toggle.defaultBorders
        },
        label.setSizeMode(SizeMode(MaxContentOrViewDimension, ViewDimension)),
    ).wrapNodes()

    init {
        if (this.isChecked != isChecked) {
            click()
        }
        skinChanged.set(skin)
    }

    var isChecked
        get() = state.checked
        set(value) {
            if (value != state.checked) {
                click()
            }
        }

    init {
        setListener(handler)
        skinChanged.set(skin)
    }

    fun setLabel(text: String) = apply {
        label.text = text
    }

    @Deprecated("", ReplaceWith("setProperty(isChecked, handler)"))
    fun setListener(isChecked: Boolean = true, handler: CheckBox.() -> Unit) = apply {
        this.isChecked = isChecked
        setListener(handler)
    }

    @Deprecated("", ReplaceWith("setProperty(property)"))
    fun setListener(property: KMutableProperty0<Boolean>) = apply {
        setListener(property.get()) { property.set(isChecked) }
    }

    fun setChecked(isChecked: Boolean) = apply {
        this.isChecked = isChecked
    }

    fun setListener(handler: CheckBox.() -> Unit) = apply {
        clickEvent.then { handler(this) }
    }

    fun setProperty(property: KMutableProperty0<Boolean>) = apply {
        setChecked(property.get())
        setListener { property.set(isChecked) }
    }

    fun setProperty(initial: Boolean, setter: (Boolean) -> Unit) = apply {
        setChecked(initial)
        setListener { setter(isChecked) }
    }

    fun setListenerForChecked(handler: CheckBox.() -> Unit) = apply {
        clickEvent.then { if (state.checked) handler(this) }
    }


}