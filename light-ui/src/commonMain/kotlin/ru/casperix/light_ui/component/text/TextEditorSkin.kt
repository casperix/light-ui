package ru.casperix.light_ui.component.text

import ru.casperix.math.color.Color

data class TextEditorSkin(val label: LabelSkin, val selectionColor: Color = Color.BLACK, val cursorThick: Float = 2f)