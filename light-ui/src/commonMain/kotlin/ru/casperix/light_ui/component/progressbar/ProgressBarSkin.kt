package ru.casperix.light_ui.component.progressbar

import ru.casperix.math.color.Color
import ru.casperix.math.vector.float32.Vector2f

data class ProgressBarSkin(val defaultSize:Vector2f, val borderColor: Color, val borderThick: Float, val backColor: Color, val bodyColor: Color)