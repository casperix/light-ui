package ru.casperix.light_ui.component.button

import ru.casperix.light_ui.component.button.graphic.ToggleButtonGraphic
import ru.casperix.light_ui.component.text.Label
import ru.casperix.light_ui.element.SizeMode
import ru.casperix.light_ui.element.setSizeMode
import ru.casperix.light_ui.node.Node
import ru.casperix.light_ui.skin.SkinProvider
import ru.casperix.signals.concrete.EmptySignal
import ru.casperix.signals.concrete.Signal
import kotlin.reflect.KMutableProperty0





class ToggleButton(
    val label: Label,
    isChecked: Boolean = true,
    skin: ToggleButtonSkin = SkinProvider.skin.toggleButton,
    handler: (ToggleButton.() -> Unit)? = null,
) : AbstractButton<ToggleButtonSkin>(skin) {
    constructor(
        text: String = "",
        isChecked: Boolean = true,
        skin: ToggleButtonSkin = SkinProvider.skin.toggleButton,
        handler: (ToggleButton.() -> Unit)? = null,
    ) : this(
        Label(text),
        isChecked,
        skin,
        handler,
    )

    constructor(
        label: String, property: KMutableProperty0<Boolean>, skin: ToggleButtonSkin = SkinProvider.skin.toggleButton,
    )
            : this(label, property.get(), skin, {
        property.set(isChecked)
    })


    override val drawer = ToggleButtonGraphic(skin, state)
    override val children = listOf(Node(label))

    var isChecked
        get() = state.checked
        set(value) {
            if (value != state.checked) {
                click()
            }
        }

    init {
        label.setSizeMode(SizeMode.view)
        setListener(handler)
        skinChanged.set(skin)
        this.isChecked = isChecked
    }

    fun setLabel(text: String) = apply {
        label.text = text
    }

    @Deprecated("", ReplaceWith("setProperty(isChecked, handler)"))
    fun setListener(isChecked: Boolean = true, handler: ToggleButton.() -> Unit) = apply {
        this.isChecked = isChecked
        clickEvent.then { handler(this) }
    }

    @Deprecated("", ReplaceWith("setProperty(property)"))
    fun setListener(property: KMutableProperty0<Boolean>) = apply {
        setListener(property.get()) { property.set(isChecked) }
    }

    @Deprecated("", ReplaceWith("setListenerForChecked(isChecked, handler)"))
    fun setCheckedListener(isChecked: Boolean = true, handler: ToggleButton.() -> Unit) = apply {
        this.isChecked = isChecked
        clickEvent.then { if (state.checked) handler(this) }
    }

    fun setChecked(isChecked: Boolean) = apply {
        this.isChecked = isChecked
    }

    fun setListener(handler: ToggleButton.() -> Unit) = apply {
        clickEvent.then { handler(this) }
    }

    fun setProperty(property: KMutableProperty0<Boolean>) = apply {
        setChecked(property.get())
        setListener { property.set(isChecked) }
    }

    fun setProperty(initial: Boolean, setter: (Boolean) -> Unit) = apply {
        setChecked(initial)
        setListener { setter(isChecked) }
    }

    fun setListenerForChecked(handler: ToggleButton.() -> Unit) = apply {
        clickEvent.then { if (state.checked) handler(this) }
    }
}