package ru.casperix.light_ui.component.button

import kotlinx.serialization.Serializable

@Serializable
class ButtonState {
    var checked = false
    var enabled = true
    var focused = false
    var pressed = false
}