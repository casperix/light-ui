package ru.casperix.light_ui.component.button

import ru.casperix.light_ui.component.button.graphic.PushButtonGraphic
import ru.casperix.light_ui.component.text.Label
import ru.casperix.light_ui.skin.SkinProvider
import ru.casperix.light_ui.element.SizeMode
import ru.casperix.light_ui.element.setSizeMode
import ru.casperix.light_ui.node.Node


class PushButton(
    val label: Label,
    skin: PushButtonSkin = SkinProvider.skin.pushButton,
    handler: (PushButton.() -> Unit)? = null
) : AbstractButton<PushButtonSkin>(skin) {
    constructor(
        text: String = "",
        skin: PushButtonSkin = SkinProvider.skin.pushButton,
        handler: (PushButton.() -> Unit)? = null
    ) : this(Label(text), skin, handler)


    override val drawer = PushButtonGraphic(skin, state)
    override val children = listOf(Node(label))

    init {
        label.setSizeMode(SizeMode.view)
        setListener(handler)
        skinChanged.set(skin)
    }

    fun setLabel(text: String) = apply {
        label.text = text
    }

    fun setListener(handler: PushButton.() -> Unit) = apply {
        clickEvent.then { handler(this) }
    }
}

