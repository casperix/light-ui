package ru.casperix.light_ui.component.misc

import ru.casperix.light_ui.layout.LayoutSide
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.misc.max
import ru.casperix.misc.toPrecision
import kotlinx.serialization.Serializable


@Serializable
data class SideIndents(val left: Float, val top: Float, val right: Float, val bottom: Float) {
	constructor(space: Float) : this(space, space, space, space)
	constructor(spaceX: Float, spaceY: Float) : this(spaceX, spaceY, spaceX, spaceY)
	constructor(left: Int, top: Int, right: Int, bottom: Int) : this(left.toFloat(), top.toFloat(), right.toFloat(), bottom.toFloat())
	constructor(space: Int) : this(space, space, space, space)
	constructor(spaceX: Int, spaceY: Int) : this(spaceX, spaceY, spaceX, spaceY)

	override fun toString(): String {
		return "SideIndents(left=${left.toPrecision(1)}, top=${top.toPrecision(1)}, right=${right.toPrecision(1)}, bottom=${bottom.toPrecision(1)})"
	}

	val leftTop: Vector2f get() = Vector2f(left, top)
	val rightBottom: Vector2f get() = Vector2f(right, bottom)
	val size: Vector2f get() = Vector2f(left + right, top + bottom)

	operator fun plus(other: SideIndents): SideIndents {
		return SideIndents(left + other.left, top + other.top, right + other.right, bottom + other.bottom)
	}

	fun gapFromSide(side: LayoutSide): Float {
		return when (side) {
			LayoutSide.LEFT -> left
			LayoutSide.RIGHT -> right
			LayoutSide.TOP -> top
			LayoutSide.BOTTOM -> bottom
		}
	}

	fun upper(other: SideIndents): SideIndents {
		return SideIndents(
			max(this.left, other.left),
			max(this.top, other.top),
			max(this.right, other.right),
			max(this.bottom, other.bottom),
		)
	}

	companion object {
		val ZERO = SideIndents(0)
	}
}