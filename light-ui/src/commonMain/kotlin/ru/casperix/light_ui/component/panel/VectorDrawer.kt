package ru.casperix.light_ui.component.panel

import ru.casperix.light_ui.element.ElementDrawer
import ru.casperix.light_ui.node.DrawContext
import ru.casperix.renderer.Renderer2D
import ru.casperix.renderer.vector.VectorGraphic2D
import kotlin.time.Duration

class VectorDrawer(val graphic: VectorGraphic2D) : ElementDrawer {
    override fun draw(renderer: Renderer2D, context: DrawContext, tick: Duration) {
        renderer.draw(graphic, context.worldMatrix)
    }
}