package ru.casperix.light_ui.component.button.graphic

import ru.casperix.light_ui.component.button.AbstractButton
import ru.casperix.light_ui.component.button.ButtonState
import ru.casperix.light_ui.component.button.CheckBoxSkin
import ru.casperix.light_ui.node.DrawContext
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.renderer.Renderer2D
import kotlin.time.Duration

class CheckBoxGraphic(val skin: CheckBoxSkin, val state: ButtonState) :
    ButtonGraphic() {
    override fun draw(renderer: Renderer2D, context: DrawContext, tick: Duration) {
        val view = AbstractButton.getViewState(state)
        val toggle = skin.toggle
        val map = if (state.checked) toggle.checked else toggle.unchecked

        val viewSkin = map[view]
        if (viewSkin != null) {
            drawState(toggle.contourThick, viewSkin, renderer, context, tick)
        }

        val size = context.element.placement.viewportSize
        val transform = Matrix3f.scale(size) * context.worldMatrix

        if (state.checked) {
            renderer.draw(skin.selected, transform)
        } else {
            renderer.draw(skin.unselected, transform)
        }
    }
}