package ru.casperix.light_ui.component.scroll

import ru.casperix.light_ui.component.misc.DragWithPointer
import ru.casperix.light_ui.component.panel.QuadGraphic
import ru.casperix.light_ui.component.text.SkinnedElement
import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.layout.ScrollLayout
import ru.casperix.light_ui.node.Node
import ru.casperix.light_ui.skin.SkinProvider
import ru.casperix.light_ui.util.Transition
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.interpolation.float32.InterpolationFloat
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.light_ui.element.*
import kotlin.time.Duration


class ScrollBox(skin: ScrollBoxSkin = SkinProvider.skin.scrollBox) : SkinnedElement<ScrollBoxSkin>(skin, SizeMode.view), AbstractContainer,
    ElementViewport, ElementUpdate {
    val content = Node()

    private val back = Node()
    private val front = Node()
    override val layout: Layout = ScrollLayout()
    override val children = listOf(back, content, front)
    override val input = DragWithPointer(placement, ::onMove, ::onStop)

    private var candidateOffset = Vector2f.ZERO

    override var update: ElementUpdate? = this

    private val transition = Transition(InterpolationFloat::vector2)

    init {
        skinChanged.then {
            skin.front?.let {
                front.element = DrawableContainer(Layout.DEFAULT, emptyList(), QuadGraphic(it))
            }
            skin.back?.let {
                back.element = DrawableContainer(Layout.DEFAULT, emptyList(), QuadGraphic(it))
            }
            onMove(Vector2f.ZERO)
        }
        skinChanged.set(skin)
    }

    override fun invalidateLayout() {
        super.invalidateLayout()
    }


    private fun onMove(value: Vector2f) {
        val area = getArea()
        val delta = if (area.dimension.x == 0f) {
            value.yAxis
        } else if (area.dimension.y == 0f) {
            value.xAxis
        } else {
            value
        }
        if (delta != Vector2f.ZERO) {
            candidateOffset += delta
            transition.calculate(candidateOffset, Duration.ZERO, Duration.ZERO)
        }
    }

    private fun onStop() {
        val area = getArea()
        candidateOffset = candidateOffset.clamp(area.min, area.max)
    }

    override fun update(tick: Duration) {
        val contentPlacement = content.element.placement
        val targetOffset = transition.calculate(candidateOffset, skin.scrollReverseTime, tick)
        val nextPosition = targetOffset + contentPlacement.borders.leftTop
        content.transform = content.transform.copy(position = nextPosition)
    }

    private fun getArea(): Box2f {
        val contentPlacement = content.element.placement
        val contentSize = contentPlacement.contentArea.dimension + contentPlacement.borders.size
        val maxOffset = Vector2f.ZERO.lower(placement.viewportSize - contentSize)
        return Box2f(maxOffset, Vector2f.ZERO)
    }
}

