package ru.casperix.light_ui.component.togglegroup

enum class ToggleGroupSelectionMode {
    ALWAYS_ONE,
    MAX_ONE,
}