package ru.casperix.light_ui.component.text

import ru.casperix.light_ui.component.misc.SideIndents
import ru.casperix.light_ui.component.panel.QuadSkin
import ru.casperix.math.color.Color
import ru.casperix.math.color.rgba.RgbaColor
import ru.casperix.multiplatform.font.FontReference

data class LabelSkin(val font: FontReference, val color: Color, val back: QuadSkin?, val borders: SideIndents = SideIndents(5f), val drawTextMetrics:Boolean = false)

