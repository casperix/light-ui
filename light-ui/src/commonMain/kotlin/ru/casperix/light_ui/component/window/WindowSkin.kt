package ru.casperix.light_ui.component.window

import ru.casperix.light_ui.component.misc.SideIndents
import ru.casperix.light_ui.component.text.LabelSkin
import ru.casperix.math.color.Color

data class WindowSkin(val bodyColor: Color, val titleColor: Color, val titleText: LabelSkin, val titleHeight: Float, val contentBorder: SideIndents)