package ru.casperix.light_ui.component.text.editor

import kotlin.time.Duration
import kotlin.time.Duration.Companion.ZERO
import kotlin.time.Duration.Companion.milliseconds

class TextCursorAnimation {
    private val blinkTime = 500.milliseconds
    private var currentBlinkTime: Duration = Duration.ZERO
    private var blinkActive = true

    fun animate(tick: Duration): Boolean {
        currentBlinkTime += tick
        if (currentBlinkTime > blinkTime) {
            currentBlinkTime -= blinkTime
            blinkActive = !blinkActive
        }
        return blinkActive
    }

    fun reset() {
        currentBlinkTime = ZERO
        blinkActive = true
    }
}