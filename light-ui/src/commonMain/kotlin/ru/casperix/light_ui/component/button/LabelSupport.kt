package ru.casperix.light_ui.component.button

import ru.casperix.light_ui.component.text.Label

interface LabelSupport {
    val label: Label

    fun setLabel(text: String) = apply {
        label.text = text
    }
}