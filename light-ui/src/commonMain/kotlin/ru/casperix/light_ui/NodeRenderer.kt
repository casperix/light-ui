package ru.casperix.light_ui

import ru.casperix.light_ui.NodeViewport.getVisualArea
import ru.casperix.light_ui.node.RenderContext
import ru.casperix.light_ui.node.ViewPortPhase
import ru.casperix.math.axis_aligned.int32.Box2i
import ru.casperix.misc.fold
import ru.casperix.renderer.Renderer2D
import kotlin.time.Duration

class NodeRenderer {
    private val scissorList = mutableListOf<Box2i?>()

    fun render(renderList:List<RenderContext>, renderer: Renderer2D, tick: Duration) {
        scissorList.clear()
        renderList.forEach {
            renderContext(renderer, it, tick)
        }
    }

    private fun renderContext(renderer: Renderer2D, context: RenderContext, tick: Duration) {
        context.fold({ viewContext ->
            when (viewContext.phase) {
                ViewPortPhase.START -> {
                    val scissor = getVisualArea(viewContext.node, viewContext.worldMatrix)
                    pushScissor(renderer, scissor)
                }

                ViewPortPhase.FINISH -> {
                    popScissor(renderer)
                }
            }
        }, { drawContext ->
            drawContext.drawer.draw(renderer, drawContext, tick)
        })
    }

    private fun popScissor(renderer: Renderer2D) {
        scissorList.removeLastOrNull()
        val scissor = scissorList.lastOrNull()
        renderer.scissor = scissor
    }

    private fun pushScissor(renderer: Renderer2D, scissor: Box2i?) {
        scissorList += scissor
        renderer.scissor = scissor
    }
}

