package ru.casperix.light_ui.types

enum class Orientation {
    HORIZONTAL,
    VERTICAL;

    fun rotate(): Orientation {
        return when (this) {
            HORIZONTAL -> VERTICAL
            VERTICAL -> HORIZONTAL
        }
    }

}