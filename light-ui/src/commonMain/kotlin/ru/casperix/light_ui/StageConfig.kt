package ru.casperix.light_ui

import ru.casperix.math.color.Color

object StageConfig {
    var debugContents = false
    var debugViewports = false
    var debugHierarchy = false
    var debugLegend = false
    var viewportFilter = true

    val COLORS = listOf(Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW, Color.CYAN, Color.FUCHSIA)
}