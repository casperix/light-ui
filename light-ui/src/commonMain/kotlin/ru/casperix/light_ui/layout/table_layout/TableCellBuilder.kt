package ru.casperix.light_ui.layout.table_layout

import ru.casperix.light_ui.node.LayoutTarget
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.vector.float32.Vector2f

internal class TableCellBuilder(val target: LayoutTarget?) {
    var minElementSize = Vector2f.ZERO
    var maxElementSize = Vector2f.ZERO
    var actualCellSize = Vector2f.ZERO
    var actualArea = Box2f.ZERO

    var minCellSize = Vector2f.ZERO
    var maxCellSize = Vector2f.ZERO
}