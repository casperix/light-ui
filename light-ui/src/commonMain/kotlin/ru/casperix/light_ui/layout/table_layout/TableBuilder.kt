package ru.casperix.light_ui.layout.table_layout

import ru.casperix.light_ui.node.LayoutTarget
import ru.casperix.math.array.ArrayAccessND
import ru.casperix.math.array.CustomMap2D
import ru.casperix.math.vector.int32.Vector2i

internal class TableBuilder(val columnsAmount: Int, val rowsAmount: Int, items: List<LayoutTarget>) {

    val dimension = Vector2i(columnsAmount, rowsAmount)

    val cellsMap = CustomMap2D.createByXY(dimension) {
        val index = ArrayAccessND.index2D(dimension, it)
        val target = items.getOrNull(index)
        TableCellBuilder(target)
    }

    fun getAllColumns(): List<List<TableCellBuilder>> {
        return (0 until columnsAmount).map {
            getColumn(it)
        }
    }

    fun getAllRows(): List<List<TableCellBuilder>> {
        return (0 until rowsAmount).map {
            getRow(it)
        }
    }

    fun getColumn(columnIndex: Int): List<TableCellBuilder> {
        return (0 until rowsAmount).mapNotNull { rowIndex ->
            cellsMap.getOrNull(Vector2i(columnIndex, rowIndex))
        }
    }

    fun getRow(rowIndex: Int): List<TableCellBuilder> {
        return (0 until columnsAmount).mapNotNull { columnIndex ->
            cellsMap.getOrNull(Vector2i(columnIndex, rowIndex))
        }
    }
}