package ru.casperix.light_ui.layout

import ru.casperix.light_ui.element.ElementPlacement
import ru.casperix.light_ui.layout.orientation_layout.OrientationLayout
import ru.casperix.light_ui.layout.table_layout.TableLayout
import ru.casperix.light_ui.node.LayoutTarget
import ru.casperix.light_ui.types.Orientation
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.renderer.misc.AlignMode

interface Layout {
    fun calculate(placement: ElementPlacement, items: List<LayoutTarget>): Box2f

    companion object {
        val VERTICAL_CENTER = OrientationLayout(Orientation.VERTICAL, AlignMode.CENTER_CENTER)
        val VERTICAL = OrientationLayout(Orientation.VERTICAL, AlignMode.LEFT_TOP)

        val HORIZONTAL_CENTER = OrientationLayout(Orientation.HORIZONTAL, AlignMode.CENTER_CENTER)
        val HORIZONTAL = OrientationLayout(Orientation.HORIZONTAL, AlignMode.LEFT_TOP)

        val DEFAULT = VERTICAL_CENTER
        val FREE = FreeLayout()
        val STACK = StackLayout(AlignMode.CENTER_CENTER)

        val TABLE_1xN = TableLayout(Orientation.HORIZONTAL, 1)
        val TABLE_2xN = TableLayout(Orientation.HORIZONTAL, 2)
        val TABLE_3xN = TableLayout(Orientation.HORIZONTAL, 3)
        val TABLE_4xN = TableLayout(Orientation.HORIZONTAL, 4)

        val TABLE_Nx1 = TableLayout(Orientation.VERTICAL, 1)
        val TABLE_Nx2 = TableLayout(Orientation.VERTICAL, 2)
        val TABLE_Nx3 = TableLayout(Orientation.VERTICAL, 3)
        val TABLE_Nx4 = TableLayout(Orientation.VERTICAL, 4)
    }
}



