package ru.casperix.light_ui.layout

import ru.casperix.math.vector.int32.Vector2i

enum class LayoutSide {
    LEFT,
    RIGHT,
    TOP,
    BOTTOM;

    fun invert(): LayoutSide {
        return when (this) {
            TOP -> BOTTOM
            BOTTOM -> TOP
            LEFT -> RIGHT
            RIGHT -> LEFT
        }
    }

    fun rotateCCW(): LayoutSide {
        return when (this) {
            LEFT -> BOTTOM
            TOP -> LEFT
            RIGHT -> TOP
            BOTTOM -> RIGHT
        }
    }
    fun rotateCW(): LayoutSide {
        return when (this) {
            LEFT -> TOP
            TOP -> RIGHT
            RIGHT -> BOTTOM
            BOTTOM -> LEFT
        }
    }

    fun direction(): Vector2i {
        return when (this) {
            TOP -> Vector2i(0, -1)
            BOTTOM -> Vector2i(0, 1)
            LEFT -> Vector2i(-1, 0)
            RIGHT -> Vector2i(1, 0)
        }
    }

}