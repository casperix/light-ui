package ru.casperix.light_ui.layout

import ru.casperix.light_ui.element.ElementPlacement
import ru.casperix.light_ui.node.LayoutTarget
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.vector.float32.Vector2f

class ScrollLayout : Layout {
    override fun calculate(placement: ElementPlacement, items: List<LayoutTarget>): Box2f {
        val lastViewportSize = placement.viewportSize

        val content = items.getOrNull(1)
        content?.placement?.apply {
            //TODO: resize by max
            //val targetViewportSize = Vector2f(Float.POSITIVE_INFINITY)
            val targetViewportSize = contentArea.dimension
            val size = calculateSize(targetViewportSize, contentArea.dimension, contentArea.dimension)
            viewportSize = lastViewportSize.upper(size)
        }

        val front = items.getOrNull(2)
        val back = items.getOrNull(0)

        listOfNotNull(front, back).forEach {
            it.placement.viewportSize = lastViewportSize
        }
        return Box2f.byDimension(Vector2f.ZERO, lastViewportSize)
    }
}