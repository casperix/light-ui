package ru.casperix.light_ui.layout.common

import ru.casperix.light_ui.types.Orientation
import ru.casperix.math.vector.float32.Vector2f

/**
 *  We can use "layout space" for algorithm unification
 *
 *  horizontal layout:
 *
 *  +--+--+--+--> along
 *  |  |  |  |
 *  +--+--+--+
 *  |
 *  cross
 *
 *
 *  vertical layout:
 *
 *  +--+--> cross
 *  |  |
 *  +--+
 *  |  |
 *  +--+
 *  |  |
 *  +--+
 *  |
 *  along
 *
 */
internal class LayoutSpace(val along: Float, val cross: Float) {
    fun toScreenSpace(orientation: Orientation): Vector2f {
        return if (orientation == Orientation.VERTICAL) {
            Vector2f(cross, along)
        } else {
            Vector2f(along, cross)
        }
    }
}