package ru.casperix.light_ui.layout

import ru.casperix.light_ui.element.ElementPlacement
import ru.casperix.light_ui.node.LayoutTarget
import ru.casperix.math.axis_aligned.float32.Box2f

class FreeLayout : Layout {
    override fun calculate(placement: ElementPlacement, items: List<LayoutTarget>): Box2f {
        if (items.isEmpty()) {
            return Box2f.ZERO
        }
        return Box2f.ZERO
    }
}

