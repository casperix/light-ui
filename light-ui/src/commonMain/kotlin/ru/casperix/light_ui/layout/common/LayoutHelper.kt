package ru.casperix.light_ui.layout.common

import ru.casperix.light_ui.node.LayoutTarget
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.renderer.misc.AlignMode

internal object LayoutHelper {
    fun applyLayout(
        alignMode: AlignMode,
        areaList: List<Box2f>,
        viewportSize: Vector2f,
        isHorizontal: Boolean?,
        children: List<LayoutTarget>,
        roundToPixel:Boolean,
    ): Box2f {
        val fullArea = calculateSize(areaList)
        val size = fullArea.dimension

        val alignOffset = Vector2f(
            alignMode.horizontal.getPosition(viewportSize.x, size.x),
            alignMode.vertical.getPosition(viewportSize.y, size.y),
        )

        val alignedArea = areaList.map { area ->
            val localOffset = if (isHorizontal != null) {
                if (isHorizontal) {
                    val y = alignMode.vertical.getPosition(size.y, area.dimension.y)
                    Vector2f(0f, y)
                } else {
                    val x = alignMode.horizontal.getPosition(size.x, area.dimension.x)
                    Vector2f(x, 0f)
                }
            } else {
                Vector2f.ZERO
            }

            val leftTop = (area.min + alignOffset + localOffset).run {
                if (roundToPixel) round()
                else this
            }
            val dimension = area.dimension.run {
                if (roundToPixel) round()
                else this
            }
            Box2f.byDimension(leftTop, dimension)
        }
        setPlaceList(children, alignedArea)
        return Box2f(fullArea.min + alignOffset, fullArea.max + alignOffset)
    }

    fun setPlaceList(children: List<LayoutTarget>, areaList: List<Box2f>) {
        if (children.size != areaList.size) {
            throw Exception("Expected that list equal. children (${children.size}) not equals areas(${areaList.size})")
        }
        children.forEachIndexed { index, node ->
            node.setPlace(areaList[index])
        }
    }

    fun calculateSize(areaList: List<Box2f>): Box2f {
        if (areaList.isEmpty()) return Box2f.ZERO

        val min = areaList.map { it.min }.reduce { a, b -> a.lower(b) }
        val max = areaList.map { it.max }.reduce { a, b -> a.upper(b) }

        return calculateSize(min, max)
    }

    fun calculateSize(min: Vector2f, max: Vector2f): Box2f {
        return Box2f(min, min.upper(max))
    }

    fun calculateSize(box: Box2f): Box2f {
        return calculateSize(box.min, box.max)
    }
}