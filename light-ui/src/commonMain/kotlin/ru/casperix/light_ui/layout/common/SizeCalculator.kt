package ru.casperix.light_ui.layout.common

import ru.casperix.light_ui.element.ElementPlacement
import ru.casperix.light_ui.node.LayoutTarget
import ru.casperix.light_ui.types.Orientation
import ru.casperix.math.vector.float32.Vector2f
import kotlin.math.max

class SizeCalculator(children: List<LayoutTarget>, viewportSize: Vector2f, layoutSize: Vector2f, orientation: Orientation) {
    val sizeList: List<Vector2f>
    val maxAlong: Float
    val maxCross: Float

    val size: Vector2f

    init {
        sizeList = children.map {
            val childPlacement = it.placement
            getSize(childPlacement, viewportSize, layoutSize)
        }

        var along = 0f
        var cross = 0f

        val isHorizontal = orientation == Orientation.HORIZONTAL
        val alongIndex = if (isHorizontal) 0 else 1
        val crossIndex = if (isHorizontal) 1 else 0

        sizeList.forEach {
            along += it.component(alongIndex)
            cross = max(cross, it.component(crossIndex))
        }

        maxAlong = along
        maxCross = cross

        size = LayoutSpace(maxAlong, maxCross).toScreenSpace(orientation)
    }

    companion object {
        fun getSize(childPlacement: ElementPlacement, viewportSize: Vector2f, layoutSize: Vector2f): Vector2f {
            return childPlacement.calculateSize(viewportSize, childPlacement.contentArea.dimension, layoutSize)
        }
    }

}