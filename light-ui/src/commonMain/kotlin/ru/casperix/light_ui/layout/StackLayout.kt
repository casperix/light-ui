package ru.casperix.light_ui.layout

import ru.casperix.light_ui.element.ElementPlacement
import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.layout.common.LayoutHelper
import ru.casperix.light_ui.layout.common.SizeCalculator
import ru.casperix.light_ui.node.LayoutTarget
import ru.casperix.light_ui.types.Orientation
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.renderer.misc.AlignMode

class StackLayout(val alignMode: AlignMode) : Layout {
    override fun calculate(placement: ElementPlacement, children: List<LayoutTarget>): Box2f {
        val isHorizontal = false
        val orientation = Orientation.VERTICAL
        val viewportSize = placement.viewportSize
        val crossViewport = viewportSize.axisProjection(!isHorizontal)
        val minInfo = SizeCalculator(children, crossViewport, Vector2f.ZERO, orientation)
        val maxInfo = SizeCalculator(children, viewportSize, minInfo.size.axisProjection(!isHorizontal), orientation)

        //TODO: grow individual
        val alongReserve = Vector2f.ZERO.upper(viewportSize - minInfo.size).axisProjection(isHorizontal)
        val alongWant = Vector2f.ZERO.upper(maxInfo.size - minInfo.size).axisProjection(isHorizontal)

        //  relative grow factor
        val alongGrow = alongReserve.lower(alongWant)

        val alongGrowFactor = Vector2f(
            if (alongWant.x == 0f) 0f else alongGrow.x / alongWant.x,
            if (alongWant.y == 0f) 0f else alongGrow.y / alongWant.y,
        )


        val areaList = children.mapIndexed { index, element ->
            val minSize = minInfo.sizeList[index]
            val maxSize = maxInfo.sizeList[index]
            val deltaSize = (maxSize - minSize)

            val crossReserve = Vector2f.ZERO.upper(viewportSize - minSize)
            val along = deltaSize * alongGrowFactor
            val cross = deltaSize.lower(crossReserve).axisProjection(!isHorizontal)

            val actualSize = minSize + (cross + along)
            Box2f.byDimension(Vector2f.ZERO, actualSize)
        }

        return LayoutHelper.applyLayout(alignMode, areaList, viewportSize,  isHorizontal, children, true)
    }
}