package ru.casperix.light_ui.layout.table_layout

import ru.casperix.light_ui.element.ElementPlacement
import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.layout.common.LayoutHelper
import ru.casperix.light_ui.layout.common.SizeCalculator
import ru.casperix.light_ui.node.LayoutTarget
import ru.casperix.light_ui.types.Orientation
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.misc.ceilToInt
import ru.casperix.renderer.misc.AlignMode


/**
 *  Mega-super layout.
 *
 *  Universal table layout!
 *
 *  Can replace old layouts: orientation-layout, split-layout, maximize-layout, etc.
 */
class TableLayout(
    val rowOrientation: Orientation,
    val rowSize: Int = 1,
    val alignMode: AlignMode = AlignMode.CENTER_CENTER,
) : Layout {
    override fun calculate(placement: ElementPlacement, items: List<LayoutTarget>): Box2f {
        val table = TableBuilder(rowSize, (items.size.toFloat() / rowSize).ceilToInt(), items)

        val widthAxis = rowOrientation == Orientation.HORIZONTAL
        val heightAxis = rowOrientation == Orientation.VERTICAL
        val viewportSize = placement.viewportSize

        calculateElementMinMax(table, viewportSize)

        calculateMinCellSize(table.getAllColumns(), widthAxis)
        calculateMinCellSize(table.getAllRows(), heightAxis)

        calculateMaxCellSize(table.getAllColumns(), widthAxis)
        calculateMaxCellSize(table.getAllRows(), heightAxis)

        table.getAllRows().forEach { row ->
            calculateActualSize(row, viewportSize, widthAxis)
        }

        table.getAllColumns().forEach { columns ->
            calculateActualSize(columns, viewportSize, heightAxis)
        }


        placeAll(table, alignMode, rowOrientation)

        val areaList = table.cellsMap.array.subList(0, items.size).map {
            it.actualArea
        }

        return LayoutHelper.applyLayout(alignMode, areaList, viewportSize, null, items, true)
    }

    private fun calculateElementMinMax(table: TableBuilder, viewportSize: Vector2f) {
        table.cellsMap.forEach { cell ->
            cell.target?.let { target ->
                cell.minElementSize = SizeCalculator.getSize(target.placement, Vector2f.ZERO, Vector2f.ZERO)
                cell.maxElementSize = SizeCalculator.getSize(target.placement, viewportSize, Vector2f.ZERO)
            }
        }
    }

    private fun calculateMaxCellSize(groupList: List<List<TableCellBuilder>>, useXAxis: Boolean) {
        groupList.forEach { group ->
            val minSizeColumn = group.map { it.maxElementSize }.reduceOrNull { a, b -> Vector2f.ZERO.upper(a.upper(b)) } ?: Vector2f.ZERO
            group.forEach {
                it.maxCellSize += minSizeColumn.axisProjection(useXAxis)
            }
        }
    }

    private fun placeAll(table: TableBuilder, alignMode: AlignMode, rowOrientation: Orientation) {
        val widthAxis = rowOrientation == Orientation.HORIZONTAL
        val heightAxis = rowOrientation == Orientation.VERTICAL

        var rowOffset = Vector2f.ZERO
        var columnOffset = Vector2f.ZERO
        table.getAllRows().forEach { row ->

            row.forEach { cell ->
                val placement = cell.target?.placement
                val elementSize = if (placement != null) {
                    placement.calculateSize(cell.actualCellSize, placement.contentArea.dimension, cell.actualCellSize).lower(cell.maxElementSize)
                } else {
                    cell.actualCellSize
                }

                val elementAlignOffset = alignMode.getPosition(cell.actualCellSize, elementSize)

                val area = Box2f.byDimension(rowOffset + columnOffset + elementAlignOffset, elementSize)
                cell.actualArea = area


                columnOffset += cell.actualCellSize.axisProjection(widthAxis)
            }
            columnOffset = Vector2f.ZERO
            rowOffset += row.firstOrNull()?.actualCellSize?.axisProjection(heightAxis) ?: Vector2f.ZERO
        }
    }

    private fun calculateActualSize(row: List<TableCellBuilder>, viewportSize: Vector2f, currentAxis: Boolean) {

        val minRowWidth = row.map { it.minCellSize.axisProjection(currentAxis) }.reduceOrNull { a, b -> a + b } ?: Vector2f.ZERO
        val maxRowWidth = row.map { it.maxCellSize.axisProjection(currentAxis) }.reduceOrNull { a, b -> a + b } ?: Vector2f.ZERO
        val wantDelta = Vector2f.ZERO.upper(maxRowWidth - minRowWidth).axisProjection(currentAxis)
        val actualDelta = Vector2f.ZERO.upper(wantDelta.lower(viewportSize - minRowWidth)).axisProjection(currentAxis)

        var deltaFactor = actualDelta / wantDelta
        if (!deltaFactor.x.isFinite()) deltaFactor = Vector2f(0f, deltaFactor.y)
        if (!deltaFactor.y.isFinite()) deltaFactor = Vector2f(deltaFactor.x, 0f)

        row.forEach {
            it.actualCellSize += (it.minCellSize + (it.maxCellSize - it.minCellSize) * deltaFactor).axisProjection(currentAxis)
        }
    }


    private fun calculateMinCellSize(groupList: List<List<TableCellBuilder>>, useXAxis: Boolean) {
        groupList.forEach { group ->
            val minSizeColumn = group.map { it.minElementSize }.reduceOrNull { a, b -> Vector2f.ZERO.upper(a.upper(b)) } ?: Vector2f.ZERO
            group.forEach {
                it.minCellSize += minSizeColumn.axisProjection(useXAxis)
            }
        }
    }

}

