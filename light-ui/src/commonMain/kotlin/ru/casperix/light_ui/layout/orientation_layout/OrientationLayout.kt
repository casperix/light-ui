package ru.casperix.light_ui.layout.orientation_layout

import ru.casperix.light_ui.element.ElementPlacement
import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.layout.common.LayoutHelper
import ru.casperix.light_ui.layout.common.SizeCalculator
import ru.casperix.light_ui.node.LayoutTarget
import ru.casperix.light_ui.types.Orientation
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.renderer.misc.AlignMode

/**
 *  Super layout.
 *
 *  Simple, but universal layout!
 *
 *  Can replace old layouts: orientation-layout, split-layout, maximize-layout, etc.
 */
class OrientationLayout(
    val orientation: Orientation,
    val alignMode: AlignMode,
    val onceBorderMode: Boolean = true,
) : Layout {


    override fun calculate(placement: ElementPlacement, children: List<LayoutTarget>): Box2f {
        val isHorizontal = orientation == Orientation.HORIZONTAL
        val viewportSize = placement.viewportSize
        val crossViewport = viewportSize.axisProjection(!isHorizontal)
        val minInfo = SizeCalculator(children, crossViewport, Vector2f.ZERO, orientation)
        val maxInfo = SizeCalculator(children, viewportSize, minInfo.size.axisProjection(!isHorizontal), orientation)

        //TODO: grow individual
        val alongReserve = Vector2f.ZERO.upper(viewportSize - minInfo.size).axisProjection(isHorizontal)
        val alongWant = Vector2f.ZERO.upper(maxInfo.size - minInfo.size).axisProjection(isHorizontal)

        //  relative grow factor
        val alongGrow = alongReserve.lower(alongWant)

        val alongGrowFactor = Vector2f(
            if (alongWant.x == 0f) 0f else alongGrow.x / alongWant.x,
            if (alongWant.y == 0f) 0f else alongGrow.y / alongWant.y,
        )

        var corner = Vector2f.ZERO

        val areaList = children.mapIndexed { index, element ->
            val minSize = minInfo.sizeList[index]
            val maxSize = maxInfo.sizeList[index]
            val deltaSize = (maxSize - minSize)

            val crossReserve = Vector2f.ZERO.upper(viewportSize - minSize)
            val along = deltaSize * alongGrowFactor
            val cross = deltaSize.lower(crossReserve).axisProjection(!isHorizontal)

            val actualSize = minSize + (cross + along)

            val borderOffset = if (onceBorderMode) {
                val next = children.getOrNull(index + 1)
                val maxOffset = if (next != null) {
                    val lastOffset = element.placement.borders.rightBottom
                    val nextOffset = next.placement.borders.leftTop

                    lastOffset.upper(nextOffset)
                } else Vector2f.ZERO

                maxOffset.axisProjection(isHorizontal)
            } else Vector2f.ZERO

            Box2f.byDimension(corner, actualSize).apply {
                corner += (actualSize - borderOffset).upper(Vector2f.ZERO).axisProjection(isHorizontal)
            }
        }

        return LayoutHelper.applyLayout(alignMode, areaList, viewportSize, isHorizontal, children, true)
    }


}

