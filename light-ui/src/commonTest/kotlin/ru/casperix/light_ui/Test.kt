package ru.casperix.light_ui

import ru.casperix.light_ui.util.DelegatesExt
import kotlin.test.Test


class Some() {
    var data: String by DelegatesExt.observableChange("") {
        println("new: $it")
    }
}

class Test {
    @Test

    fun some() {
        Some().apply {
            println(data)
            data = "setA"
            println(data)
            data = "setB"
            println(data)
            data = "setB"
            println(data)
        }

    }
}