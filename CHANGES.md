1.1.5
- Layout: stack-layout
- Layout: use only one border by default
- Openable container

1.1.1
- Sliders

1.1.0
- Spine move to library (io.gitlab.casperix:spine-light-ui:1.1.0)

1.0.1
- Fix spine align 

1.0.0
- Support for improved text rendering
- Improved toggle-button & check-box
- Scroll box fixed

0.11.0
- setListener simplified (use setChecked for check)
- Use ToggleGroup instead createToggleGroup
- TabMenu & Tab moved to another package

0.10.3
- update dependencies

0.10.1
- fix scroll box

0.10.0
- use `ru.casperix`
- add wasm demo
- fix android demo