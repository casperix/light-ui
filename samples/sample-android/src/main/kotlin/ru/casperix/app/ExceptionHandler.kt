package ru.casperix.app

class ExceptionHandler(val activity: MainActivity) : Thread.UncaughtExceptionHandler {
    override fun uncaughtException(p0: Thread, p1: Throwable) {
        p1.printStackTrace()
        stop()
    }

    private fun stop() {
        android.os.Process.killProcess(android.os.Process.myPid())
    }

}