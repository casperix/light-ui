package ru.casperix.app

import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import ru.casperix.opengl.core.app.androidSurfaceLauncher

class MainActivity : FragmentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Thread.setDefaultUncaughtExceptionHandler(ExceptionHandler(this))
        androidSurfaceLauncher(this) { surface ->
            SampleLauncher(surface)
        }
    }
}

