package ru.casperix.app

import ru.casperix.app.surface.Surface
import ru.casperix.app.surface.SurfaceListener
import ru.casperix.input.InputEvent
import ru.casperix.light_ui.Stage
import ru.casperix.light_ui.component.spine.Spine
import ru.casperix.light_ui.component.window.Window
import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.node.Node
import ru.casperix.math.color.Colors
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.opengl.renderer.OpenGlRenderer2d
import ru.casperix.renderer.Renderer2D
import ru.casperix.spine.Skeleton
import ru.casperix.spine.json.JsonSkeletonLoader
import kotlin.time.Duration

class SpineUIApplication(val surface: Surface) : SurfaceListener {
    val renderer: Renderer2D = OpenGlRenderer2d()

    val contentNode = Node()
    val stage = Stage(
        Node(
            Window(
                "Spine as UI component", Layout.VERTICAL_CENTER,
                contentNode,
            )
        )
    )

    init {
        JsonSkeletonLoader.load("spineboy/spineboy-ess.json", "spineboy/spineboy.atlas").thenAccept {
            contentNode.element = Spine(Skeleton(it.skeletonData)).apply {
                controller.addAnimationIfExist("walk")
            }
        }
    }

    override fun dispose() {

    }

    override fun input(event: InputEvent) {
        stage.input(event)
    }

    override fun nextFrame(tick: Duration) {
        renderer.environment.viewPort = surface.getSize()
        stage.render(renderer, tick)
    }

}