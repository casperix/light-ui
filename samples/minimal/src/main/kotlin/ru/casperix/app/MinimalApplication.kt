package ru.casperix.app

import ru.casperix.app.surface.Surface
import ru.casperix.app.surface.SurfaceListener
import ru.casperix.input.InputEvent
import ru.casperix.light_ui.Stage
import ru.casperix.light_ui.component.button.PushButton
import ru.casperix.light_ui.component.window.Window
import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.node.Node
import ru.casperix.opengl.core.app.jvmSurfaceLauncher
import ru.casperix.opengl.renderer.OpenGlRenderer
import ru.casperix.renderer.Renderer2D
import kotlin.time.Duration

fun main() {
    jvmSurfaceLauncher(true) { MinimalApplication(it) }
}

class MinimalApplication(val surface: Surface) : SurfaceListener {
    val renderer = OpenGlRenderer()
    val renderer2d: Renderer2D = renderer.renderer2D

    val stage = Stage(
        Node(
            Window(
                "Hello", Layout.VERTICAL_CENTER,
                listOf(
                    PushButton("First"),
                    PushButton("Second"),
                )
            )
        )
    )

    override fun dispose() {

    }

    override fun input(event: InputEvent) {
        stage.input(event)
    }

    override fun nextFrame(tick: Duration) {
        stage.render(renderer2d, tick)
    }

}