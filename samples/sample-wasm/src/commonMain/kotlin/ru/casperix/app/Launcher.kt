package ru.casperix.app

import ru.casperix.opengl.core.app.wasmSurfaceLauncher
fun main() {
    wasmSurfaceLauncher { SampleLauncher(it) }
}