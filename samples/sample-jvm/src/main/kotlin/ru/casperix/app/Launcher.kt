package ru.casperix.app

import ru.casperix.opengl.core.app.jvmSurfaceLauncher

@OptIn(ExperimentalUnsignedTypes::class)
fun main() {
    jvmSurfaceLauncher(true) { SampleLauncher(it) }
}