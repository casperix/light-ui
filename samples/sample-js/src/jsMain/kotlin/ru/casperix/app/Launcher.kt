package ru.casperix.app

import ru.casperix.opengl.core.app.jsSurfaceLauncher


fun main() {
    jsSurfaceLauncher { SampleLauncher(it) }
}