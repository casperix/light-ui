package ru.casperix.app

import ru.casperix.app.surface.Surface
import ru.casperix.app.surface.SurfaceListener
import ru.casperix.input.InputEvent
import ru.casperix.light_ui.Stage
import ru.casperix.light_ui.component.figma.loader.JsonFigmaLoader
import ru.casperix.light_ui.component.figma.node.ChildrenRelatedProperties
import ru.casperix.light_ui.component.figma.node.FrameNode
import ru.casperix.light_ui.component.figma.renderer.FigmaRenderer
import ru.casperix.light_ui.component.window.Window
import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.node.Node
import ru.casperix.math.color.Colors
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.opengl.renderer.OpenGlRenderer2d
import ru.casperix.renderer.Environment
import ru.casperix.renderer.Renderer2D
import kotlin.time.Duration

class FigmaUIApplication(val surface: Surface) : SurfaceListener {
    val renderer: Renderer2D = OpenGlRenderer2d()
    var targetFrame: FrameNode? = null

    val contentNode = Node()
    val stage = Stage(
        Node(
            Window(
                "Figma graphic", Layout.VERTICAL_CENTER,
                contentNode,
            )
        )
    )

    init {
        JsonFigmaLoader.load("simple.json").thenAccept {
            val page1 = it.document.children.firstOrNull { it.name == "Page 1" } as? ChildrenRelatedProperties ?: return@thenAccept
            val frame1 = page1.children.firstOrNull { it.name == "Frame 1" } as? FrameNode ?: return@thenAccept
            targetFrame = frame1
        }
    }

    override fun dispose() {

    }

    override fun input(event: InputEvent) {
        stage.input(event)
    }

    override fun nextFrame(tick: Duration) {
        renderer.viewPort = surface.getSize()
        renderer.environment = Environment(
            projectionMatrix = Matrix3f.orthographic(surface.getSize(), true),
            ambientColor = Colors.BLACK,
        )
        stage.render(renderer, tick)
        targetFrame?.let {
            FigmaRenderer.renderNode(renderer, it, Matrix3f.Companion.scale(Vector2f(0.5f)) * Matrix3f.translate(100f, 400f))
        }
        renderer.flush()
    }

}