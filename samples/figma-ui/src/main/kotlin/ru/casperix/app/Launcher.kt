package ru.casperix.app

import ru.casperix.opengl.core.app.jvmSurfaceLauncher

fun main() {
    jvmSurfaceLauncher(true) { FigmaUIApplication(it) }
}