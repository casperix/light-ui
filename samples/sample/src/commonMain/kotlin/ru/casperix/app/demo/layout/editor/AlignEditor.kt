package ru.casperix.app.demo.layout.editor

import ru.casperix.app.demo.layout.editor.SmallSkin.smallToggleButton
import ru.casperix.light_ui.View
import ru.casperix.light_ui.component.togglegroup.ToggleGroupSelectionMode
import ru.casperix.light_ui.component.button.ToggleButton
import ru.casperix.light_ui.component.createToggleGroup
import ru.casperix.light_ui.component.text.Label
import ru.casperix.light_ui.element.Container
import ru.casperix.light_ui.layout.Layout
import ru.casperix.renderer.misc.Align

class AlignEditor(vertical: Boolean, callback: () -> Unit) : View {
    var align = Align.CENTER

    override val root = Container(
        Layout.VERTICAL,
        Label(OrientationHelper.orientationName(vertical) + " align"),
        createToggleGroup(
            Layout.VERTICAL,
            ToggleGroupSelectionMode.ALWAYS_ONE,
            ToggleButton(OrientationHelper.minName(vertical), skin = smallToggleButton).setListenerForChecked {
                align = Align.MIN
                callback()
            },
            ToggleButton("center", skin = smallToggleButton).setListenerForChecked {
                align = Align.CENTER
                callback()
            },
            ToggleButton(OrientationHelper.maxName(vertical), skin = smallToggleButton).setListenerForChecked {
                align = Align.MAX
                callback()
            },
        )
    )
}