package ru.casperix.app.demo

import ru.casperix.light_ui.component.slider.Slider
import ru.casperix.light_ui.component.text.Label
import ru.casperix.light_ui.component.window.Window
import ru.casperix.light_ui.element.Element
import ru.casperix.light_ui.element.SizeMode
import ru.casperix.light_ui.element.setSizeMode
import ru.casperix.light_ui.layout.Layout
import ru.casperix.math.vector.float32.Vector2f
import kotlin.random.Random


fun createSliderDemo(): Element {
    val random = Random(1)
    val amount = 3

    val bars = (0 until amount).map {
        Slider().apply {
            value = random.nextFloat()
            setSizeMode(SizeMode.fixed(Vector2f(300f, 50f)))
        }
    }


    return Window(
        "slider",
        Layout.VERTICAL,
        listOf(Label("You can touch items:")) + bars,
    )
}