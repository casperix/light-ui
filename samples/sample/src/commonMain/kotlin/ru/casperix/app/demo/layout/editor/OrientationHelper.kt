package ru.casperix.app.demo.layout.editor

object OrientationHelper {
    fun minName(vertical: Boolean):String {
        return if (vertical) "top" else "left"
    }
    fun maxName(vertical: Boolean):String {
        return if (vertical) "bottom" else "right"
    }

    fun axisName(vertical: Boolean):String {
        return  if (vertical) "Y" else "X"
    }
    fun orientationName(vertical: Boolean):String {
        return  if (vertical) "vertical" else "horizontal"
    }
}