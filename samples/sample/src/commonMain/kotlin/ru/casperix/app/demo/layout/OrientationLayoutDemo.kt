package ru.casperix.app.demo.layout

import ru.casperix.app.demo.ContentGenerator
import ru.casperix.app.demo.layout.editor.AlignEditor
import ru.casperix.app.demo.layout.editor.OrientationEditor
import ru.casperix.light_ui.component.panel.Panel
import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.layout.orientation_layout.OrientationLayout
import ru.casperix.light_ui.node.Node
import ru.casperix.light_ui.util.wrapNodes
import ru.casperix.renderer.misc.AlignMode
import ru.casperix.signals.concrete.EmptySignal
import ru.casperix.light_ui.element.*


object OrientationLayoutDemo {


    fun create(): Element {
        val content = Node(Empty())

        val onGenerate = EmptySignal()
        val generate = onGenerate::set

        val amount = 3

        val itemControllerList = (0 until amount).map {
            DimensionBehaviourViewController {
                generate()
            }
        }

        val orientationController = OrientationEditor(generate)
        val horizontalAlignEditor = AlignEditor(false, generate)
        val verticalAlignEditor = AlignEditor(true, generate)

        onGenerate.then {
            content.element = ContentGenerator.generateContent(
                OrientationLayout(
                    orientationController.orientation,
                    AlignMode(horizontalAlignEditor.align, verticalAlignEditor.align)
                ),
                itemControllerList.size,
                postBuilder = { index, node ->
                    val itemController = itemControllerList[index]
                    node.element.placement.sizeMode = SizeMode(itemController.xController.dimensionMode, itemController.yController.dimensionMode)
//                    if (index >= 0) {
//                        node.element = PushButton("click me")
//                    }
                })
        }

        onGenerate.set()

        return Panel(
            Layout.HORIZONTAL,
            Node(
                Container(
                    Layout.VERTICAL,
                    Container(
                        Layout.HORIZONTAL,
                        orientationController.root,
                        horizontalAlignEditor.root,
                        verticalAlignEditor.root,
                    ),
                    Container(
                        Layout.VERTICAL,
                        itemControllerList.map { it.root }.wrapNodes()
                    )
                )
            ),
            content
        ).setSizeMode(SizeMode.max)
    }
}