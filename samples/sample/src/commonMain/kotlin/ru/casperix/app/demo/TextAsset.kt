package ru.casperix.app.demo

object TextAsset {

    private val enText =
        "The graphical user interface (GUI /dʒiːjuːˈaɪ/ jee-you-eye[1][Note 1] or /ˈɡuːi/[2]) is a form of user interface that allows users to interact with electronic devices through graphical icons and audio indicator such as primary notation, instead of text-based user interfaces, typed command labels or text navigation. GUIs were introduced in reaction to the perceived steep learning curve of command-line interfaces (CLIs),[3][4][5] which require commands to be typed on a computer keyboard. "
    private val ruText = "Чаще всего элементы интерфейса в GUI реализованы на основе метафор и отображают их назначение и свойства, что облегчает понимание и использование электронных устройств неподготовленными пользователями. "
    private val frText =
        "Ce type d'interface a été créé en 1973 sur le Xerox Alto par les ingénieurs du Xerox PARC pour remplacer les interfaces en ligne de commande. Mis sur le marché à la fin des années 1970 avec le Star de Xerox, le Lisa d'Apple et popularisé par cette dernière firme avec l'ordinateur Macintosh, commercialisé en 1984"

    private val zhText = "渲染是三维计算机图形学中的最重要的研究课题之一，并且在实践领域它与其它技术密切相关。 在图形流水线中，渲染是最后一项重要步骤，通过它得到模型与动画最终显示效果。 自从二十世纪七十年代以来，随着计算机图形的不断复杂化，渲染也成為一項越来越重要的技术。"

    private val arText = "في أغلب الأحيان ، يتم تنفيذ عناصر الواجهة في واجهة المستخدم الرسومية على أساس الاستعارات وتعكس الغرض منها وخصائصها ، مما يسهل على المستخدمين غير المستعدين فهم واستخدام الأجهزة الإلكترونية."

    val text = 			"english:\n" + enText + "\n\nrussian:\n" + ruText + "\n\nfrench:\n" + frText + "\n\nchinese (partially supported):\n" + zhText + "\n\narabic (partially supported):\n" + arText


}