package ru.casperix.app.demo.benchmark

enum class RenderMode {
    SINGLE,
    LIST,
}