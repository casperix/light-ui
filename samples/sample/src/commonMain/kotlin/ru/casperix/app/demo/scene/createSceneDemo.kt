package ru.casperix.app.demo.scene

import ru.casperix.app.AppAssets
import ru.casperix.light_ui.component.button.PushButton
import ru.casperix.light_ui.component.window.Window
import ru.casperix.light_ui.element.Container
import ru.casperix.light_ui.element.Element
import ru.casperix.light_ui.element.SizeMode
import ru.casperix.light_ui.element.setSizeMode
import ru.casperix.light_ui.layout.Layout
import ru.casperix.math.color.Color
import ru.casperix.math.random.nextQuad2f
import ru.casperix.math.random.nextTriangle2f
import ru.casperix.math.vector.float32.Vector2f
import kotlin.random.Random

fun createSceneDemo(assets: AppAssets): Element {

    val scene = Scene(assets).setSizeMode(SizeMode.view)
    val random = Random(1)

    return Window(
        "scene",
        Layout.HORIZONTAL,
        Container(
            Layout.VERTICAL,
            PushButton("clear") {
                scene.custom.clear()
            },
            PushButton("add tri (color)") {
                scene.custom += ShapeContainer(
                    ShapeWithSimple(Color.all.random(random)),
                    random.nextTriangle2f(maxOffset = Vector2f(400f)),
                )
            },
            PushButton("add quad (color)") {
                scene.custom += ShapeContainer(
                    ShapeWithSimple(Color.all.random(random)),
                    random.nextQuad2f(maxOffset = Vector2f(400f)),
                )
            },
            PushButton("add quad (albedo)") {
                scene.custom += ShapeContainer(
                    ShapeWithPhong(assets.lorry.content!!, null, null),
                    random.nextQuad2f(maxOffset = Vector2f(400f)),
                )
            },
            PushButton("add quad (albedo+normal)") {
                scene.custom += ShapeContainer(
                    ShapeWithPhong(assets.lorry.content!!, assets.lorry_normals.content!!, null),
                    random.nextQuad2f(maxOffset = Vector2f(400f)),
                )
            },
        ),
        scene
    ).setSizeMode(SizeMode.max)
}