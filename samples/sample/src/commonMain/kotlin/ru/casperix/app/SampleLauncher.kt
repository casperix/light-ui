package ru.casperix.app

import ru.casperix.app.surface.DesktopWindow
import ru.casperix.app.surface.Surface
import ru.casperix.app.surface.SurfaceListener
import ru.casperix.input.InputEvent
import ru.casperix.light_ui.Stage
import ru.casperix.light_ui.StageConfig
import ru.casperix.light_ui.component.text.Label
import ru.casperix.light_ui.component.window.Window
import ru.casperix.light_ui.element.Element
import ru.casperix.light_ui.element.SizeMode
import ru.casperix.light_ui.element.setSizeMode
import ru.casperix.light_ui.layout.table_layout.TableLayout
import ru.casperix.light_ui.node.Node
import ru.casperix.light_ui.types.Orientation
import ru.casperix.math.color.Color
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.misc.time.getTimeMs
import ru.casperix.multiplatform.loader.PixelMapLoader
import ru.casperix.multiplatform.loader.resourceLoader
import ru.casperix.opengl.renderer.OpenGlRenderer
import ru.casperix.renderer.Renderer2D
import ru.casperix.renderer.pixel_map.PixelMap
import ru.casperix.signals.concrete.EitherCollectionFuture
import kotlin.time.Duration

@ExperimentalUnsignedTypes
class SampleLauncher(val surface: Surface) : SurfaceListener {
    private val renderer = OpenGlRenderer()
    private val renderer2d:Renderer2D = renderer.renderer2D

    private var ui: Stage? = null

    private var testLayout = false

    init {
        renderer.apply {
            config = config.copy(cacheStaticSummaryIndicesMax = 20_000_000)
        }

//        GLConfig.throwIfError = true
        if (surface is DesktopWindow) {
            surface.maximize()
        }

        EitherCollectionFuture(
            resourceLoader.loadImage("albedo.png"),
            resourceLoader.loadImage("normals.png"),
            resourceLoader.loadImage("light.png"),
            resourceLoader.loadImage("axis.png"),
            resourceLoader.loadImage("wires.png"),
        ).then({
            lazySetup(it[0], it[1], it[2], it[3], it[4])
        }, {
            throw Exception("Can't load resources: $it")
        })
    }

    private fun lazySetup(
        albedo: PixelMap,
        normals: PixelMap,
        light: PixelMap,
        axis: PixelMap,
        wires: PixelMap
    ) {
        val center = Vector2f(200f)

        val stage = Stage(Node())

        if (testLayout) {
//            PlatformMisc.debugOneFPSMode = true
            StageConfig.debugViewports = true
            stage.root.element = createTestElement()
        } else {
            val assets = AppAssets()
            PixelMapLoader.thenComplete {
                stage.root.element = SceneViewController(renderer, surface, stage, assets).root
            }
        }

        this.ui = stage
    }

    private var counter = 0
    private var lastInputTime = 0L

    override fun input(event: InputEvent) {
        ui?.input(event)

        lastInputTime = getTimeMs()
        requestFrame()
    }


    override fun nextFrame(tick: Duration) {
        if (testLayout) {
            if (counter++ % 2 == 0) {
                testLayout = false
                ui?.root?.element = createTestElement()
            }
        }
        updateViewport()

        renderer2d.clearColor(Color.BLACK.toRGBA())
        ui?.render(renderer2d, tick)

        requestFrame()
    }

    private fun requestFrame() {
        if (lastInputTime + 1000L > getTimeMs()) {
            surface.requestFrame()
        }
    }

    private fun createTestElement(): Element {
        return Window(
            "Title",
            TableLayout(Orientation.HORIZONTAL, 2),
            Label("some"),
            Label("32").setSizeMode(SizeMode.row),
        )

//        return ContentGenerator.generateContent(
//            OrientationLayout(
//                Orientation.HORIZONTAL,
//                AlignMode.CENTER_CENTER
//            ), 5, generateSubChild = true
//        )
    }

    private fun updateViewport() {
        val dimension = surface.getSize()
        if (renderer2d.viewPort == dimension) return

        //  TODO: HACK. U can remove it
        renderer2d.viewPort = dimension
        renderer2d.setOrthographicCamera(dimension.toDimension2f(), false, true)
    }

    override fun dispose() {

    }
}

