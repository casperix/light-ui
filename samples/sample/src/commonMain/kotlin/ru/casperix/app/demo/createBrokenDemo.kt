package ru.casperix.app.demo

import ru.casperix.light_ui.custom.OpenableContainer
import ru.casperix.light_ui.custom.editor.SliderColorEditor
import ru.casperix.light_ui.element.AbstractContainer

fun createBrokenDemo(): AbstractContainer {


    val panel = SliderColorEditor("Color")

    return OpenableContainer("Test", panel.root)
}