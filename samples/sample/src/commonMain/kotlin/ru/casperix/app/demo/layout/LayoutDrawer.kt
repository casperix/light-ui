package ru.casperix.app.demo.layout

import ru.casperix.light_ui.element.ElementDrawer
import ru.casperix.light_ui.node.DrawContext
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.color.Color
import ru.casperix.math.geometry.grow
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.toQuad
import ru.casperix.renderer.Renderer2D
import kotlin.time.Duration


class LayoutDrawer(val color: Color) : ElementDrawer {
    override fun draw(renderer: Renderer2D, context: DrawContext, tick: Duration) {
        val placement = context.node.element.placement

        val contentArea = placement.contentArea.toQuad().convert { context.worldMatrix.transform(it) }
        val viewportArea = Box2f(Vector2f.ZERO, placement.viewportSize).toQuad().convert { context.worldMatrix.transform(it) }

        renderer.draw {
            addQuad(color.toRGB().toRGBA(0.2f), viewportArea)
            addQuadContour(color, viewportArea.grow(-1f), 2f)
            addQuadContour(Color.BLACK.toRGBA(0.2f), contentArea.grow(-1f), 2f)
        }
    }
}