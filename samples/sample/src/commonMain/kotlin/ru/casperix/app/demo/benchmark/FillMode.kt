package ru.casperix.app.demo.benchmark

enum class FillMode {
    IMAGE,
    COLOR,
    TEXT,
}