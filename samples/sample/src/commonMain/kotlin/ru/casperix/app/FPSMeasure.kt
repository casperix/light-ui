package ru.casperix.app

import kotlin.time.Duration

class FPSMeasure {
    private var ticks = mutableListOf<Duration>()

    var medianTick = Duration.ZERO; private set

    fun nextFrame(tick: Duration) {
        ticks += tick
        while (ticks.size > 100) {
            ticks.removeFirst()
        }

        medianTick = ticks.reduce { a, b -> a + b }.div(ticks.size.toDouble())
    }
}