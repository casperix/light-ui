package ru.casperix.app

import ru.casperix.multiplatform.loader.PixelMapLoader


@ExperimentalUnsignedTypes
class AppAssets {
    val templateAlbedo = PixelMapLoader.getLoader("albedo.png")
    val templateNormals = PixelMapLoader.getLoader("normals.png")
    val light = PixelMapLoader.getLoader("light.png")
    val axis = PixelMapLoader.getLoader("axis.png")
    val wires = PixelMapLoader.getLoader("wires.png")
    val lorry = PixelMapLoader.getLoader("small_lorry_cargo_albedo.png")
    val lorry_normals = PixelMapLoader.getLoader("small_lorry_cargo_normals.png")
}