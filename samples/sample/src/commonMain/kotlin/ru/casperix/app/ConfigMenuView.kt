package ru.casperix.app

import ru.casperix.light_ui.View
import ru.casperix.light_ui.component.button.PushButton
import ru.casperix.light_ui.component.button.ToggleButton
import ru.casperix.light_ui.component.text.Label
import ru.casperix.light_ui.component.window.Window
import ru.casperix.light_ui.element.SizeMode
import ru.casperix.light_ui.element.setSizeMode
import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.node.Node
import ru.casperix.light_ui.util.wrapNodes

class ConfigMenuView : View {
    val info = Label()
    val fps = Label()
    val buttons = Node()
    val btnCharMissingIgnore = PushButton()
    val btnCharMissingSquare = PushButton()
    val btnCharMissingCode = PushButton()
    val btnPrintSize = PushButton()
    val btnVSync = ToggleButton()
    val btnFullScreen = ToggleButton()

    override val root = Window(
        Label(),
        Layout.VERTICAL,
        listOf(buttons) + listOf(
            btnCharMissingIgnore,
            btnCharMissingSquare,
            btnCharMissingCode,
            btnPrintSize,
            btnVSync,
            btnFullScreen,
            info,
            fps,
        ).wrapNodes()
    ).setSizeMode(SizeMode.content)
}