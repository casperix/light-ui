package ru.casperix.app.demo.scene

import ru.casperix.app.AppAssets
import ru.casperix.input.*
import ru.casperix.light_ui.component.misc.DragWithPointer
import ru.casperix.light_ui.element.*
import ru.casperix.light_ui.node.DrawContext
import ru.casperix.light_ui.node.InputContext
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.collection.getLooped
import ru.casperix.math.color.Color
import ru.casperix.math.color.rgba.RgbaColor4b
import ru.casperix.math.color.rgba.RgbaColor4f
import ru.casperix.math.geometry.CustomPolygon
import ru.casperix.math.geometry.Polygon2f
import ru.casperix.math.random.nextTriangle2f
import ru.casperix.math.random.nextVector2f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.renderer.Renderer2D
import ru.casperix.renderer.light.PointLight
import kotlin.random.Random
import kotlin.time.Duration

class ShapePivot(var position: Vector2f)
class ShapeContainer(val shape: Shape, val points: List<ShapePivot>) {
    constructor(shape: Shape, initialGeometry: Polygon2f) : this(
        shape,
        initialGeometry.getVertices().map { ShapePivot(it) })
}

class Scene(assets: AppAssets) : AbstractElement(SizeMode.content), Element, ElementDrawer, ElementInput {

    val all = listOf(
        Color.RED,
        Color.GREEN,
        Color.BLUE,
        Color.YELLOW,
        Color.FUCHSIA,
        Color.TEAL,
        Color.CYAN,
    )

    private val SELECTED_PIVOT = 1f
    private val FOCUSED_PIVOT = 0.75f
    private val NORMAL_PIVOT = 0.3f
    private val NORMAL_CONTAINER = RgbaColor4f(1f, 1f, 1f, 0.25f)

    private val INSIDE_RANGE = 8f
    private val OUTSIDE_RANGE = 12f
    private val FEEL_RANGE = 40f

    private val pivotDrag = DragWithPointer(placement, ::moveIt)

    private var focusedPivot: ShapePivot? = null
    private var selectedPivot: ShapePivot? = null

    override val drawer = this
    override val input = this

    val custom = mutableListOf<ShapeContainer>()
    val light = ShapeContainer(
        ShapeMarker(assets.light.content!!),
        CustomPolygon(Random(1).nextVector2f(100f, 200f))
    )

    private val containers get() = (custom + light)

    init {
        placement.contentArea = Box2f(Vector2f.ZERO, Vector2f(500f))
        custom += ShapeContainer(
            ShapeWithSimple(Color.RED),
            Random(1).nextTriangle2f(0f, 500f)
        )
    }

    override fun draw(renderer: Renderer2D, context: DrawContext, tick: Duration) {
        val lightPosition = context.worldMatrix.transform(light.points.first().position).expand(100f)

        renderer.lights = listOf(PointLight.DEFAULT.copy(position = lightPosition))

        containers.forEach { container ->
            val geometry = CustomPolygon(container.points.map { context.worldMatrix.transform(it.position) })

            renderer.draw {
                container.points.forEachIndexed { index, pivot ->
                    val alpha = when (pivot) {
                        selectedPivot -> SELECTED_PIVOT
                        focusedPivot -> FOCUSED_PIVOT
                        else -> NORMAL_PIVOT
                    }

                    val color = all.getLooped(index).toRGBA(alpha)
                    addCircle(color, pivot.position, INSIDE_RANGE, OUTSIDE_RANGE, 16, context.worldMatrix)
                }

                addPolygon(NORMAL_CONTAINER, geometry)
            }

            container.shape.draw(renderer, geometry)
        }
    }

    override fun input(event: InputEvent, context: InputContext) {
        if (event !is PointerEvent) return

        val localPosition = context.worldMatrix.inverse().transform(event.position)

        if (event is PointerDown) {
            selectedPivot = getNearPoint(localPosition)
        }
        if (event is PointerMove) {
            focusedPivot = getNearPoint(localPosition)
        }
        if (event is PointerUp) {
            selectedPivot = null
        }

        pivotDrag.input(event, context)
    }

    private fun getNearPoint(localPosition: Vector2f): ShapePivot? {
        val controlPoints = containers.flatMap {
            it.points
        }

        return controlPoints.firstOrNull {
            (it.position.distTo(localPosition) < FEEL_RANGE)
        }
    }

    private fun moveIt(value: Vector2f) {
        val focusedPoint = selectedPivot ?: return
        focusedPoint.position = (focusedPoint.position + value).clamp(placement.borders.leftTop, placement.viewportSize)

    }
}
