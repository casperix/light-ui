package ru.casperix.app.demo

import ru.casperix.light_ui.component.text.editor.TextEditor
import ru.casperix.light_ui.component.window.Window
import ru.casperix.light_ui.element.AbstractContainer
import ru.casperix.light_ui.element.SizeMode
import ru.casperix.light_ui.element.setSizeMode
import ru.casperix.light_ui.layout.Layout
import ru.casperix.math.vector.float32.Vector2f

fun createTextEditorDemo(): AbstractContainer {
    return Window(
        "text editor",
        Layout.VERTICAL_CENTER,
        TextEditor("some text for edit").setSizeMode(SizeMode.view),
        TextEditor("other text for edit").setSizeMode(SizeMode.view),
    ).setSizeMode(SizeMode.fixed(Vector2f(400f, 150f)))
}
