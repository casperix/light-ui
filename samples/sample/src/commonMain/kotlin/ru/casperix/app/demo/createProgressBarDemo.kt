package ru.casperix.app.demo

import ru.casperix.light_ui.component.progressbar.ProgressBar
import ru.casperix.light_ui.component.window.Window
import ru.casperix.light_ui.element.Element
import ru.casperix.light_ui.element.ElementUpdate
import ru.casperix.light_ui.layout.Layout
import ru.casperix.math.random.nextFloat
import kotlin.random.Random
import kotlin.time.DurationUnit


fun createProgressBarDemo(): Element {
    val random = Random(1)
    val amount = 3

    val bars = (0 until amount).map {
        ProgressBar().apply { progress = random.nextFloat() }
    }
    val speeds = (0 until amount).map {
        random.nextFloat(0.02f, 0.20f)
    }
    return Window(
        "progress-bar",
        Layout.VERTICAL,
        bars,
    ).apply {
        update = ElementUpdate { tick ->
            bars.forEachIndexed { index, bar ->
                bar.progress += tick.toDouble(DurationUnit.SECONDS).toFloat() * speeds[index]
                if (bar.progress > 1f) bar.progress = 0f
            }
        }
    }
}