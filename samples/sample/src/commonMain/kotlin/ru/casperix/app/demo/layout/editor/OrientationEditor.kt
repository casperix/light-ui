package ru.casperix.app.demo.layout.editor

import ru.casperix.app.demo.layout.editor.SmallSkin.smallToggleButton
import ru.casperix.light_ui.View
import ru.casperix.light_ui.component.togglegroup.ToggleGroupSelectionMode
import ru.casperix.light_ui.component.button.ToggleButton
import ru.casperix.light_ui.component.createToggleGroup
import ru.casperix.light_ui.component.text.Label
import ru.casperix.light_ui.element.Container
import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.types.Orientation

class OrientationEditor(callback: () -> Unit) : View {
    var orientation = Orientation.VERTICAL

    override val root = Container(
        Layout.VERTICAL,
        Label("orientation"),
        createToggleGroup(
            Layout.VERTICAL,
            ToggleGroupSelectionMode.ALWAYS_ONE,
            ToggleButton("vertical", skin = smallToggleButton).setListenerForChecked {
                orientation = Orientation.VERTICAL
                callback()
            },
            ToggleButton("horizontal", skin = smallToggleButton).setListenerForChecked {
                orientation = Orientation.HORIZONTAL
                callback()
            }
        )
    )
}