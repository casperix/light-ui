package ru.casperix.app.demo.benchmark

import ru.casperix.app.AppAssets
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.collection.getLooped
import ru.casperix.math.color.Color
import ru.casperix.math.geometry.Quad2f
import ru.casperix.math.geometry.float32.rotate
import ru.casperix.math.geometry.float32.translate
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.random.nextDegreeFloat
import ru.casperix.math.random.nextVector2f
import ru.casperix.math.transform.float32.Transform2f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.toQuad
import ru.casperix.misc.ceilToInt
import ru.casperix.misc.nextString
import ru.casperix.multiplatform.font.FontReference
import ru.casperix.multiplatform.text.drawText
import ru.casperix.renderer.Renderer2D
import ru.casperix.renderer.material.SimpleMaterial
import ru.casperix.renderer.material.Texture2D
import ru.casperix.renderer.pixel_map.PixelMap
import ru.casperix.renderer.vector.VectorGraphic2D
import ru.casperix.renderer.vector.builder.VectorGraphicBuilder2D
import kotlin.math.sqrt
import kotlin.random.Random
import kotlin.time.Duration


@ExperimentalUnsignedTypes
class BenchmarkShapeBuilder(val assets: AppAssets) {

    class ShapeRow(val shapes: List<Shape>, val graphic: VectorGraphic2D)

    class TextInfo(val text: String, val matrix: Matrix3f)

    class Shape(val textInfo: TextInfo, val polygon: Quad2f, val color: Color, val image: PixelMap)

    private val random = Random(1)

    private val imageSource = listOfNotNull(assets.lorry.content, assets.axis.content, assets.wires.content)
    private var lastState: GraphicState? = null
    private var rows = listOf<ShapeRow>()

    var sizeMode = SizeVariant.MEDIUM
    var renderMode = RenderMode.SINGLE
    var fillMode = FillMode.COLOR
    var windowSize = Vector2f.ZERO
    var useRandom = false
    var amount = 1024

    val font = FontReference("Serif", 10)

    private fun rebuildIfChanged() {
        val nextState = GraphicState(sizeMode, renderMode, fillMode, windowSize, useRandom, amount)
        if (lastState == nextState) return
        lastState = nextState

        val shapeOfShapes = generateShapes(amount, windowSize)

        rows = shapeOfShapes.map { shapes ->
            if (shapes.isEmpty()) {
                ShapeRow(emptyList(), VectorGraphic2D.EMPTY)
            } else {
                val graphic = VectorGraphicBuilder2D.build {
                    when (fillMode) {
                        FillMode.COLOR -> shapes.forEach {
                            addQuad(SimpleMaterial(it.color), it.polygon)
                        }

                        FillMode.IMAGE -> shapes.forEach {
                            addQuad(SimpleMaterial(Texture2D(it.image)), it.polygon, Box2f.ONE.toQuad())
                        }

                        else -> {}
                    }
                }
                ShapeRow(shapes, graphic)
            }
        }
    }

    private fun generateShapes(amount: Int, windowSize: Vector2f): List<List<Shape>> {
        var remains = amount
        val intervalsAmount = sqrt(amount.toFloat()).ceilToInt()
        val intervalSize = windowSize * (1f / intervalsAmount.toFloat())

        if (!intervalSize.isFinite()) {
            return emptyList()
        }

        val shapeOfShapes = mutableListOf<List<Shape>>()

        (0 until intervalsAmount).forEach { y ->
            val shapes = mutableListOf<Shape>()
            (0 until intervalsAmount).forEach { x ->
                if (remains-- >= 0) {
                    shapes += if (useRandom) {
                        createRandomShape()
                    } else {
                        createShape(x, y, intervalSize)
                    }
                }
            }
            shapeOfShapes += shapes
        }
        return shapeOfShapes
    }

    private fun createRandomShape(): Shape {
        val shapeSizeRange = sizeMode.range
        val border = Vector2f(shapeSizeRange.endInclusive)
        val pivot = random.nextVector2f(border, windowSize - border * 2f)
        val angle = random.nextDegreeFloat()
        val shapeSize = random.nextVector2f(shapeSizeRange.start, shapeSizeRange.endInclusive)
        val quad = Box2f(Vector2f.ZERO, shapeSize).toQuad().rotate(angle = angle.value).translate(pivot)
        val m = Transform2f(random.nextVector2f(shapeSize, windowSize - shapeSize * 2f), rotation = angle).matrix

        return Shape(
            TextInfo(random.nextString(), m),
            quad,
            Color.all.random(random),
            imageSource.random(random),
        )
    }

    private fun createShape(x: Int, y: Int, intervalSize: Vector2f): Shape {
        val offset = Vector2f(x.toFloat(), y.toFloat()) * intervalSize
        val m = Matrix3f.translate(offset)
        val q = Box2f.byDimension(offset, (intervalSize - Vector2f(1f)).upper(Vector2f.ONE)).toQuad()

        val image = imageSource.getLooped(y)

        return Shape(TextInfo("$x;$y", m), q, Color.all.getLooped(y), image)
    }


    fun draw(renderer: Renderer2D, worldMatrix: Matrix3f) {
        rebuildIfChanged()

        when (renderMode) {
            RenderMode.SINGLE -> {
                rows.forEach { row ->
                    renderer.draw(row.graphic, worldMatrix)
                }
            }

            RenderMode.LIST -> {
                rows.forEach { row ->
                    row.shapes.forEach { shape ->
                        drawElement(renderer, worldMatrix, shape)
                    }
                }
            }
        }
    }

    private fun drawElement(renderer: Renderer2D, worldMatrix: Matrix3f, shape: Shape) {
        when (fillMode) {
            FillMode.TEXT -> {
                renderer.drawText(
                    shape.textInfo.text,
                    shape.textInfo.matrix * worldMatrix,
                    font,
                    color = shape.color
                )
            }

            FillMode.IMAGE -> {
                renderer.draw(worldMatrix) {
                    addQuad(SimpleMaterial(Texture2D(shape.image)), shape.polygon, Box2f.ONE.toQuad())
                }
            }

            FillMode.COLOR -> {
                renderer.draw(worldMatrix) {
                    addQuad(shape.color, shape.polygon)
                }
            }
        }
    }

    fun update(tick: Duration) {
    }
}