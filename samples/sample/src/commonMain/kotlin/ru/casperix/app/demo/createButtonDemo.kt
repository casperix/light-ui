package ru.casperix.app.demo

import ru.casperix.light_ui.component.button.CheckBox
import ru.casperix.light_ui.component.button.PushButton
import ru.casperix.light_ui.component.button.ToggleButton
import ru.casperix.light_ui.skin.SkinProvider
import ru.casperix.light_ui.element.AbstractContainer
import ru.casperix.light_ui.element.Container
import ru.casperix.light_ui.layout.Layout
import ru.casperix.math.vector.float32.Vector2f

fun createButtonDemo(): AbstractContainer {
    val toggleButtonSkin = SkinProvider.skin.toggleButton.copy(defaultSize = Vector2f(240f, 60f))
    val checkButtonSkin = SkinProvider.skin.checkButton.copy(defaultSize = Vector2f(240f, 60f))

    return Container(
        Layout.VERTICAL,
        PushButton("Push button"),
        ToggleButton("Toggle button"),
        CheckBox("Check box"),
        ToggleButton("Toggle button\nchecked & disabled", true, toggleButtonSkin).apply {
            isEnabled = false
        },
        ToggleButton("Toggle button\nunchecked & disabled", false, toggleButtonSkin).apply {
            isEnabled = false
        },
        CheckBox("Check box\nchecked & disabled", true, checkButtonSkin).apply {
            isEnabled = false
        },
        CheckBox("Check box\nunchecked & disabled", false, checkButtonSkin).apply {
            isEnabled = false
        },
    )
}