package ru.casperix.app.demo.layout.editor

import ru.casperix.light_ui.View
import ru.casperix.light_ui.component.button.CheckBox
import ru.casperix.light_ui.component.misc.SideIndents
import ru.casperix.light_ui.component.text.Label
import ru.casperix.light_ui.element.Container
import ru.casperix.light_ui.layout.Layout

class SideIndentsEditor(name:String, callback: () -> Unit) : View {
    var side = SideIndents.ZERO

    override val root = Container(
        Layout.VERTICAL,
        Label(name),
        CheckBox("left", false) {
            val value = if (isChecked) 10f else 0f
            side = side.copy(left = value)
            callback()
        },
        CheckBox("right", false) {
            val value = if (isChecked) 10f else 0f
            side = side.copy(right = value)
            callback()
        },
        CheckBox("top", false) {
            val value = if (isChecked) 10f else 0f
            side = side.copy(top = value)
            callback()
        },
        CheckBox("bottom", false) {
            val value = if (isChecked) 10f else 0f
            side = side.copy(bottom = value)
            callback()
        },
    )
}