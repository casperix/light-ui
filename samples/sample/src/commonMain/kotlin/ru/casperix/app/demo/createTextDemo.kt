package ru.casperix.app.demo

import ru.casperix.light_ui.component.text.Label
import ru.casperix.light_ui.component.text.LabelSkin
import ru.casperix.light_ui.component.window.Window
import ru.casperix.light_ui.skin.SkinProvider
import ru.casperix.light_ui.element.AbstractContainer
import ru.casperix.light_ui.element.Container
import ru.casperix.light_ui.element.SizeMode
import ru.casperix.light_ui.element.setSizeMode
import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.util.wrapNodes
import ru.casperix.math.color.Color
import ru.casperix.multiplatform.font.FontLeading
import ru.casperix.multiplatform.font.FontReference
import ru.casperix.multiplatform.font.FontWeight
import ru.casperix.renderer.misc.AlignMode

fun createTextDemo(): AbstractContainer {
    return Window(
        "text",
        Layout.VERTICAL,
        Container(
            Layout.HORIZONTAL,
            listOf(
                Label("Normal", LabelSkin(FontReference("Serif", 32), Color.WHITE, null)),
                Label("Bold", LabelSkin(FontReference("Serif", 32, FontWeight.BOLD), Color.WHITE, null)),
                Label("Red", LabelSkin(FontReference("Serif", 32, FontWeight.BOLD), Color.RED, null)),
                Label("Leading\nLIGHT", LabelSkin(FontReference("Serif", 20, leading = FontLeading.LIGHT), Color.WHITE, null)),
                Label("Leading\nNORMAL", LabelSkin(FontReference("Serif", 20, leading = FontLeading.NORMAL), Color.WHITE, null)),
                Label("Leading\nDENSITY", LabelSkin(FontReference("Serif", 20, leading = FontLeading.DENSITY), Color.WHITE, null)),
                Label(
                    "Green - ascent\nRed - descent\nBlue - leading",
                    LabelSkin(FontReference("Serif", 20), Color.WHITE, null, drawTextMetrics = true)
                ),
                Label("Small label", LabelSkin(FontReference("Serif", 10), Color.WHITE, null)),
            ).onEach {
                it.setSizeMode(SizeMode.row)
            }.wrapNodes()
        ),
        Label("01112...IKW (SWM: default)", LabelSkin(FontReference("Serif", 50), Color.WHITE, null)),
        Label("01112...IKW (SWM: monospaced)", LabelSkin(FontReference("Monospaced", 50), Color.WHITE, null)),
        Label("01112...IKW (SWM: digits)", LabelSkin(FontReference("Serif", 50), Color.WHITE, null)),
        Label(TextAsset.text, SkinProvider.skin.label, alignMode = AlignMode.CENTER_TOP).setSizeMode(SizeMode.view)
    ).setSizeMode(SizeMode.max)
}