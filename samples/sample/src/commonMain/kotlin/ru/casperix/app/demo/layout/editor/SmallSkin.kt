package ru.casperix.app.demo.layout.editor

import ru.casperix.light_ui.skin.SkinProvider
import ru.casperix.math.vector.float32.Vector2f

object SmallSkin {
    val smallToggleButton = SkinProvider.skin.toggleButton.copy(defaultSize = Vector2f(130f, 40f))
}