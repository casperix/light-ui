package ru.casperix.app.demo.layout

import ru.casperix.app.demo.ContentGenerator.generateContent
import ru.casperix.light_ui.component.tabmenu.Tab
import ru.casperix.light_ui.component.tabmenu.TabMenu
import ru.casperix.light_ui.element.AbstractContainer
import ru.casperix.light_ui.element.SizeMode
import ru.casperix.light_ui.element.setSizeMode
import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.layout.LayoutSide


object LayoutDemo {
    fun create(): AbstractContainer {

        return TabMenu(
            LayoutSide.LEFT, listOf(
                Tab("orientation", OrientationLayoutDemo.create()),
                Tab("table", TableLayoutDemo.create()),
                Tab("stack", generateContent(Layout.STACK, 3)),
                Tab("free", generateContent(Layout.FREE)),
            )
        ).setSizeMode(SizeMode.max)
    }
}

