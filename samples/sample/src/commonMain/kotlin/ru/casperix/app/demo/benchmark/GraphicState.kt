package ru.casperix.app.demo.benchmark

import ru.casperix.math.vector.float32.Vector2f

data class GraphicState(
    val sizeMode: SizeVariant,
    val renderMode: RenderMode,
    val fillMode: FillMode,
    val windowSize: Vector2f,
    val useRandom: Boolean,
    val amount: Int
)