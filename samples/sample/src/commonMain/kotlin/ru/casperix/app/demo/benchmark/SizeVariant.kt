package ru.casperix.app.demo.benchmark

enum class SizeVariant(val range: ClosedFloatingPointRange<Float>) {
    SMALL(1f..10f),
    MEDIUM(10f..100f),
    LARGE(100f..1000f),
}