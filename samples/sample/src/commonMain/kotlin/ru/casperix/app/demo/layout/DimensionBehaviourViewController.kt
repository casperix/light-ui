package ru.casperix.app.demo.layout

import ru.casperix.app.demo.layout.editor.SizeModeEditor
import ru.casperix.light_ui.View
import ru.casperix.light_ui.component.panel.Panel
import ru.casperix.light_ui.element.Container
import ru.casperix.light_ui.layout.Layout

class DimensionBehaviourViewController(callback: (DimensionBehaviourViewController) -> Unit) : View {

    val xController = SizeModeEditor(false) { callback(this) }
    val yController = SizeModeEditor(true) { callback(this) }

    override val root = Panel(
        Layout.VERTICAL,
        Container(
            Layout.HORIZONTAL,
            xController.root,
            yController.root
        )
    )
}