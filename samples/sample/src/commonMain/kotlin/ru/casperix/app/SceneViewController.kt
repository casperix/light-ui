package ru.casperix.app

import ru.casperix.app.demo.*
import ru.casperix.app.demo.benchmark.createBenchmark
import ru.casperix.app.demo.layout.LayoutDemo
import ru.casperix.app.demo.scene.createSceneDemo
import ru.casperix.app.surface.Surface
import ru.casperix.light_ui.Stage
import ru.casperix.light_ui.View
import ru.casperix.light_ui.component.tabmenu.Tab
import ru.casperix.light_ui.component.tabmenu.TabMenu
import ru.casperix.light_ui.component.button.ToggleButton
import ru.casperix.light_ui.component.text.Label
import ru.casperix.light_ui.element.Container
import ru.casperix.light_ui.element.ElementUpdate
import ru.casperix.light_ui.element.SizeMode
import ru.casperix.light_ui.element.setSizeMode
import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.layout.LayoutSide
import ru.casperix.light_ui.skin.SkinProvider
import ru.casperix.misc.toPrecision
import ru.casperix.opengl.renderer.OpenGlRenderer
import kotlin.time.DurationUnit

class SceneViewController(val renderer: OpenGlRenderer, surface: Surface, val stage: Stage, val assets: AppAssets) : View {
    private val INIT_BY_PREFIX = "spine"

    private var frameCounter = 0L
    private val fpsMeasure = FPSMeasure()

    val tabs = listOf(
        Tab(ToggleButton("broken"), createBrokenDemo()),
        Tab(ToggleButton("config"), ConfigMenu.create(renderer, surface, stage)),
        Tab(ToggleButton("benchmark"), createBenchmark(assets)),
        Tab(ToggleButton("text"), createTextDemo()),
        Tab(ToggleButton("text-editor"), createTextEditorDemo()),
        Tab(ToggleButton("scene"), createSceneDemo(assets)),
        Tab(ToggleButton("layout"), LayoutDemo.create()),
        Tab(ToggleButton("buttons"), createButtonDemo()),
        Tab(ToggleButton("images"), createImageDemo(assets)),
        Tab(ToggleButton("progress-bar"), createProgressBarDemo()),
        Tab(ToggleButton("slider"), createSliderDemo()),
        Tab(ToggleButton("scroll-box"), createScrollBoxDemo()),
//        ToggleButton("tabs") { if (state.checked) content.component = dummy },
//        ToggleButton("chart") { if (state.checked) content.component = dummy },
//        ToggleButton("tree") { if (state.checked) content.component = dummy },
    )

    private val selectorController = TabMenu(
        LayoutSide.LEFT,
        tabs,
    ).setSizeMode(SizeMode.max)

    private val titleController = Label("perf-info", SkinProvider.skin.label)

    override val root = Container(
//        Layout.SPLIT_TOP,
        Layout.VERTICAL,
        titleController,
        selectorController,
    ).setSizeMode(SizeMode.max)

    init {
        tabs.forEach {
            val text = it.button.label.text
            if (text.startsWith(INIT_BY_PREFIX)) {
                it.button.isChecked = true
            }
        }
        root.update = ElementUpdate { tick ->
            frameCounter++
            fpsMeasure.nextFrame(tick)

            val fps = (1.0 / fpsMeasure.medianTick.toDouble(DurationUnit.SECONDS)).toPrecision(1)

            renderer.statistic.last.apply {
                val info = listOf(
                    Pair("triangles", triangles),
                    Pair("batches", batches),
                    Pair("states", states),
                    Pair("fps", fps),
                    Pair("static", static),
                    Pair("frames", frameCounter),
                    Pair("dynamic-bufs", dynamicBufferAmount),
                    Pair("static-bufs", staticBufferAmount),
                )
                titleController.text = info.map { (name, value) ->
                    "$name: $value"
                }.joinToString("; ")
            }

        }
    }
}
