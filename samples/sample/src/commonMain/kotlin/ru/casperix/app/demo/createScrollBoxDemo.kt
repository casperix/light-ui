package ru.casperix.app.demo

import ru.casperix.light_ui.component.scroll.ScrollBox
import ru.casperix.light_ui.component.window.Window
import ru.casperix.light_ui.element.AbstractContainer
import ru.casperix.light_ui.element.SizeMode
import ru.casperix.light_ui.element.setSizeMode
import ru.casperix.light_ui.layout.Layout


fun createScrollBoxDemo(): AbstractContainer {
    val scrollHorizontal = ScrollBox()
    scrollHorizontal.content.element = ContentGenerator.generateContent(Layout.HORIZONTAL_CENTER, 200, 100f..400f)

    val scrollVertical = ScrollBox()
    scrollVertical.content.element = ContentGenerator.generateContent(Layout.VERTICAL_CENTER, 5, 100f..400f)

    return Window(
        "scroll-box",
        Layout.VERTICAL,
        scrollHorizontal,
        scrollVertical,
    ).setSizeMode(SizeMode.view)
}