package ru.casperix.app.demo.benchmark

import ru.casperix.app.AppAssets
import ru.casperix.light_ui.component.togglegroup.ToggleGroupSelectionMode
import ru.casperix.light_ui.component.button.PushButton
import ru.casperix.light_ui.component.button.ToggleButton
import ru.casperix.light_ui.component.createToggleGroup
import ru.casperix.light_ui.component.text.Label
import ru.casperix.light_ui.component.togglegroup.ToggleGroup
import ru.casperix.light_ui.component.window.Window
import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.node.DrawContext
import ru.casperix.renderer.Renderer2D
import ru.casperix.light_ui.element.*
import kotlin.time.Duration

@ExperimentalUnsignedTypes
fun createBenchmark(assets: AppAssets): AbstractContainer {
    val builder = BenchmarkShapeBuilder(assets)

    val info = Label("")

    arrayOf(ToggleButton("fill: color") {
        if (isChecked) builder.fillMode = FillMode.COLOR
    },
        ToggleButton("fill: image") {
            if (isChecked) builder.fillMode = FillMode.IMAGE
        },
        ToggleButton("fill: text") {
            if (isChecked) builder.fillMode = FillMode.TEXT

        })
    val buttons = Container(
        Layout.VERTICAL,
        info,
        PushButton("64") { builder.amount = 64 },
        PushButton("1024") { builder.amount = 1024 },
        PushButton("16384") { builder.amount = 16384 },
        PushButton("262144") { builder.amount = 262144 },
        PushButton("1M") { builder.amount = 1024 * 1024 },
        PushButton("clear") { builder.amount = 0 },
        ToggleGroup(
            Layout.VERTICAL,
            ToggleGroupSelectionMode.ALWAYS_ONE,
            ToggleButton("render: single") {
                if (isChecked) builder.renderMode = RenderMode.SINGLE
            },
            ToggleButton("render: shape-list") {
                if (isChecked) builder.renderMode = RenderMode.LIST
            },
        ),
        ToggleButton("use random", builder::useRandom),
        ToggleGroup(
            Layout.VERTICAL,
            ToggleGroupSelectionMode.ALWAYS_ONE,
            ToggleButton("size: small") {
                if (isChecked) builder.sizeMode = SizeVariant.SMALL
            },
            ToggleButton("size: medium") {
                if (isChecked) builder.sizeMode = SizeVariant.MEDIUM
            },
            ToggleButton("size: large") {
                if (isChecked) builder.sizeMode = SizeVariant.LARGE
            },
        ),

        )

    return Window(
        "benchmark",
        Layout.HORIZONTAL,
        buttons,
        AbstractElement(SizeMode.max, object : ElementDrawer {

            override fun draw(renderer: Renderer2D, context: DrawContext, tick: Duration) {
                builder.windowSize = context.size
                builder.draw(renderer, context.worldMatrix)
            }
        }),
    ).setSizeMode(SizeMode.max)
        .setUpdate { tick ->
            info.text = "elements: " + builder.amount
            builder.update(tick)
        }
}
