package ru.casperix.app.demo

import ru.casperix.app.AppAssets
import ru.casperix.light_ui.component.image.Image
import ru.casperix.light_ui.component.window.Window
import ru.casperix.light_ui.layout.Layout
import ru.casperix.math.angle.float32.DegreeFloat
import ru.casperix.light_ui.element.*
import kotlin.time.DurationUnit

fun createImageDemo(assets: AppAssets): Element {
    return Container(
        Layout.VERTICAL,
        Window(
            "default",
            Layout.DEFAULT,
            Container(
                Layout.DEFAULT,
                Image(assets.lorry.content!!)
            ).setSizeMode(SizeMode.max)
        ),
        Window(
            "scale (0.5)",
            Layout.DEFAULT,
            Container(
                Layout.DEFAULT,
                Image(assets.lorry.content!!).apply {
                    scale = 0.5f
                }
            ).setSizeMode(SizeMode.max)
        ),
        Window(
            "scale (0.5) & rotate",
            Layout.DEFAULT,
            Container(
                Layout.DEFAULT,
                Image(assets.lorry.content!!).apply {
                    scale = 0.5f
                }.setElementUpdate { image, duration ->
                    image.rotate += DegreeFloat(30f * duration.toDouble(DurationUnit.SECONDS).toFloat())
                }
            ).setSizeMode(SizeMode.max)
        ),
    )
}