package ru.casperix.app.demo.layout

import ru.casperix.app.demo.ContentGenerator
import ru.casperix.app.demo.layout.editor.AlignEditor
import ru.casperix.app.demo.layout.editor.OrientationEditor
import ru.casperix.light_ui.component.button.ButtonLogic
import ru.casperix.light_ui.component.button.PushButton
import ru.casperix.light_ui.component.panel.Panel
import ru.casperix.light_ui.component.text.Label
import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.layout.table_layout.TableLayout
import ru.casperix.light_ui.node.Node
import ru.casperix.renderer.misc.AlignMode
import ru.casperix.signals.concrete.EmptySignal
import ru.casperix.light_ui.element.*


object TableLayoutDemo {
    val MIN_AMOUNT = 1
    val MAX_AMOUNT = 20

    fun create(): Element {
        val content = Node(Empty())

        val onGenerate = EmptySignal()
        val generate = onGenerate::set

        var actualItem: Node? = null
        var actualAmount = 4
        var actualSeed = 1

        val selectedInfo = Label("<empty>")
        var selfEditor = false
        val dimensionController = DimensionBehaviourViewController {
            val item = actualItem ?: return@DimensionBehaviourViewController
            if (!selfEditor) {
                item.element.placement.sizeMode = SizeMode(it.xController.dimensionMode, it.yController.dimensionMode)
            }
        }

        val orientationController = OrientationEditor(generate)
        val horizontalAlignEditor = AlignEditor(false, generate)
        val verticalAlignEditor = AlignEditor(true, generate)

        onGenerate.then {
            content.element = ContentGenerator.generateContent(
                TableLayout(
                    orientationController.orientation,
                    3,
                    AlignMode(horizontalAlignEditor.align, verticalAlignEditor.align)
                ),
                actualAmount,
                seed = actualSeed,
                postBuilder = { index, node ->
                    node.element.placement.sizeMode = SizeMode.content
                    (node.element.input as? ButtonLogic)?.clickEvent?.then {
                        selectedInfo.text = "item: ${index + 1}"
                        actualItem = node
                        selfEditor = true
                        dimensionController.xController.dimensionMode = node.element.placement.sizeMode.width
                        dimensionController.yController.dimensionMode = node.element.placement.sizeMode.height
                        selfEditor = false
                    }
                },
                generateSubChild = false
            )
        }

        onGenerate.set()

        return Panel(
            Layout.HORIZONTAL,
            Node(
                Container(
                    Layout.VERTICAL,
                    PushButton("generate") {
                        actualSeed++
                        onGenerate.set()
                    },
                    PushButton("add item") {
                        actualAmount = (actualAmount + 1).coerceIn(MIN_AMOUNT..MAX_AMOUNT)
                        onGenerate.set()
                    },
                    PushButton("remove item") {
                        actualAmount = (actualAmount - 1).coerceIn(MIN_AMOUNT..MAX_AMOUNT)
                        onGenerate.set()
                    },
                    Container(
                        Layout.HORIZONTAL,
                        orientationController.root,
                        horizontalAlignEditor.root,
                        verticalAlignEditor.root,
                    ),
                    Container(
                        Layout.VERTICAL,
                        selectedInfo,
                        dimensionController.root,
                    )
                )
            ),
            content
        ).setSizeMode(SizeMode.max)
    }
}