package ru.casperix.app

import ru.casperix.app.surface.Surface
import ru.casperix.app.surface.component.FullScreenSupport
import ru.casperix.light_ui.DebugReport
import ru.casperix.light_ui.Stage
import ru.casperix.light_ui.StageConfig
import ru.casperix.light_ui.buildAndUpdate
import ru.casperix.light_ui.component.button.CheckBox
import ru.casperix.light_ui.component.button.PushButton
import ru.casperix.light_ui.component.createTable
import ru.casperix.light_ui.element.Element
import ru.casperix.light_ui.element.ElementUpdate
import ru.casperix.misc.format.FormatType
import ru.casperix.multiplatform.PlatformMisc
import ru.casperix.multiplatform.text.TextRenderConfig
import ru.casperix.multiplatform.text.impl.MissingSymbolBehaviour
import ru.casperix.opengl.renderer.OpenGlRenderer
import ru.casperix.renderer.Renderer2D

object ConfigMenu {
    fun create(renderer: OpenGlRenderer, surface: Surface, stage: Stage): Element = ConfigMenuView().buildAndUpdate {

        val elements = mutableListOf<Element>()

        renderer.let {
            val config = it.config

            elements += listOf(
                CheckBox("Accumulate batches", handler = { it.config = it.config.copy(cacheAccumulateBatches = isChecked) }),
                CheckBox("Cull face", handler = { it.config = it.config.copy(geometryCullFace = isChecked) }),
                CheckBox("Static cache", handler = { it.config = it.config.copy(cacheStaticUsing =  isChecked) }),
            )
        }

        elements += listOf(
//            Label("debug:"),
            CheckBox("use one fps mode", PlatformMisc::debugOneFPSMode),
            CheckBox("gui hierarchy", StageConfig::debugHierarchy),
            CheckBox("gui contents", StageConfig::debugContents),
            CheckBox("gui viewports", StageConfig::debugViewports),
            CheckBox("gui legend", StageConfig::debugLegend),
            CheckBox("viewport filter", StageConfig::viewportFilter),
//            CheckBox("show text metrics", TextRenderConfig::debugTextMetrics),
//            Label("features:"),
            CheckBox("continuous rendering", PlatformMisc::continuousRendering),
            CheckBox("text round to pixel", TextRenderConfig::textRoundToPixel),
//            CheckBox("text align if overflow", TextRenderConfig::textAlignIfOverflow),
            PushButton("Print hierarchy") {
                val report = DebugReport.debugHierarchy(stage.root)
                println(report)
            },
        )

        buttons.element = createTable(elements, 2)

        TextRenderConfig.apply {
            btnCharMissingIgnore.setLabel("char missing: ignore").setListener {
                textMissingSymbolBehaviour = MissingSymbolBehaviour.IGNORE
            }
            btnCharMissingSquare.setLabel("char missing: square").setListener {
                textMissingSymbolBehaviour = MissingSymbolBehaviour.SHOW_WHITE_SQUARE
            }
            btnCharMissingCode.setLabel("char missing: code").setListener {
                textMissingSymbolBehaviour = MissingSymbolBehaviour.SHOW_CHAR_CODE
            }
        }

        btnPrintSize.setLabel("Print size").setListener {
            renderer.viewPort.run {
                info.text = "Size: $width x $height"
            }
        }
        btnVSync.setLabel("VSync").setListener(surface.getVerticalSync()) {
            surface.setVerticalSync(isChecked)
        }
        btnFullScreen.setLabel("FullScreen").setListener(false) {
            (surface as? FullScreenSupport)?.setFullScreen(isChecked)
        }

        root.title.text = "window"

        ElementUpdate {
            val display = surface.getDisplay()

            info.text =
                listOf(
                    "Platform:",
                    surface.getTypeName(),
                    "Window:",
                    "size: ${surface.getSize().format(FormatType.SHORT)}",
                    "position: ${surface.getPosition().format(FormatType.SHORT)}",
                    "Display:",
                    "size: ${display.inchSize.format(FormatType.SHORT)}",
                    "DPI: ${display.densityPerInch.format(FormatType.SHORT)}",
                    "resolution: ${display.resolution.format(FormatType.SHORT)}",
                    "BPP: ${display.bitsPerPixel}",
                    "Frequency: ${display.frequency}",
                ).joinToString("\n")
        }
    }
}