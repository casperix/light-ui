package ru.casperix.app.demo.layout.editor

import ru.casperix.light_ui.View
import ru.casperix.light_ui.component.button.CheckBox
import ru.casperix.light_ui.component.text.Label
import ru.casperix.light_ui.element.Container
import ru.casperix.light_ui.layout.Layout
import ru.casperix.math.vector.float32.Vector2f

class GapEditor(name: String, callback: () -> Unit) : View {
    var gap = Vector2f.ZERO

    override val root = Container(
        Layout.VERTICAL,
        Label(name),
        CheckBox("horizontal", false) {
            val value = if (isChecked) 10f else 0f
            gap = gap.copy(x = value)
            callback()
        },
        CheckBox("vertical", false) {
            val value = if (isChecked) 10f else 0f
            gap = gap.copy(y = value)
            callback()
        },
    )
}