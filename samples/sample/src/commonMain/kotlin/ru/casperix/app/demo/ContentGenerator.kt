package ru.casperix.app.demo

import ru.casperix.app.demo.layout.LayoutDrawer
import ru.casperix.light_ui.StageConfig.COLORS
import ru.casperix.light_ui.component.button.ButtonLogic
import ru.casperix.light_ui.component.button.ButtonState
import ru.casperix.light_ui.component.misc.SideIndents
import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.node.Node
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.collection.getLooped
import ru.casperix.math.random.nextVector2f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.light_ui.element.*
import kotlin.random.Random
import kotlin.random.nextInt

object ContentGenerator {

    fun generateContent(
        layout: Layout,
        preferredAmount: Int? = null,
        sizeRange: ClosedFloatingPointRange<Float> = 50f..200f,
        postBuilder: (Int, Node) -> Unit = { _, _ -> },
        generateSubChild: Boolean = true,
        depth: Int = 0,
        seed:Int = 1,
    ): AbstractContainer {
        val random = Random(seed)

        val amount = preferredAmount ?: random.nextInt(4..6)
        val nodes = generateSizeList(random, amount, sizeRange).mapIndexed { index, size ->
            val template = Node(AbstractElement(SizeMode.fixed(size), LayoutDrawer(COLORS.getLooped(index)), ButtonLogic(ButtonState()))).apply {
                transform = transform.translate(random.nextVector2f(0f, 200f).round())
                element.placement.contentArea = Box2f.byDimension(Vector2f.ZERO, size)
//                element.placement.borders = SideIndents(5f)
                //for free layout
                setPlace(Box2f.byDimension(transform.position, size))
            }
            postBuilder(index, template)
            if (index == 1 && generateSubChild) {
                val container = generateContent(Layout.HORIZONTAL, 2, generateSubChild = false, depth = depth + 1)
                Node(container)
            } else {
                template
            }.apply {
                if (generateSubChild) {
                    element.tag = "container-$index"
                } else {
                    element.tag = "sub-container-$index"
                }
            }
        }

        val sizeMode = if (depth == 0) SizeMode.max else SizeMode.content

        return Container(
            layout,
            nodes
        ).setSizeMode(sizeMode).apply {
            if (generateSubChild) {
                placement.borders = SideIndents(5f)
            }
        }
    }

    fun generateElements(
        amount: Int = 6,
        sizeRange: ClosedFloatingPointRange<Float> = 50f..200f,
    ): List<Element> {
        val random = Random(1)
        return generateSizeList(random, amount, sizeRange).mapIndexed { index, size ->
            AbstractElement(SizeMode.fixed(size), LayoutDrawer(COLORS.getLooped(index))).apply {
                placement.contentArea = Box2f.byDimension(Vector2f.ZERO, size)
            }
        }
    }


    fun generateSizeList(random: Random, amount: Int, sizeRange: ClosedFloatingPointRange<Float>): List<Vector2f> {
        return (0 until amount).map {
            random.nextVector2f(sizeRange.start, sizeRange.endInclusive).round()
        }
    }
}