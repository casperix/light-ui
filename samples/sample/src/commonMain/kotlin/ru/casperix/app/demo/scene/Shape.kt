package ru.casperix.app.demo.scene

import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.color.Color
import ru.casperix.math.geometry.Polygon2f
import ru.casperix.math.geometry.Quad2f
import ru.casperix.renderer.Renderer2D
import ru.casperix.renderer.material.PhongMaterial
import ru.casperix.renderer.material.SimpleMaterial
import ru.casperix.renderer.pixel_map.PixelMap
import ru.casperix.renderer.vector.builder.VectorGraphicBuilderExtension2D.Companion.IDENTITY_QUAD

sealed interface Shape {
    fun draw(renderer: Renderer2D, geometry: Polygon2f)
}

class ShapeWithSimple(val color: Color) : Shape {
    override fun draw(renderer: Renderer2D, geometry: Polygon2f) {
        renderer.draw {
            addPolygon(color, geometry)
        }
    }

}

class ShapeWithPhong(val albedo: PixelMap, val normal: PixelMap?, val specular: PixelMap?) : Shape {
    override fun draw(renderer: Renderer2D, geometry: Polygon2f) {
        val quad = Quad2f.from(geometry.getVertices()) ?: return

        renderer.draw {
            addQuad(PhongMaterial(diffuseMap = albedo, normalMap = normal, specularMap = specular), quad, IDENTITY_QUAD)
        }
    }
}

class ShapeMarker(val albedo: PixelMap) : Shape {
    override fun draw(renderer: Renderer2D, geometry: Polygon2f) {
        val v = geometry.getVertices()
        if (v.isEmpty()) return

        val position = v.first()
        val quad = Box2f.byRadius(position, albedo.dimension.toVector2f() / 2f)

        renderer.draw {
            addRect(SimpleMaterial(albedo), quad, Box2f.ONE)
        }
    }
}
