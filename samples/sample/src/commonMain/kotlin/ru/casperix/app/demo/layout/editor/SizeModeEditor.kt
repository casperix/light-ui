package ru.casperix.app.demo.layout.editor

import ru.casperix.app.demo.layout.editor.SmallSkin.smallToggleButton
import ru.casperix.light_ui.View
import ru.casperix.light_ui.component.togglegroup.ToggleGroupSelectionMode
import ru.casperix.light_ui.component.button.ToggleButton
import ru.casperix.light_ui.component.createToggleGroup
import ru.casperix.light_ui.layout.Layout
import ru.casperix.light_ui.element.*


class SizeModeEditor(val vertical: Boolean, callback: () -> Unit) : View {
    private val FIXED = FixedDimension(100f)

    private val fixedToggle = ToggleButton("Fixed", skin = smallToggleButton).setListenerForChecked {
        dimensionMode = FIXED
        callback()
    }
    private val contentToggle = ToggleButton("Content", skin = smallToggleButton).setListenerForChecked {
        dimensionMode = ContentDimension
        callback()
    }
    private val viewToggle = ToggleButton("View", skin = smallToggleButton).setListenerForChecked {
        dimensionMode = ViewDimension
        callback()
    }
    private val minToggle = ToggleButton("Min C. or V.", skin = smallToggleButton).setListenerForChecked {
        dimensionMode = MinContentOrViewDimension
        callback()
    }
    private val maxToggle = ToggleButton("Max C. or V.", skin = smallToggleButton).setListenerForChecked {
        dimensionMode = MaxContentOrViewDimension
        callback()
    }
    private val layoutToggle = ToggleButton("Max C. or L.", skin = smallToggleButton).setListenerForChecked {
        dimensionMode = MaxContentOrLayoutDimension
        callback()
    }

    var dimensionMode: DimensionMode = MaxContentOrLayoutDimension
        set(value) {
            when (value) {
                is FixedDimension -> fixedToggle.isChecked = true
                is ContentDimension -> contentToggle.isChecked = true
                is ViewDimension -> viewToggle.isChecked = true
                is MinContentOrViewDimension -> minToggle.isChecked = true
                is MaxContentOrViewDimension -> maxToggle.isChecked = true
                is MaxContentOrLayoutDimension -> layoutToggle.isChecked = true
                else -> throw Exception("Invalid dimension $value")
            }
            field = value
        }

    override val root = Container(
        Layout.VERTICAL,
//        Label(axisName(vertical)),
        createToggleGroup(
            Layout.VERTICAL,
            ToggleGroupSelectionMode.ALWAYS_ONE,
            fixedToggle,
            contentToggle,
            viewToggle,
            minToggle,
            maxToggle,
            layoutToggle,
        )
    )
}