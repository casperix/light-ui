package ru.casperix.light_ui.component.spine

import ru.casperix.light_ui.element.*
import ru.casperix.light_ui.node.DrawContext
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.renderer.Renderer2D
import ru.casperix.renderer.misc.AlignMode
import ru.casperix.spine.Skeleton
import ru.casperix.spine.renderer.SpineController
import kotlin.time.Duration


class Spine(val skeleton: Skeleton) : Element, ElementDrawer, ElementUpdate {
    override val placement = ElementPlacement(SizeMode.view)
    override val drawer = this
    override var input: ElementInput? = null
    override var update: ElementUpdate? = this
    override var tag: String = ""

    val controller = SpineController(skeleton)
    /**
     *  useSpinePivot = true, for align spine pivot (ignore dimension)
     *  useSpinePivot = false, for align spine container (ignore pivot)
     */
    var useSpinePivot = false
    var align: AlignMode = AlignMode.CENTER_CENTER
    var debug = false
    var scale = 1f

    override fun update(tick: Duration) {
        controller.nextFrame(tick)
    }

    override fun draw(renderer: Renderer2D, context: DrawContext, tick: Duration) {
        val area = skeleton.data.area
        val pivotOffset = if (useSpinePivot) {
            Vector2f.ZERO
        } else {
            -area.min  - area.dimension / 2f
        }

        val contentSize = area.dimension * scale
        val viewportCenter = align.getPosition(placement.viewportSize, contentSize) + contentSize / 2f
        val scaleAndFlip = Vector2f(scale, -scale)
        val spinePivot = viewportCenter + pivotOffset * scaleAndFlip
        controller.worldMatrix = Matrix3f.scale(scaleAndFlip) * Matrix3f.translate(spinePivot) * context.worldMatrix
        placement.contentArea = Box2f.byCorners(spinePivot + area.min * scaleAndFlip, spinePivot + area.max * scaleAndFlip)

        controller.render(renderer, debug)
    }
}

