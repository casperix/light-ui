package ru.casperix.light_ui.component.figma

import ru.casperix.light_ui.component.figma.loader.JsonFigmaLoader
import kotlin.test.Test

class FigmaJsonParserTest {

    @Test
    fun test() {
        JsonFigmaLoader.load("simple.json").then({
            println("Load success")
        }, {
            throw Exception(it.toString())
        })
    }
}

