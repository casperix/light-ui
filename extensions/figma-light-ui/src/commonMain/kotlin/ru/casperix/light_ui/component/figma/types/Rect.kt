package ru.casperix.light_ui.component.figma.types

import kotlinx.serialization.Serializable

@Serializable
data class Rect(
    val x: Float,
    val y: Float,
    val width: Float,
    val height: Float,
)


