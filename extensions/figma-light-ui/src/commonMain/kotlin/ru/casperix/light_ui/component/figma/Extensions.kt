package ru.casperix.light_ui.component.figma

import ru.casperix.light_ui.component.figma.types.RGB
import ru.casperix.light_ui.component.figma.types.RGBA
import ru.casperix.light_ui.component.figma.types.TextAlignHorizontal
import ru.casperix.light_ui.component.figma.types.TextAlignVertical
import ru.casperix.math.color.float32.Color3f
import ru.casperix.math.color.float32.Color4f
import ru.casperix.renderer.misc.Align


fun TextAlignHorizontal.toAlign(): Align {
    return when (this) {
        TextAlignHorizontal.LEFT -> Align.MIN
        TextAlignHorizontal.CENTER -> Align.CENTER
        TextAlignHorizontal.RIGHT -> Align.MAX
        TextAlignHorizontal.JUSTIFIED -> Align.MIN
    }
}

fun TextAlignVertical.toAlign(): Align {
    return when (this) {
        TextAlignVertical.TOP -> Align.MIN
        TextAlignVertical.CENTER -> Align.CENTER
        TextAlignVertical.BOTTOM -> Align.MAX
    }
}


fun RGB.toColor(): Color3f {
    return Color3f(r.toFloat(), g.toFloat(), b.toFloat())
}

fun RGBA.toColor(): Color4f {
    return Color4f(r.toFloat(), g.toFloat(), b.toFloat(), a.toFloat())
}