package ru.casperix.light_ui.component.figma.types

enum class StrokeAlignJson {
    NONE,
    INSIDE,
    CENTER,
    OUTSIDE,
}