package ru.casperix.light_ui.component.figma.node

import ru.casperix.light_ui.component.figma.types.BlendMode
import ru.casperix.light_ui.component.figma.types.Effect
import ru.casperix.light_ui.component.figma.types.MaskType

interface BlendRelatedProperties {
    val opacity: Double
    val blendMode: BlendMode
    val isMask: Boolean
    val maskType: MaskType
    val effects: List<Effect>
    val effectStyleId: String?
}