package ru.casperix.light_ui.component.figma.types

import kotlinx.serialization.Serializable

@Serializable
data class RGBA(
    val r:Double,
    val g:Double,
    val b:Double,
    val a:Double,
)