package ru.casperix.light_ui.component.figma.node

import ru.casperix.light_ui.component.figma.types.ExportSettings

interface ExportRelatedProperties {
    val exportSettings:List<ExportSettings>
}