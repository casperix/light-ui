package ru.casperix.light_ui.component.figma.types

enum class AxisSizingJson {
    FIXED,
    AUTO,
}