package ru.casperix.light_ui.component.figma.types

enum class TextResize {
    NONE,
    WIDTH_AND_HEIGHT,
    HEIGHT,
}