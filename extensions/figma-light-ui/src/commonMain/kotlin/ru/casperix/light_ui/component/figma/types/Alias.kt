package ru.casperix.light_ui.component.figma.types

import ru.casperix.math.quad_matrix.float32.Matrix3f

typealias Number = Double
typealias NumberOrMixed = Double//or mixed -- not supported
typealias StringOrMixed = String//or mixed -- not supported

/**
 * list1: [a b tx]
 * list2: [c d ty]
 */
typealias TransformJson = List<List<Float>>

val identityTransform = listOf(listOf(1f, 0f, 0f), listOf(0f, 1f, 0f))

fun TransformJson.toMatrix3f(): Matrix3f {
    val a = this[0][0]
    val b = this[0][1]
    val x = this[0][2]

    val c = this[1][0]
    val d = this[1][1]
    val y = this[1][2]

    return Matrix3f(
        floatArrayOf(
            a, c, 0f,
            b, d, 0f,
            x, y, 1f,
        )
    )
}

