package ru.casperix.light_ui.component.figma.types

enum class MaskType {
    ALPHA,
    VECTOR,
    LUMINANCE,
}