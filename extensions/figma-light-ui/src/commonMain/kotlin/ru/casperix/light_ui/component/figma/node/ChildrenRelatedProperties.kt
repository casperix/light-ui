package ru.casperix.light_ui.component.figma.node

import ru.casperix.light_ui.component.figma.node.SceneNodeProperties

interface ChildrenRelatedProperties {
    val children: List<SceneNodeProperties>
}