package ru.casperix.light_ui.component.figma.types

import kotlinx.serialization.Serializable

@Serializable
data class UnitValue(
    val value: Double = 0.0,
    val unit: UnitType = UnitType.AUTO,
) {
    companion object {
        val ZERO = UnitValue(0.0, UnitType.PIXELS)
    }
}