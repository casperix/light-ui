package ru.casperix.light_ui.component.figma.types

enum class TextDecoration {
    NONE,
    UNDERLINE,
    STRIKETHROUGH,
}