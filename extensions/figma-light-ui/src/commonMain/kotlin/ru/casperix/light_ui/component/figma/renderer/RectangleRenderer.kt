package ru.casperix.light_ui.component.figma.renderer

import ru.casperix.light_ui.component.figma.node.RectangleNode
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.float64.Vector2d
import ru.casperix.renderer.Renderer2D
import kotlin.io.encoding.ExperimentalEncodingApi

object RectangleRenderer {

    fun renderNode(renderer: Renderer2D, node: RectangleNode, worldMatrix: Matrix3f = Matrix3f.IDENTITY) = node.apply {
        val area = node.run {
            Box2f.byDimension(Vector2f.ZERO, Vector2d(width, height).toVector2f())
        }

        node.fills.forEach { paint ->
            PaintRenderer.renderPaint(renderer, paint, node.cornerRadius, area, worldMatrix)
        }
    }
}
