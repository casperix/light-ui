package ru.casperix.light_ui.component.figma.types

enum class TextAlignHorizontal {
    LEFT,
    CENTER,
    RIGHT,
    JUSTIFIED,
}