package ru.casperix.light_ui.component.figma.node

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import ru.casperix.light_ui.component.figma.node.BaseNodeProperties
import ru.casperix.light_ui.component.figma.node.DevResourceProperties
import ru.casperix.light_ui.component.figma.node.PluginDataProperties


@Serializable
@SerialName("DOCUMENT")
data class DocumentNode(
    override val id: String,
    override val type: String,
    override val name: String,
    val children: List<PageNode>,
) : BaseNodeProperties, PluginDataProperties, DevResourceProperties


