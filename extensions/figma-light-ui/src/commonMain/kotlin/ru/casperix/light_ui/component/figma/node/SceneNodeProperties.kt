package ru.casperix.light_ui.component.figma.node

import kotlinx.serialization.Serializable

@Serializable
sealed interface SceneNodeProperties : BaseNodeProperties {
    val visible: Boolean
    val locked: Boolean
    val stuckNodes: List<Unit>
    val attachedConnectors: List<Unit>
    val boundVariables: Unit?
    val inferredVariables: Unit?
    val resolvedVariableModes: Unit?
    val explicitVariableModes: Unit?
}
//BooleanOperationNode
//CodeBlockNode
//ComponentNode
//ComponentSetNode
//ConnectorNode
//EllipseNode
//EmbedNode
//FrameNode
//GroupNode
//InstanceNode
//LineNode
//LinkUnfurlNode
//MediaNode
//PolygonNode
//RectangleNode
//SectionNode
//ShapeWithTextNode
//SliceNode
//StampNode
//StarNode
//StickyNode
//TableNode
//TableCellNode
//TextNode
//VectorNode
//WidgetNode
//MediaNode