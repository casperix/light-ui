package ru.casperix.light_ui.component.figma.types

enum class AxisAligning {
    AUTO,
    MIN,
    MAX,
    CENTER,
    SPACE_BETWEEN,
    BASELINE,
}
