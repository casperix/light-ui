package ru.casperix.light_ui.component.figma.node

import ru.casperix.light_ui.component.figma.types.*

interface GeometryRelatedProperties {
    val dashPattern: List<Double>
    val fills: List<Paint>
    val fillGeometry: List<VectorPath>?
    val fillStyleId: StringOrMixed
    val strokes: List<Paint>
    val strokeStyleId: StringOrMixed
    val strokeWeight: NumberOrMixed
    val strokeJoin: StrokeJoinJson
    val strokeAlign: StrokeAlignJson
    val strokeGeometry: List<VectorPath>?
    val strokeCap: StrokeCapJson
    val strokeMiterLimit: Double
}