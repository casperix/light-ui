package ru.casperix.light_ui.component.figma.types

import kotlinx.serialization.Serializable

/**
 *
 * "data" is list of commands:
 * M x y: The absolute "move to" command.
 * L x y: The absolute "line to" command.
 * Q x0 y0 x y: The absolute "quadratic spline to" command. Note that while Figma supports this as input, we will never generate this ourselves. All quadratic splines are converted to cubic splines internally.
 * C x0 y0 x1 y1 x y: The absolute "cubic spline to" command.
 * Z: The "close path" command.
 */
@Serializable
data class VectorPath(
    val windingRule: WindingRule,
    val data: String,
)

