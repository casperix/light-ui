package ru.casperix.light_ui.component.figma.node

interface IndividualStrokesRelatedProperties {
    val strokeBottomWeight: Double
    val strokeLeftWeight: Double
    val strokeRightWeight: Double
    val strokeTopWeight: Double
}