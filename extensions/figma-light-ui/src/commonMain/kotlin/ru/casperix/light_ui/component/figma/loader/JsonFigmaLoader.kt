package ru.casperix.light_ui.component.figma.loader

import kotlinx.serialization.json.Json
import ru.casperix.light_ui.component.figma.types.FigmaFile
import ru.casperix.misc.Left
import ru.casperix.misc.Right
import ru.casperix.multiplatform.loader.ResourceCustomLoadError
import ru.casperix.multiplatform.loader.ResourceLoadError
import ru.casperix.multiplatform.loader.resourceLoader
import ru.casperix.signals.concrete.EitherFuture
import ru.casperix.signals.concrete.mapAccept

object JsonFigmaLoader {
    private val json = Json {
        ignoreUnknownKeys = true
        classDiscriminator = "type"
    }

    fun load(path: String): EitherFuture<FigmaFile, ResourceLoadError> = resourceLoader.loadText(path).mapAccept {
        try {
            val file = json.decodeFromString<FigmaFile>(it)
            Right(file)
        } catch (t: Throwable) {
            Left(ResourceCustomLoadError("Decode exception ($path): ${t.message}"))
        }
    }
}