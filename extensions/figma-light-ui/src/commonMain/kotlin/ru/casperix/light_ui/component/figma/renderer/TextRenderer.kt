package ru.casperix.light_ui.component.figma.renderer

import ru.casperix.light_ui.component.figma.node.TextNode
import ru.casperix.light_ui.component.figma.toAlign
import ru.casperix.math.color.Colors
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.multiplatform.font.FontReference
import ru.casperix.multiplatform.text.drawText
import ru.casperix.multiplatform.text.impl.TextView
import ru.casperix.multiplatform.text.textRenderer
import ru.casperix.renderer.Renderer2D
import ru.casperix.renderer.misc.AlignMode
import kotlin.math.roundToInt

object TextRenderer {
    fun renderNode(renderer: Renderer2D, node: TextNode, worldMatrix: Matrix3f = Matrix3f.IDENTITY) = node.apply {
        val reference = FontReference(
            fontName.family,
            fontSize.roundToInt(),
        )

        val size = Vector2f(width.toFloat(), height.toFloat())
        val align = AlignMode(textAlignHorizontal.toAlign(), textAlignVertical.toAlign())

        val scheme = textRenderer.getTextScheme(listOf(TextView(characters, reference, Colors.BLACK)), size, align)
        renderer.drawText(scheme, worldMatrix)
    }
}
