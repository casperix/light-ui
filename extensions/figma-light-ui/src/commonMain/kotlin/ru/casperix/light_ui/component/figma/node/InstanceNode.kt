package ru.casperix.light_ui.component.figma.node

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import ru.casperix.light_ui.component.figma.node.*
import ru.casperix.light_ui.component.figma.types.*

@Serializable
@SerialName("INSTANCE")
data class InstanceNode(
    override val id: String,
    override val type: String,
    override val name: String,
    override val children: List<SceneNodeProperties>,
//    val mainComponent: ComponentNode? = null,
    val mainComponentId: String,

    //SceneNodePropertiesProperties
    override val visible: Boolean = true,
    override val locked: Boolean = false,
    override val stuckNodes: List<Unit> = emptyList(),
    override val attachedConnectors: List<Unit> = emptyList(),
    override val boundVariables: Unit? = null,
    override val inferredVariables: Unit? = null,
    override val resolvedVariableModes: Unit? = null,
    override val explicitVariableModes: Unit? = null,

    //LayoutRelatedProperties
    override val x: Double,
    override val y: Double,
    override val width: Double,
    override val height: Double,
    override val minWidth: Double? = null,
    override val maxWidth: Double? = null,
    override val minHeight: Double? = null,
    override val maxHeight: Double? = null,
    override val relativeTransform: TransformJson,
    override val absoluteTransform: TransformJson? = null,
    override val absoluteBoundingBox: Rect? = null,
    override val layoutAlign: LayoutAlignJson = LayoutAlignJson.INHERIT,
    override val layoutGrow: Double,
    override val layoutPositioning: LayoutPositioningJson,
    override val absoluteRenderBounds: Rect? = null,
    override val constrainProportions: Boolean? = null,
    override val rotation: Double,
    override val layoutSizingHorizontal: LayoutSizingJson? = null,
    override val layoutSizingVertical: LayoutSizingJson? = null,
    override val constraints: Constraints,

    //FramePrototypingRelatedProperties
    override val detachedInfo: DetachedInfo? = null,
    override val layoutGrids: List<LayoutGrid> = emptyList(),
    override val gridStyleId: String = "",
    override val clipsContent: Boolean = false,
    override val guides: List<Guide> = emptyList(),
    override val inferredAutoLayout: InferredAutoLayoutResult? = null,
    override val layoutMode: Layout = Layout.NONE,
    override val layoutWrap: LayoutWrap = LayoutWrap.NO_WRAP,
    override val paddingLeft: Double = 0.0,
    override val paddingRight: Double = 0.0,
    override val paddingTop: Double = 0.0,
    override val paddingBottom: Double = 0.0,
    override val primaryAxisSizingMode: AxisSizingJson = AxisSizingJson.AUTO,
    override val counterAxisSizingMode: AxisSizingJson = AxisSizingJson.AUTO,
    override val primaryAxisAlignItems: AxisAligning = AxisAligning.AUTO,
    override val counterAxisAlignItems: AxisAligning = AxisAligning.AUTO,
    override val counterAxisAlignContent: AxisAligning = AxisAligning.AUTO,
    override val itemSpacing: Double = 0.0,
    override val counterAxisSpacing: Double? = null,
    override val itemReverseZIndex: Boolean = false,
    override val strokesIncludedInLayout: Boolean = false,
    override val devStatus: DevStatus? = null,

    //AnnotationProperties
    override val annotations: List<AnnotationFigma> = emptyList(),

    //ContainerRelatedProperties
    override val expanded: Boolean = false,

    //GeometryRelatedProperties
    override val dashPattern: List<Double> = emptyList(),
    override val fills: List<Paint> = emptyList(),
    override val fillGeometry: List<VectorPath> = emptyList(),
    override val fillStyleId: StringOrMixed = "",
    override val strokes: List<Paint> = emptyList(),
    override val strokeStyleId: StringOrMixed = "",
    override val strokeWeight: NumberOrMixed = 0.0,
    override val strokeJoin: StrokeJoinJson = StrokeJoinJson.MITER,
    override val strokeAlign: StrokeAlignJson = StrokeAlignJson.NONE,
    override val strokeGeometry: List<VectorPath> = emptyList(),
    override val strokeCap: StrokeCapJson = StrokeCapJson.NONE,
    override val strokeMiterLimit: Double = 0.0,

    //CornerRelatedProperties
    override val cornerRadius: NumberOrMixed = 0.0,
    override val cornerSmoothing: Double = 0.0,
    override val topLeftRadius: Double = 0.0,
    override val topRightRadius: Double = 0.0,
    override val bottomLeftRadius: Double = 0.0,
    override val bottomRightRadius: Double = 0.0,

    //BlendRelatedProperties
    override val opacity: Double = 1.0,
    override val blendMode: BlendMode = BlendMode.NORMAL,
    override val isMask: Boolean = false,
    override val maskType: MaskType = MaskType.ALPHA,
    override val effects: List<Effect> = emptyList(),
    override val effectStyleId: String? = null,


    //IndividualStrokesRelatedProperties
    override val strokeBottomWeight: Double = 0.0,
    override val strokeLeftWeight: Double = 0.0,
    override val strokeRightWeight: Double = 0.0,
    override val strokeTopWeight: Double = 0.0,

    //ExportRelatedProperties
    override val exportSettings: List<ExportSettings> = emptyList(),

    //ReactionPrototypingRelatedProperties
    override val reactions: List<Reaction> = emptyList(),

    ) : BaseNodeProperties, SceneNodeProperties, PluginDataProperties, DevResourceProperties, ChildrenRelatedProperties,
    AnnotationProperties, ContainerRelatedProperties, GeometryRelatedProperties, CornerRelatedProperties,
    BlendRelatedProperties, LayoutRelatedProperties, IndividualStrokesRelatedProperties, ExportRelatedProperties,
    ReactionPrototypingRelatedProperties,
    FramePrototypingRelatedProperties
