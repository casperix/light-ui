package ru.casperix.light_ui.component.figma.types

enum class ConstraintType {
    MIN,
    CENTER,
    MAX,
    STRETCH,
    SCALE,
}