package ru.casperix.light_ui.component.figma.types

import kotlinx.serialization.Serializable

@Serializable
data class HyperlinkTarget(
    val type: Type,
    val value: String,
) {
    enum class Type {
        URL,
        NODE,
    }
}

