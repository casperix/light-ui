package ru.casperix.light_ui.component.figma.types

import kotlinx.serialization.Serializable

@Serializable
object DocumentationLink