package ru.casperix.light_ui.component.figma.types

enum class LayoutAlignJson {
    MIN,
    CENTER,
    MAX,
    STRETCH,
    INHERIT,
}

