package ru.casperix.light_ui.component.figma.node

import ru.casperix.light_ui.component.figma.types.AnnotationFigma

interface AnnotationProperties {
    val annotations: List<AnnotationFigma>
}

