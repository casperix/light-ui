package ru.casperix.light_ui.component.figma.node

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import ru.casperix.light_ui.component.figma.node.*
import ru.casperix.light_ui.component.figma.types.*

@Serializable
@SerialName("TEXT")
data class TextNode(
    //BaseNodeProperties
    override val id: String,
    override val type: String,
    override val name: String,

    val textAlignHorizontal: TextAlignHorizontal = TextAlignHorizontal.CENTER,
    val textAlignVertical: TextAlignVertical = TextAlignVertical.CENTER,
    val textAutoResize: TextResize = TextResize.NONE,
    val textTruncation: TextTruncation = TextTruncation.DISABLED,
    val maxLines: Double? = null,
    val paragraphIndent: Double,
    val paragraphSpacing: Double,
    val listSpacing: Double = 0.0,
    val hangingPunctuation: Boolean = false,
    val hangingList: Boolean = false,
    val autoRename: Boolean = false,

    //BlendRelatedProperties
    override val opacity: Double = 1.0,
    override val blendMode: BlendMode = BlendMode.NORMAL,
    override val isMask: Boolean = false,
    override val maskType: MaskType = MaskType.ALPHA,
    override val effects: List<Effect> = emptyList(),
    override val effectStyleId: String? = null,

    //LayoutRelatedProperties
    override val x: Double = 0.0,
    override val y: Double = 0.0,
    override val width: Double = 0.0,
    override val height: Double = 0.0,
    override val minWidth: Double? = null,
    override val maxWidth: Double? = null,
    override val minHeight: Double? = null,
    override val maxHeight: Double? = null,
    override val relativeTransform: TransformJson = identityTransform,
    override val absoluteTransform: TransformJson? = null,
    override val absoluteBoundingBox: Rect? = null,
    override val layoutAlign: LayoutAlignJson = LayoutAlignJson.INHERIT,
    override val layoutGrow: Double = 0.0,
    override val layoutPositioning: LayoutPositioningJson,
    override val absoluteRenderBounds: Rect? = null,
    override val constrainProportions: Boolean? = null,
    override val rotation: Double = 0.0,
    override val layoutSizingHorizontal: LayoutSizingJson? = null,
    override val layoutSizingVertical: LayoutSizingJson? = null,
    override val constraints: Constraints = Constraints.DEFAULT,

    //SceneNodePropertiesProperties
    override val visible: Boolean = true,
    override val locked: Boolean = false,
    override val stuckNodes: List<Unit> = emptyList(),
    override val attachedConnectors: List<Unit> = emptyList(),
    override val boundVariables: Unit? = null,
    override val inferredVariables: Unit? = null,
    override val resolvedVariableModes: Unit? = null,
    override val explicitVariableModes: Unit? = null,

    //GeometryRelatedProperties
    override val dashPattern: List<Double> = emptyList(),
    override val fills: List<Paint> = emptyList(),
    override val fillGeometry: List<VectorPath> = emptyList(),
    override val fillStyleId: StringOrMixed = "",
    override val strokes: List<Paint> = emptyList(),
    override val strokeStyleId: StringOrMixed = "",
    override val strokeWeight: NumberOrMixed = 0.0,
    override val strokeJoin: StrokeJoinJson = StrokeJoinJson.MITER,
    override val strokeAlign: StrokeAlignJson = StrokeAlignJson.NONE,
    override val strokeGeometry: List<VectorPath> = emptyList(),
    override val strokeCap: StrokeCapJson = StrokeCapJson.NONE,
    override val strokeMiterLimit: Double = 0.0,

    //ExportRelatedProperties
    override val exportSettings: List<ExportSettings> = emptyList(),

    //Other
    val characters: String = "",
    val fontSize: NumberOrMixed = 16.0,
    val fontName: FontName = FontName.DEFAULT,
    val fontWeight: NumberOrMixed = 1.0,
    val textCase: TextCase = TextCase.ORIGINAL,
    val textDecoration: TextDecoration = TextDecoration.NONE,
    val letterSpacing: UnitValue = UnitValue.ZERO,
    val lineHeight: UnitValue = UnitValue.ZERO,
    val leadingTrim: LeadingTrim = LeadingTrim.NONE,
    val textStyleId: String = "",
    val hyperlink: HyperlinkTarget? = null,
) : BaseNodeProperties, SceneNodeProperties, BlendRelatedProperties, GeometryRelatedProperties, LayoutRelatedProperties, ExportRelatedProperties