package ru.casperix.light_ui.component.figma.types

enum class WindingRule {
    NONE,
    NONZERO,
    EVENODD,
}

