package ru.casperix.light_ui.component.figma.types

enum class TextCase {
    ORIGINAL,
    UPPER,
    LOWER,
    TITLE,
    SMALL_CAPS,
    SMALL_CAPS_FORCED,
}