package ru.casperix.light_ui.component.figma.types

enum class LayoutWrap {
    NO_WRAP,
    WRAP,
}