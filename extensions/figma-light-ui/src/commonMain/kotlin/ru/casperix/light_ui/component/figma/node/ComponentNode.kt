package ru.casperix.light_ui.component.figma.node

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import ru.casperix.light_ui.component.figma.types.*

@Serializable
@SerialName("COMPONENT")
data class ComponentNode(
    override val id: String,
    override val type: String,
    override val name: String,
    val description: String,
    val documentationLinks: List<DocumentationLink>,

    //SceneNodePropertiesProperties
    override val visible: Boolean = true,
    override val locked: Boolean = false,
    override val stuckNodes: List<Unit> = emptyList(),
    override val attachedConnectors: List<Unit> = emptyList(),
    override val boundVariables: Unit? = null,
    override val inferredVariables: Unit? = null,
    override val resolvedVariableModes: Unit? = null,
    override val explicitVariableModes: Unit? = null,

    //ChildrenRelatedProperties
    override val children: List<SceneNodeProperties>,

    //LayoutRelatedProperties
    override val x: Double,
    override val y: Double,
    override val width: Double,
    override val height: Double,
    override val minWidth: Double? = null,
    override val maxWidth: Double? = null,
    override val minHeight: Double? = null,
    override val maxHeight: Double? = null,
    override val relativeTransform: TransformJson,
    override val absoluteTransform: TransformJson? = null,
    override val absoluteBoundingBox: Rect? = null,
    override val layoutAlign: LayoutAlignJson = LayoutAlignJson.INHERIT,
    override val layoutGrow: Double,
    override val layoutPositioning: LayoutPositioningJson,
    override val absoluteRenderBounds: Rect? = null,
    override val constrainProportions: Boolean? = null,
    override val rotation: Double,
    override val layoutSizingHorizontal: LayoutSizingJson? = null,
    override val layoutSizingVertical: LayoutSizingJson? = null,
    override val constraints: Constraints,
) : BaseNodeProperties, SceneNodeProperties, ChildrenRelatedProperties, LayoutRelatedProperties
