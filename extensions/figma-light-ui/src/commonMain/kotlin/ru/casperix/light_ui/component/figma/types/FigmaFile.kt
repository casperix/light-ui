package ru.casperix.light_ui.component.figma.types

import kotlinx.serialization.Serializable
import ru.casperix.light_ui.component.figma.node.DocumentNode

/**
 * See https://www.figma.com/plugin-docs/api/
 */
@Serializable
data class FigmaFile(
    val version:String,
    val apiVersion:String,
    val document: DocumentNode,
    val viewport:Unit,
    val localPaintStyles:List<Unit>,
    val localTextStyles:List<Unit>,
    val localEffectStyles:List<Unit>,
    val localGridStyles:List<Unit>,
)