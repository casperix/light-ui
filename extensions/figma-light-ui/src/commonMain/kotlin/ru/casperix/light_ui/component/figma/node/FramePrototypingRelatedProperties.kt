package ru.casperix.light_ui.component.figma.node

import ru.casperix.light_ui.component.figma.types.*

interface FramePrototypingRelatedProperties {
    val detachedInfo: DetachedInfo?
    val layoutGrids: List<LayoutGrid>
    val gridStyleId: String
    val clipsContent: Boolean
    val guides: List<Guide>
    val inferredAutoLayout: InferredAutoLayoutResult?
    val layoutMode: Layout
    val layoutWrap: LayoutWrap
    val paddingLeft: Double
    val paddingRight: Double
    val paddingTop: Double
    val paddingBottom: Double
    val primaryAxisSizingMode: AxisSizingJson
    val counterAxisSizingMode: AxisSizingJson
    val primaryAxisAlignItems: AxisAligning
    val counterAxisAlignItems: AxisAligning
    val counterAxisAlignContent: AxisAligning
    val itemSpacing: Double
    val counterAxisSpacing: Double?
    val itemReverseZIndex: Boolean
    val strokesIncludedInLayout: Boolean
    val devStatus: DevStatus?
}