package ru.casperix.light_ui.component.figma.node

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import ru.casperix.light_ui.component.figma.node.ChildrenRelatedProperties
import ru.casperix.light_ui.component.figma.node.ExportRelatedProperties
import ru.casperix.light_ui.component.figma.types.ExportSettings
import ru.casperix.light_ui.component.figma.types.Guide
import ru.casperix.light_ui.component.figma.types.Paint

@Serializable
@SerialName("PAGE")
data class PageNode(
    override val id: String,
    override val type: String,
    override val name: String,

    //ChildrenRelatedProperties
    override val children: List<SceneNodeProperties>,

    val guides: List<Guide> = emptyList(),
    val selection: List<SceneNodeProperties> = emptyList(),
    val selectedTextRange: Unit? = null,
    val flowStartingPoints: List<Unit> = emptyList(),
    val backgrounds: List<Paint> = emptyList(),
    val prototypeBackgrounds: List<Paint> = emptyList(),
    val prototypeStartNode: Unit? = null,

    //ExportRelatedProperties
    override val exportSettings: List<ExportSettings> = emptyList(),
) : BaseNodeProperties, ChildrenRelatedProperties, ExportRelatedProperties

