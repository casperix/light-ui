package ru.casperix.light_ui.component.figma.types

import kotlinx.serialization.Serializable

@Serializable
data class Guide(
    val axis: GuideAxis,
    val offset: Double,
)