package ru.casperix.light_ui.component.figma.types

import kotlinx.serialization.Serializable

@Serializable
data class AnnotationFigma(
    val label: String? = null,
    val properties: List<Map<String, String>>? = null,
)