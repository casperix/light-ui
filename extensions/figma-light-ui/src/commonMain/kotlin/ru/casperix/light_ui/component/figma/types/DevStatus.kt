package ru.casperix.light_ui.component.figma.types

import kotlinx.serialization.Serializable

@Serializable
data class DevStatus(
    val type: Type,
    val description: String,
) {
    enum class Type {
        READY_FOR_DEV,
        COMPLETED,
    }
}