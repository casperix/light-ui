package ru.casperix.light_ui.component.figma.types

import kotlinx.serialization.Serializable

@Serializable
data class ImageFilters(
    val exposure: Double? = null,
    val contrast: Double? = null,
    val saturation: Double? = null,
    val temperature: Double? = null,
    val tint: Double? = null,
    val highlights: Double? = null,
    val shadows: Double? = null,
)
