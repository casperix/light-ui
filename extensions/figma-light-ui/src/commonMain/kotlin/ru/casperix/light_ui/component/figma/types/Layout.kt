package ru.casperix.light_ui.component.figma.types

enum class Layout {
    NONE,
    HORIZONTAL,
    VERTICAL,
}