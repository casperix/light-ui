package ru.casperix.light_ui.component.figma.node

import ru.casperix.light_ui.component.figma.types.Reaction

interface ReactionPrototypingRelatedProperties {
    val reactions: List<Reaction>
}