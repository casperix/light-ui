package ru.casperix.light_ui.component.figma.types

import kotlinx.serialization.Serializable

@Serializable
data class ColorStop(
    val position: Double,
    val color: RGBA,
    val boundVariables: Unit? = null,
)
