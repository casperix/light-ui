package ru.casperix.light_ui.component.figma.types

enum class TextTruncation {
    DISABLED,
    ENDING,
}
