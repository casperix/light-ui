package ru.casperix.light_ui.component.figma.renderer

import ru.casperix.light_ui.component.figma.node.*
import ru.casperix.light_ui.component.figma.types.toMatrix3f
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.color.Colors
import ru.casperix.math.geometry.grow
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.float64.Vector2d
import ru.casperix.math.vector.toQuad
import ru.casperix.multiplatform.font.FontReference
import ru.casperix.multiplatform.text.drawText
import ru.casperix.renderer.Renderer2D
import ru.casperix.renderer.material.SimpleMaterial

object FigmaRenderer {

    fun renderNode(renderer: Renderer2D, node: SceneNodeProperties, parentMatrix: Matrix3f = Matrix3f.IDENTITY) {
        val localMatrix = if (node is LayoutRelatedProperties) {
            node.relativeTransform.toMatrix3f()
        } else {
            Matrix3f.IDENTITY
        }
        val worldMatrix = localMatrix * parentMatrix


        if (node is TextNode) {
            TextRenderer.renderNode(renderer, node, worldMatrix)
        }
        if (node is RectangleNode) {
            RectangleRenderer.renderNode(renderer, node, worldMatrix)
        }
        if (node is FrameNode) {
            FrameRenderer.renderNode(renderer, node, worldMatrix)
        }

        if (node is ChildrenRelatedProperties) {
            node.children.forEach {
                renderNode(renderer, it, worldMatrix)
            }
        }

//        if (node is LayoutRelatedProperties) {
//            val area = node.run {
//                Box2f.byDimension(Vector2f.ZERO, Vector2d(width, height).toVector2f())
//            }.toQuad()
//
//            val identity = Box2f.ONE.toQuad()
//
//            renderer.drawQuad(SimpleMaterial(Colors.BLACK.setAlpha(0.2)), area, identity, worldMatrix)
//            renderer.drawText(node.name, FontReference("Serif", 24), worldMatrix)
//        }
    }
}