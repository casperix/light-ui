package ru.casperix.light_ui.component.figma.types

import kotlinx.serialization.Serializable

@Serializable
data class Constraints(
    val horizontal: ConstraintType,
    val vertical: ConstraintType,
) {
    companion object {
        val DEFAULT = Constraints(ConstraintType.CENTER, ConstraintType.CENTER)
    }
}

