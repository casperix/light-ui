package ru.casperix.light_ui.component.figma.types

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("VIDEO")
data class VideoPaint(
    override val type: String,
    val scaleMode: ImageScale,
    val videoHash: String? = null,
    val videoTransform: TransformJson? = null,
    val scalingFactor: Double? = null,
    val rotation: Double? = null,
    val filters: ImageFilters? = null,
) : Paint