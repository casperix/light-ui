package ru.casperix.light_ui.component.figma.types

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
sealed interface Paint {
    val type: String
}

@Serializable
@SerialName("SOLID")
data class SolidPaint(
    override val type: String,
    val color: RGB,
    val boundVariables: Unit?
) : Paint

enum class ImageScale {
    FILL,
    FIT,
    CROP,
    TILE,
}

interface GradientPaint : Paint {
    val gradientTransform: TransformJson
    val gradientStops: List<ColorStop>
}

@SerialName("GRADIENT_LINEAR")
class GradientLinearPaint(
    override val type: String,
    override val gradientTransform: TransformJson,
    override val gradientStops: List<ColorStop>,
) : GradientPaint


@SerialName("GRADIENT_RADIAL")
class GradientRadialPaint(
    override val type: String,
    override val gradientTransform: TransformJson,
    override val gradientStops: List<ColorStop>,
) : GradientPaint


@SerialName("GRADIENT_ANGULAR")
class GradientAngularPaint(
    override val type: String,
    override val gradientTransform: TransformJson,
    override val gradientStops: List<ColorStop>,
) : GradientPaint


@SerialName("GRADIENT_DIAMOND")
class GradientDiamondPaint(
    override val type: String,
    override val gradientTransform: TransformJson,
    override val gradientStops: List<ColorStop>,
) : GradientPaint

