package ru.casperix.light_ui.component.figma.renderer

import ru.casperix.light_ui.component.figma.drawRoundRect
import ru.casperix.light_ui.component.figma.identityQuad
import ru.casperix.light_ui.component.figma.imageDecoder
import ru.casperix.light_ui.component.figma.toColor
import ru.casperix.light_ui.component.figma.types.ImagePaint
import ru.casperix.light_ui.component.figma.types.Paint
import ru.casperix.light_ui.component.figma.types.SolidPaint
import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.vector.toQuad
import ru.casperix.renderer.Renderer2D
import ru.casperix.renderer.material.SimpleMaterial
import ru.casperix.renderer.pixel_map.PixelMap
import kotlin.io.encoding.Base64
import kotlin.io.encoding.ExperimentalEncodingApi

object PaintRenderer {
    private  val identityQuad = Box2f.ONE.toQuad()
    private val cache = mutableMapOf<String, PixelMap>()

    @OptIn(ExperimentalEncodingApi::class)
    fun renderPaint(renderer: Renderer2D, paint: Paint, cornerRadius: Double, area: Box2f, worldMatrix: Matrix3f) {
        when (paint) {
            is SolidPaint -> {
                val color = paint.color.toColor()

                if (cornerRadius > 0.0) {
                    renderer.drawRoundRect(color, area, cornerRadius.toFloat(), worldMatrix)
                } else {
                    renderer.drawBox(color, area, worldMatrix)
                }
            }

            is ImagePaint -> {
                val imgBase64 = paint.imgBase64 ?: return

                val image = cache.getOrPut(imgBase64) {
                    val bytes = Base64.decode(imgBase64)
                    imageDecoder.decode(bytes)
                }
                renderer.drawQuad(SimpleMaterial(image), area.toQuad(), identityQuad, worldMatrix)
            }

            else -> {

            }
        }
    }


}