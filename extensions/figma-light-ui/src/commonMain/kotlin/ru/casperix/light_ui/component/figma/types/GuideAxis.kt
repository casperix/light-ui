package ru.casperix.light_ui.component.figma.types

import kotlinx.serialization.Serializable

@Serializable
enum class GuideAxis {
    X,
    Y,
}