package ru.casperix.light_ui.component.figma.types

enum class DocumentColorProfile {
    LEGACY,
    SRGB,
    DISPLAY_P3,
}
