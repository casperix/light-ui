package ru.casperix.light_ui.component.figma.types

enum class TextAlignVertical {
    TOP,
    CENTER,
    BOTTOM,
}