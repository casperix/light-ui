package ru.casperix.light_ui.component.figma.node

import ru.casperix.light_ui.component.figma.node.BaseNodeProperties
import ru.casperix.light_ui.component.figma.types.*


interface LayoutRelatedProperties  {
    val x: Double
    val y: Double
    val width: Double
    val height: Double
    val minWidth: Double?
    val maxWidth: Double?
    val minHeight: Double?
    val maxHeight: Double?
    val relativeTransform: TransformJson
    val absoluteTransform: TransformJson?
    val absoluteBoundingBox: Rect?
    val layoutAlign: LayoutAlignJson
    val layoutGrow: Double
    val layoutPositioning: LayoutPositioningJson
    val absoluteRenderBounds: Rect?
    val constrainProportions: Boolean?
    val rotation: Double
    val layoutSizingHorizontal: LayoutSizingJson?
    val layoutSizingVertical: LayoutSizingJson?
    val constraints: Constraints
}

