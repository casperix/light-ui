package ru.casperix.light_ui.component.figma.node

import ru.casperix.light_ui.component.figma.types.NumberOrMixed

interface CornerRelatedProperties {
    val cornerRadius: NumberOrMixed
    val cornerSmoothing: Double
    val topLeftRadius: Double
    val topRightRadius: Double
    val bottomLeftRadius: Double
    val bottomRightRadius: Double
}