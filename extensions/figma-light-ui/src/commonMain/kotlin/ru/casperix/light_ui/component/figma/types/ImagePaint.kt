package ru.casperix.light_ui.component.figma.types

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("IMAGE")
data class ImagePaint(
    override val type: String,
    val scaleMode: ImageScale,
    val imageHash: String? = null,
    val imageTransform:TransformJson? = null,
    val scalingFactor:Double? = null,
    val rotation:Double? = null,
    val filters:ImageFilters? = null,
    val imgBase64:String? = null,//Undocumented
) : Paint