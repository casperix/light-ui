package ru.casperix.light_ui.component.figma

import ru.casperix.renderer.pixel_map.PixelMap

expect val imageDecoder:ImageDecoder


interface ImageDecoder {
    fun decode(bytes:ByteArray):PixelMap
}