package ru.casperix.light_ui.component.figma.types

import kotlinx.serialization.Serializable

@Serializable
data class Reaction(
    val action: Action?,
    val actions:List<Action>?,
    val trigger: Trigger?,
)