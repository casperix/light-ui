package ru.casperix.light_ui.component.figma.types

import kotlinx.serialization.Serializable

@Serializable
data class FontName(
    val family: String,
    val style: String,
) {
    companion object {
        val DEFAULT = FontName("Serif", "")
    }
}