package ru.casperix.light_ui.component.figma.types

enum class StrokeJoinJson {
    MITER,
    BEVEL,
    ROUND,
}