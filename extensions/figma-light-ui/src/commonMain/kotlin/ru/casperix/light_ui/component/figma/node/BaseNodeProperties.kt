package ru.casperix.light_ui.component.figma.node

interface BaseNodeProperties {
    val id: String
    val type: String
    val name: String
}

