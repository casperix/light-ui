package ru.casperix.light_ui.component.figma

import ru.casperix.multiplatform.loader.JvmImageConverter
import ru.casperix.renderer.pixel_map.PixelMap
import java.io.ByteArrayInputStream
import javax.imageio.ImageIO

actual val imageDecoder: ImageDecoder = JvmImageDecoder

object JvmImageDecoder : ImageDecoder {
    override fun decode(bytes: ByteArray): PixelMap {
        val inputStream = ByteArrayInputStream(bytes)
        val image = ImageIO.read(inputStream)
        return JvmImageConverter.createPixelMap(image, "")
    }

}