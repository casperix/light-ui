# light-ui

Add dependencies to `build.gradle`
```
dependencies {
    implementation "io.gitlab.casperix:light-ui:1.1.1"
    implementation "io.gitlab.casperix:opengl-renderer2d:1.1.0"
}
```

Now add the following code to your app
```
//  Create a tree of elements
val root = Node(...)

//  Create a stage
val stage = Stage(root)

while(true) {

    //  Take events
    stage.input(...)
    
    //  Draw frames
    stage.render(...)  
}
```

See full example [MinimalApplication](samples/minimal/src/main/kotlin/ru/casperix/app/MinimalApplication.kt)

### Deploy library
**Don't publish artifacts in parallel (configuration constraints)**

#### Local
Just add water:
- `./gradlew publishToMavenLocal`

#### Nexus
Need variables (ask the administrator):
`sonatypeStagingProfileId`,
`sonatypeUsername`,
`sonatypePassowrd`,
`signing.keyId`,
`signing.password`,
`signing.secretKeyRingFile`.

Publishing, closing, releasing:
- `./gradlew :light-ui:publishToSonatype`
- `./gradlew findSonatypeStagingRepository closeAndReleaseSonatypeStagingRepository`

- `./gradlew :extensions:figma-light-ui:publishToSonatype`
- `./gradlew findSonatypeStagingRepository closeAndReleaseSonatypeStagingRepository`

- `./gradlew :extensions:spine-light-ui:publishToSonatype`
- `./gradlew findSonatypeStagingRepository closeAndReleaseSonatypeStagingRepository`

In case of troubles, see also:
https://github.com/gradle-nexus/publish-plugin